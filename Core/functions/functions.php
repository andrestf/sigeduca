<?php
 


function DataDB($string, $sep = "-"){
     
    if($string == "") {
        return null;
    }
     
    $d = substr($string,0,2);
    $m = substr($string,3,2);
    $y = substr($string,6,4);
 
    return $y . $sep . $m . $sep . $d;
}
 
 
function DataBR($string){
     if($string == '')
            return null;    
    if($string == "") {
        return null;
    }
     
     
    $d = substr($string,8,2);
    $m = substr($string,5,2);
    $y = substr($string,0,4);
 
    return $d . "/" . $m . "/" . $y;
}


function DataHoraDB($string, $sep = "-") {
    if($string == '')
            return null;    
    
    $d = substr($string,0,2);
    $m = substr($string,3,2);
    $y = substr($string,6,4);

    $hr = substr($string,11,2);   
    $mi = substr($string,14,2);

    return $y . $sep . $m . $sep . $d . " " . $hr.":".$mi;
}

function DataHoraBR($string, $sep = "/") {
    if($string == '')
        return null;
    
    //echo $string;
    //2018-01-02 44:00
    
    $d = substr($string,8,2);
    $m = substr($string,5,2);
    $y = substr($string,0,4);

    $hr = substr($string,11,2);   
    $mi = substr($string,14,2);

    return $d . $sep . $m . $sep . $y . " " . $hr.":".$mi;
}

function Permalink($c,$a="",$datas="") {
     if(USE_REWRITEMOD) {
            if($datas != "")
                $datas = "/?$datas";
             
            return "/".$c."/".$a.$datas;
        } else {
            if($datas != "")
                $datas = "/&$datas";
             
            if($a != "")
                $a = "/$a";
             
            return '/index.php?route='.$c.$a.$datas."";
        }    
}


function MesExtenso($m){
    
    switch ($m) {
        case '1'  : $ret = 'Janeiro'    ; break;
        case '2'  : $ret = 'Fevereiro'  ; break;
        case '3'  : $ret = 'Março'      ; break;
        case '4'  : $ret = 'Abril'      ; break;
        case '5'  : $ret = 'Maio'       ; break;
        case '6'  : $ret = 'Junho'      ; break;
        case '7'  : $ret = 'Julho'      ; break;
        case '8'  : $ret = 'Agosto'     ; break;
        case '9'  : $ret = 'Setembro'   ; break;
        case '10' : $ret = 'Outubro'    ; break;
        case '11' : $ret = 'Novembro'   ; break;
        case '12' : $ret = 'Dezembro'   ; break;
        default : $ret = "MÊS INVÁLIDO"; break;
    }
    
    return $ret;
    
}

function fRand($n = 5, $onlyNum = false) {
    $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVXZKYW";

    if($onlyNum){
        $characters = "0123456789";
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $n; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function Embaralha($string,$type = "ENC",$n = 3) {
    /**/
    $type = strtoupper($type);
    $nEnc = $n;

    $out = $string;

    $old = array("A" ,"E" ,"I" ,"O" ,"U" ,"a" ,"e" ,"i" ,"o" ,"u" ,"L" ,"Z" ,"V" ,"v" ,"R" ,"r" ,"#"  );
    $new = array("*!w","4Xd","3w2","P5Y","x5!","caS","Mby","BwC","8e4","se2","Sby","Ino","sn!","+$@","T7X","d-4","80*");

    if ($type == "ENC") :
        $len = 0;
        for ($i=0; $i < $nEnc; $i++) {
            $out = base64_encode($out);
        }
        $out = str_replace($old, $new, $out);

    endif; //ENC

    if ($type == "DEC") :
        $out = str_replace(array("\r\n", "\r", "\n","<br/>","<br />"), "", $out);
        $out = str_replace("\n", "", $out);
        $out = str_replace($new, $old, $out);
        for ($i=0; $i < $nEnc; $i++) {
            $out = base64_decode($out);
        }
        return $out;
    endif;// DEC

    $nCrt = strlen($out);   $nL   = 9;  $nL0  = $nL;    $outX = $out;   $out2 = "";
    $nLinha = 0;
    for($i=0 ;$i<=$nCrt; $i++) {
        $linha = ""; $nLinha++;
        if($nLinha == $nL) {
            $linha = "<br/>";
            $nLinha = 0;
        }
        $xx = substr($out,($i*$nL0),$nL) . $linha;          
        if($xx != $linha) {
            $out2 .= $xx;
        }
    }
    $out = $out2;
    return $out;
}


/**
 * Retorna uma string do numero
 * 
 * @param string $n - Valor a ser traduzido,  apenas numeros inteiros
 * @example numeroEscrito('500');
 * @return string 
 */
function numeroEscrito($n) {
 
    $numeros[1][0] = '';
    $numeros[1][1] = 'um';
    $numeros[1][2] = 'dois';
    $numeros[1][3] = 'três';
    $numeros[1][4] = 'quatro';
    $numeros[1][5] = 'cinco';
    $numeros[1][6] = 'seis';
    $numeros[1][7] = 'sete';
    $numeros[1][8] = 'oito';
    $numeros[1][9] = 'nove';
 
    $numeros[2][0] = '';
    $numeros[2][10] = 'dez';
    $numeros[2][11] = 'onze';
    $numeros[2][12] = 'doze';
    $numeros[2][13] = 'treze';
    $numeros[2][14] = 'quatorze';
    $numeros[2][15] = 'quinze';
    $numeros[2][16] = 'dezesseis';
    $numeros[2][17] = 'dezesete';
    $numeros[2][18] = 'dezoito';
    $numeros[2][19] = 'dezenove';
    $numeros[2][2] = 'vinte';
    $numeros[2][3] = 'trinta';
    $numeros[2][4] = 'quarenta';
    $numeros[2][5] = 'cinquenta';
    $numeros[2][6] = 'sessenta';
    $numeros[2][7] = 'setenta';
    $numeros[2][8] = 'oitenta';
    $numeros[2][9] = 'noventa';
 
    $numeros[3][0] = '';
    $numeros[3][1] = 'cem';
    $numeros[3][2] = 'duzentos';
    $numeros[3][3] = 'trezentos';
    $numeros[3][4] = 'quatrocentos';
    $numeros[3][5] = 'quinhentos';
    $numeros[3][6] = 'seiscentos';
    $numeros[3][7] = 'setecentos';
    $numeros[3][8] = 'oitocentos';
    $numeros[3][9] = 'novecentos';
 
    $qtd = strlen($n);
 
    $compl[0] = ' mil ';
    $compl[1] = ' milhão ';
    $compl[2] = ' milhões ';
    $numero = "";
    $casa = $qtd;
    $pulaum = false;
    $x = 0;
    for ($y = 0; $y < $qtd; $y++) {
 
        if ($casa == 5) {
 
            if ($n[$x] == '1') {
 
                $indice = '1' . $n[$x + 1];
                $pulaum = true;
            } else {
 
                $indice = $n[$x];
            }
 
            if ($n[$x] != '0') {
 
                if (isset($n[$x - 1])) {
 
                    $numero .= ' e ';
                }
 
                $numero .= $numeros[2][$indice];
 
                if ($pulaum) {
 
                    $numero .= ' ' . $compl[0];
                }
            }
        }
 
        if ($casa == 4) {
 
            if (!$pulaum) {
 
                if ($n[$x] != '0') {
 
                    if (isset($n[$x - 1])) {
 
                        $numero .= ' e ';
                    }
                }
            }
 
            $numero .= $numeros[1][$n[$x]] . ' ' . $compl[0];
        }
 
        if ($casa == 3) {
 
            if ($n[$x] == '1' && $n[$x + 1] != '0') {
 
                $numero .= 'cento ';
            } else {
 
                if ($n[$x] != '0') {
 
                    if (isset($n[$x - 1])) {
 
                        $numero .= ' e ';
                    }
 
                    $numero .= $numeros[3][$n[$x]];
                }
            }
        }
 
        if ($casa == 2) {
 
            if ($n[$x] == '1') {
 
                $indice = '1' . $n[$x + 1];
                $casa = 0;
            } else {
 
                $indice = $n[$x];
            }
 
            if ($n[$x] != '0') {
 
                if (isset($n[$x - 1])) {
 
                    $numero .= ' e ';
                }
 
                $numero .= $numeros[2][$indice];
            }
        }
 
        if ($casa == 1) {
 
            if ($n[$x] != '0') {
                if ($numeros[1][$n[$x]] <= 10)
                    $numero .= ' ' . $numeros[1][$n[$x]];
                else
                    $numero .= ' e ' . $numeros[1][$n[$x]];
            } else {
 
                $numero .= '';
            }
        }
 
        if ($pulaum) {
 
            $casa--;
            $x++;
            $pulaum = false;
        }
 
        $casa--;
        $x++;
    }
 
    return $numero;
}

/**
 * Retorna uma string do valor 
 *  
 * @param string $n - Valor a ser traduzido, pode ser no formato americano ou brasileiro
 * @example escreverValorMoeda('1.530,64');
 * @example escreverValorMoeda('1530.64');
 * @return string 
 */
function escreverValorMoeda($n){
    //Converte para o formato float 
    if(strpos($n, ',') !== FALSE){
        $n = str_replace('.','',$n); 
        $n = str_replace(',','.',$n);
    }
 
    //Separa o valor "reais" dos "centavos"; 
    $n = explode('.',$n);
 
    return ucfirst(numeroEscrito($n[0])). ' reais' . ((isset($n[1]) && $n[1] > 0)?' e '.numeroEscrito($n[1]).' centavos.':'');
 
}


# date_range("2014-01-01", "2014-01-20", "+1 day", "m/d/Y");
# date_range("01:00:00", "23:00:00", "+1 hour", "H:i:s");
function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

function removeAcentos($string, $slug = false) {
  $string = strtolower($string);
  // Código ASCII das vogais
  $ascii['a'] = range(224, 230);
  $ascii['e'] = range(232, 235);
  $ascii['i'] = range(236, 239);
  $ascii['o'] = array_merge(range(242, 246), array(240, 248));
  $ascii['u'] = range(249, 252);
  // Código ASCII dos outros caracteres
  $ascii['b'] = array(223);
  $ascii['c'] = array(231);
  $ascii['d'] = array(208);
  $ascii['n'] = array(241);
  $ascii['y'] = array(253, 255);
  foreach ($ascii as $key=>$item) {
    $acentos = '';
    foreach ($item AS $codigo) $acentos .= chr($codigo);
    $troca[$key] = '/['.$acentos.']/i';
  }
  $string = preg_replace(array_values($troca), array_keys($troca), $string);
  // Slug?
  if ($slug) {
    // Troca tudo que não for letra ou número por um caractere ($slug)
    $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
    // Tira os caracteres ($slug) repetidos
    $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
    $string = trim($string, $slug);
  }
  return $string;
}   



require 'GeraCodigoBarra.php';
<?php

class AlunosModel extends DB {
 
    private $cn,$CONDICAO_PADRAO, $AND_CONDICAO_PADRAO;
    private $colunas = array();
    

    private $LOCALID ;
    private $usu_codigo;
    private $usu_nomemae, $usu_nomepai, $usu_nasicmento, $usu_sexo, $usu_nacionalidade; 
    private $usu_uf, $usu_naturalidade, $usu_cpf, $usu_rg, $usu_rgemissor, $usu_cep, $usu_logradouru;
    private $usu_logranumero, $usu_lograbairro, $usu_municipio, $usu_lograuf, $usu_renach, $usu_tipodoc;
    private $usu_numdoc, $usu_catatual, $usu_situacao, $usu_telresid, $usu_telcelular, $usu_telcomercial;
    private $usu_telrecado, $cad_data, $cad_usua, $can_data, $can_usua;    
    
    private $LOCAL;


    public function SetID($param) {
        $this->ALUNO_ID = $this->Prepare($param);
    }
    
    public function __construct() {
        $this->cn = $this->CnCliente();
        
        $LOCAL = $_SESSION['APP_LOCALID'];
        $this->LOCAL   = $LOCAL;
        $this->LOCALID = $LOCAL;

        $this->CONDICAO_PADRAO = " usu_localid = '$LOCAL' AND usu_tipo = 'ALUNO' ";
        $this->AND_CONDICAO_PADRAO = ' AND '.$this->CONDICAO_PADRAO . '';
        
    }
    
    public function VerificaAtivo($ID) {
        $aluno = $this->Seleciona($ID);
        if($aluno->can_data != '') {
            return false;
        }

        return true;
    }

   
    public function Listar($campos = "", $condicao = "", $order = '',$limit = "", $join = '') {
        if($campos != "") {
            $campos = ", ".$campos;
        }
        if($limit != '' ) {
            if($limit == '*') {
                $limit = "";
            } else {
                $limit = "LIMIT $limit";
            }
            
        }
        
        
        if($order != '') {
            $order = "ORDER BY $order";
        }

        if($this->TipoAcesso("usuario")) {
            $ID_USUARIO = $_SESSION['APP_USUID'];
            $condicao = $condicao . " AND (usu_responfinan = '$ID_USUARIO' or usu_id = '$ID_USUARIO')";
        }


         
        if($condicao != '') {

           $this->CONDICAO_PADRAO = "  " .$condicao;
        }



        $selec = "SELECT * $campos FROM sis_usuarios
                  LEFT OUTER JOIN sis_alunosmat on amat_alunoid = usu_id
                  LEFT OUTER JOIN sis_categoriascnh on amat_catcnh = usu_id
                  WHERE $this->CONDICAO_PADRAO  group by usu_id $order $limit  ";
        $selec = $this->ExecQuery($selec);
         
        return $selec;
    }
    
    public function get_array() {
     return $this->result_array();
 }

    //SELECIONA IDS NAO IMPORTA SE É RESPONFIN OU ALUNO
    public function ListaIDS($IDUSUARIO, $cancelados = false) {
        if($cancelados == true) {
            $cancelados = "";
        }else {
            $cancelados = " AND can_data IS NULL  ";
        }
        // SELECIONANDO OS IDS USUÁRIOS
        $query = "
            SELECT * FROM sis_usuarios WHERE (usu_id = '$IDUSUARIO' OR usu_responfinan = '$IDUSUARIO')
            AND usu_localid = '".$this->LOCALID."' $cancelados
        ";

        $select = $this->ExecQuery($query);
        if($select->num_rows > 0) {
            return $select; 
        }

        return false;
        
    }

    public function SelecionaResponFinan($ID) {
        $ID = $this->Prepare($ID);

        $selec = "SELECT * FROM sis_usuarios WHERE usu_id = '$ID'";
        $selec = $this->ExecQuery($selec);
        if($selec->num_rows != 1) {
            return false;
        } else {
            return $selec->fetch_object();
        }
    }
        
    public function Seleciona($ID) {
        $ID = $this->Prepare($ID);

        $cWhere = '';
        if(!$this->TipoAcesso("OPERADOR")) {
            $xID = $_SESSION['APP_USUID'];
            $cWhere = " AND (usu_id = '$xID' or usu_responfinan = '$xID') ";
        }

        $selec = "SELECT * FROM sis_usuarios WHERE usu_id = '$ID'  $cWhere";
        $selec = $this->ExecQuery($selec);
        if($selec->num_rows != 1) {
            return false;
        } else {
            return $selec->fetch_object();
        }
    }

    public function SelecionaFaseAtualModel($ALUNO,$MATRICULA) {
        $ALUNO     = $this->Prepare($ALUNO);
        $MATRICULA = $this->Prepare($MATRICULA);
        $query = "SELECT *
              FROM sis_alunosmatfases
              LEFT OUTER JOIN sis_servicositens ON serviten_id = amf_servitenid
              WHERE amf_alunoid = '$ALUNO'
              AND NOT amf_iniciado IS NULL 
              AND amf_concluido IS NULL
              AND amf_matid = '$MATRICULA' ";
        $fase = $this->ExecQuery($query);
        if($fase->num_rows <= 0) {
            return "Concluído";
        }
        $fase = $fase->fetch_object();
        return $fase->serviten_descricao;

    }
    
    public function SelecionaMatricula($ID) {
        $ID = $this->Prepare($ID);

        $cWhere = "";
        if($this->TipoAcesso("USUARIO")) {
            $xaluno = $_SESSION['APP_USUID'];
            //$cWhere = " AND (amat_alunoid = '$xaluno' || amat_alunoid='$xaluno')";
        }

        $selec = "SELECT *, am.can_data as amCan_data, am.can_motivo as amCan_motivo
                       FROM sis_alunosmat am 
                       LEFT OUTER JOIN sis_tpmatriculas m       ON tpmat_id = amat_tpmat " .
                       #LEFT OUTER JOIN sis_categoriascnh cnh    ON catc_id  = amat_catcnh
                       "WHERE amat_localid = '$this->LOCAL' AND amat_alunoid = '$ID' $cWhere  GROUP BY amat_id";
        $selec = $this->ExecQuery($selec);
        return $selec;

    }    
    
    public function Cadastrar($query) {
        $c = $this->ExecNonQuery($query);

        if($this->GetErro("",false)) {
            return $this->GetErro();
        } else {
            return $c;
        }
        
    }
    

    public function MarcaCancelado($IDALUNO,$TIPO) {


        $TIPO = strtoupper($TIPO);
        
        if($TIPO == "EXC") {
            $campos = array(
                "can_data" => "current_timestamp(),false",
                "can_usua" => $_SESSION['APP_USUID'].",FALSE"
            );
        }

        if($TIPO == "REC") {
            $campos = array(
                "can_data" => "NULL,false",
                "can_usua" => "NULL,FALSE"
            );
        }        

        $this->Update($IDALUNO,$campos);

    }
    
    public function Update($ID,$campos = array()) {
        $ID = $this->Prepare($ID);
        
        $upCxV  = "UPDATE sis_usuarios SET ";
        
        foreach ($campos as $campo => $valor) {
            
            if ( strtoupper(substr($valor,-5)) == "FALSE") {
                $valor =  substr($valor, 0,strlen($valor)-6);
                $upCxV .= " `$campo` = $valor,";
            } else {
                $upCxV .= " `$campo` = '$valor',";    
            }
            
        }
        $upCxV = substr($upCxV,0,-1);
        $upCxV .= " WHERE usu_id = '$ID' ";
        
        return $this->ExecNonQuery($upCxV);
    }
    
    public function UpdateFicha($ID,$campos = array()) {
        $ID = $this->Prepare($ID);
        

        switch (strtolower($campos['tipo'])) {
            case 'a':
                $campos['tipo'] = "ALUNO";
                break;
            
            case 'r':
                $campos['tipo'] = "ALUNOF";
                break;

            case 'ar':
                $campos['tipo'] = "ALUNOFA";
                break;

            default:
                exit('ERRO TIPO INVÁLIDO '. $campos['tipo'] . ' => LINHA ' . __LINE__);
                break;
        }
        
        $ignorarCampos = array("acao","chave","acesso","id");
        $obrigatorios = array(
            "nomecompleto" => "Nome Completo",
            "nomemae" => "Nome da Mãe",
            "nomepai" => "Nome do Pai",
            "nascimento" => "Data de Nascimento",
            "sexo" =>"Sexo",
            "nacionalidade" => "Nacionalidade",
            //"login"         => "Login"
            //"uf" => "UF Nacionalidade",
            //"naturalidade" => "Naturalidade",
            //"cpf" =>"CPF",
            //"rg" => "RG",
            //"rgemissor" => "orgão emissor RG",
            //"rguf" => "UF emissão RG",
            //"cep" => "CEP",
            //"logradouro" => "Logradouro",
            //"logranumero" => "Número do logradouro",
            //"lograbairro" => "Bairro do logradouro",
            //"logramunicipio" => "Município do logradouro",
            //"lograuf" => "UF do logradouro"
        );
        if($_POST['nacionalidade'] != 3 && $_POST['nacionalidade'] != 4) {
            //$obrigatorios = array("uf"=>"Informe UFx")+$obrigatorios;
            //$obrigatorios = array("naturalidade"=>"Informe naturalidade")+$obrigatorios;
        }
        
        

        if($campos['tipo'] == 'ALUNOF') {
            #$obrigatorios['cpf'] = " CPF ";
            #$obrigatorios['rg'] = " RG ";
            $obrigatorios['logradouro'] = "Logradouro";
            $obrigatorios['logranumero'] = "Número do logradouro";
            $obrigatorios['lograbairro'] = "Bairro do logradouro";
            $obrigatorios['logramunicipio'] = "Município do logradouro";
            $obrigatorios['lograuf'] = "UF do logradouro";
        }
        

        

        $upCxV  = "UPDATE sis_usuarios SET ";
        
        foreach ($campos as $campo => $valor) {
            //$campo = "usu_".$campo;

            foreach ($obrigatorios as $obg => $descri) {
                if($campo == $obg) {
                    if($valor == "") {
                        $this->SetErro('Informe ' . $descri);
                        $this->GetErro();                            
                    }
                }
            }            

            if($campo == 'nascimento') {
                $valor = DataDB($valor);
            }
            
            if($valor == '') {
                $valor = "null";
            } else {

                
                $valor = "'$valor'";
            }

            if(!in_array($campo, $ignorarCampos)) {
                $upCxV .= " `usu_$campo` = $valor,";
            }
            
            
        }
        $upCxV = substr($upCxV,0,-1);
        $upCxV .= " WHERE usu_id = '$ID' ";
        $b = $this->ExecNonQuery($upCxV);
        return $b;
    }
    
    
    public function getMunicipioDetranSP($municipio) {
        $municipio = strtoupper($municipio);
        $select = "SELECT * FROM ecnh_municipios WHERE MUNI_CIDADE = '$municipio' ";
        $this->ExecQuery($select);
        $res = $this->result_array();
        
        ///print_r($res);
        $res = $res[0]['muni_codigo'];
        
        $res = substr($res,1,strlen($res)-1);
        echo $res;
    }
    
    
    public function VerificaCampoDuplicado($id_aluno,$campo, $valor) {
        $LOCAL = $_SESSION['APP_LOCALID'];
        $query = "SELECT * FROM sis_usuarios WHERE $campo = '$valor' AND usu_id != '$id_aluno' AND usu_localid = '$LOCAL' ";
        $x = $this->ExecQuery($query);
        
        if($x->num_rows != 0) {
            return true;
        }
        return false;
        
    }
    
}
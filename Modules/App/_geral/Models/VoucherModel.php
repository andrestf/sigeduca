<?php


class VoucherModel extends DB {


	private $LOCALID;

	public function __construct()
	{
		
        $this->cn = $this->CnCliente();
        $this->LOCALID = $_SESSION['APP_LOCALID'];

	}

	public function ListaComAssentos($IDRESPON,$IDEVENTO) {

		$query = "SELECT *, i.cad_data as iCADDATA 
					  FROM evt_ingressos as i 
					  LEFT OUTER JOIN sis_usuarios ON usu_id = $IDRESPON 
					  WHERE evti_responid = '$IDRESPON' and evti_eventoid = '$IDEVENTO' AND evti_localid = '$this->LOCALID' AND (NOT evit_assento IS NULL || evit_assento != '') ";
		$x     = $this->ExecQuery($query);
		if($x->num_rows >= 1) {
			$res = $this->result_array();
			return $res;
		}
		return false;
	}


	public function SelecionaPorIDs($IDINGRESSO,$IDRESPON,$IDEVENTO)  {
		$query = "SELECT *, i.cad_data as iCADDATA 
					  FROM evt_ingressos as i 
					  LEFT OUTER JOIN sis_usuarios ON usu_id = $IDRESPON 
					  WHERE evti_responid = '$IDRESPON' and evti_eventoid = '$IDEVENTO' AND evti_localid = '$this->LOCALID' AND (NOT evit_assento IS NULL || evit_assento != '') 
					  AND evti_id = '$IDINGRESSO'
					  ";


		$x     = $this->ExecQuery($query);
		if($x->num_rows >= 1) {
			$res = $this->result_array();
			return $res[0];
		}

		return false;
	}

}

<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class EventosModel extends DB {

	private $LOCALID;
    public $assentos, $IDEVENTO, $IDRESPON;
    public function __construct() {
        $this->cn = $this->CnCliente();

        $this->LOCALID = $_SESSION['APP_LOCALID'];
    }

    public function Lista($condicao = '') {
    	if($condicao != "") {
    		$condicao = " AND $condicao ";
    	} 
    	$query = "SELECT * FROM evt_eventos WHERE TRUE $condicao AND evt_localid = '".$this->LOCALID."' ";
        $select = $this->ExecQuery($query);


        if ($select->num_rows > 0) {
            return $this->result_array();
        }

        return false;    	
    }

    public function Selecionar($id) {

    	$query = "SELECT * FROM evt_eventos WHERE evt_id = '$id' AND evt_localid = '".$this->LOCALID."' ";
        $select = $this->ExecQuery($query);


        if ($select->num_rows > 0) {
            $x =  $this->result_array();
            return $x[0];
        }

        return false;       	
    }

    public function Editar($id,$campos) {

    	$edts = "";
    	foreach ($campos as $key => $value) {
    		$edts .=  " $key = '$value',  ";
    	}

    	$q = "UPDATE evt_eventos SET $edts evt_id = evt_id WHERE evt_id ='$id' ";
    	$up = $this->ExecNonQuery($q);
		
		if($up->error) {
			return false;
		}

		return true;
    }
    public function Adicionar($object) {

    	$lOK = true;


    	#################################################
    	## INSERT DO EVENTO
    	$campos  = "";
    	$valores = "";

    	foreach ($object as $key => $value) {

    		if($key != "evt_vlrpadrao") {
    			$campos  .= "`$key`, ";
    			$valores .= "'$value', ";
    		}
    	}

		$LOCALID  = $_SESSION['APP_LOCALID'];
		$CAD_USUA = $_SESSION['APP_USUID'];    	
		$CAD_DATA = "CURRENT_TIMESTAMP()";
		$VLRUNIT  = $object['evt_vlrpadrao'];

		//valor vem X.XXX,XX
        $VLRUNIT = str_replace(".", "", $VLRUNIT);
        $VLRUNIT = str_replace(",", ".", $VLRUNIT);
	    //$campos['evt_vlrpadrao'] = $VLRUNIT;

		$this->autocommit(false);
    	$ins = "INSERT INTO evt_eventos ($campos evt_localid, evt_vlrpadrao, cad_data, cad_usua) values ($valores $LOCALID, $VLRUNIT, $CAD_DATA, $CAD_USUA)";
    	$ins = $this->ExecNonQuery($ins,false);
    	if($ins->error) {
	       	$this->roolback();
	       	$x = "Falha ao inserir evento... <BR>" . $ins->error;
            print_r($ins);
        	$lOK = false;
        	return $x;
    	} 

        

		#################################################
    	## MONTANDO OS LUGARES DOS EVENTOS
        if($lOK) {
        	$lOK = true;
        	#############################################################################
        	$EVTID = $ins->insert_id;

	        if($VLRUNIT == "") {
	        	$VLRUNIT = 0;
	        }
	        
	        //valor vem X.XXX,XX
	        #$VLRUNIT = str_replace(".", "", $VLRUNIT);
	        #$VLRUNIT = str_replace(",", ".", $VLRUNIT);

        	$valores = "";
        	$layout = new EventosLayoutController();
        	$layout = $layout->PreparaLugaresInsert($object['evt_layout']);
        	#############################################################################

        	$ins = array();
        	$valores = array();

        	$nMaxInsert = 215;
        	$nRegAtual  = 0;
        	$nInsAtual  = 0;
        	if($layout != "") {



        		$layout = explode(",", $layout);


	            ###############################
            	## PARA CADA ASSENTO NO LAYOUT
	        	$SEQUEN = 1;
	        	$ultimoAssento = end($layout);
	        	$_valores = "";
	        	foreach ($layout as $assento) {
	        		
	        		$assento = trim($assento);
	        		
	        		if($assento != '' && substr($assento, 0,1) != "!") {
		        		$ins[$nInsAtual] = "INSERT INTO evt_assentos (evta_localid, evta_eventoid , evta_cod, evta_hash, evta_sequen, evta_valor, evta_status) VALUES ";
		        		$SEP = ",";

							if($assento == $ultimoAssento) {
			        			$SEP = ";";
			        		}

			        		if( ($nRegAtual % $nMaxInsert) == 0) {
			        			$SEP = ";";
			        		}
			        	
			        	//echo $SEP;
			        	//echo "->" . $assento . " $SEP <br/>";
		        		/**/
			        		$HASH = md5(sha1(sha1(md5($assento.$EVTID)) . sha1(fRand(10)) . md5(time())));
			        		$HASH = substr($HASH, 0,8) . "-" . substr($HASH, 8,4) ."-". substr($HASH, 12,4) ."-". substr($HASH, 16,6) ."-".  substr($HASH, 22,10);
							$_valores = $_valores . "( '$LOCALID', '$EVTID', '$assento', '$HASH', '$SEQUEN',$VLRUNIT,'LIVRE')$SEP" ;	
							$valores[$nInsAtual] = $_valores; 
			        		$SEQUEN++;
						/**/

						
		        		//echo $nRegAtual . "  ";
		        		if( ($nRegAtual % $nMaxInsert) == 0)  {
		        			$_valores = "";
			        		$nInsAtual++;
			        		//$nInsAtual <br/>";
			        	}

		        	//$nRegAtual++;
					}

	        	}
	        	$nTotIngressos = $SEQUEN-1;
	        	## FIM => CADA ASSENTO LAYOUT
        	}

        	$n = 0;

        	foreach ($ins as $insert) {
	        	$inss = $insert . $valores[$n];
                //echo $inss . "<hr>";
	        	$inss = $this->ExecNonQuery($inss);
	           	if($inss->error) {
	           		echo $inss->error;
		    	   	$this->roolback();
		    	    $x = "Falha ao gerar assentos...";
	            	$lOK = false;
	            	return $x;
	            }

	            $n++;
        	}
        	

        	


            ###################################################################################################
            ## ATUALIZANDO INFORMAÇÕES DO EVENTO DE ACORDO COM OS INGRESSOS GERADOS!!!
            if($lOK) {
            	$lOK = true;
            	$up = "UPDATE evt_eventos SET evt_layoutexporta = '*', evt_qtdingressos = $nTotIngressos, evt_statusinterno = 'PRONTO' where evt_id = $EVTID";
	        	$up = $this->ExecNonQuery($up);
	        	if($up->error) {
	        		$up->error;
		    	   	$this->roolback();
		    	   	$x = "Falha ao gerar evento...";
	            	$lOK = false;
	            	return $x;
	            }


	            $this->commit();

	            return 1;


            }
            ## FIM => ATUALIZANDO INFORMAÇÕES EVENTO PARA CADA INGRESSO GERADO
    	} 
    	## FIM => MONTANDO LUGARES EVENTO



        
    }

    
    /**
	* seleciona ingressos disponiveis para o responsavel ou aluno
	* int $IDUSUARIO  = id do responsavel
	* int $IDEVENTO   = id evento
	* bol $CANCELADOS =  se inclui cancelados ou nao!
    */
    public function IngressosUsuario($IDUSUARIO, $IDEVENTO, $CANCELADOS = false) {
        $nTotalIngressos = array();


        ##################################################################
        #INGRESSOS COMPRADOS PELO USUÁRIO
        $nIngressosAdquiridos = 0;
        $IngressosAdquiridos = $this->IngressosAdquiridos($IDUSUARIO,$IDEVENTO);
        if($IngressosAdquiridos) {
            $nIngressosAdquiridos = $IngressosAdquiridos->num_rows;
        }
        $nTotalIngressos['ADQUIRIDOS'] = $nIngressosAdquiridos;



        ##################################################################
        #INGRESSOS UTILIZADOS PELO USUÁRIO
        $nUtilizados = 0;
        if($IngressosAdquiridos) {
            foreach ($IngressosAdquiridos as $ingressos) {
                if($ingressos['evit_assento'] != '') {
                    $nUtilizados++;
                }
            }
        }
         $nTotalIngressos['UTILIZADOS'] = $nUtilizados;

        ##################################################################
        #INGRESSOS DISPONIVEIS PELO USUÁRIO
        $nTotalIngressos['DISPONIVEIS'] = $nIngressosAdquiridos-$nUtilizados;       

        return $nTotalIngressos;

    }



    /** mesma coisa da funcao IngressosUsuario mas somente para reserva de ingressos por parte dos funcionários
    * seleciona ingressos disponiveis para o responsavel ou aluno
    * int $IDUSUARIO  = id do responsavel
    * int $IDEVENTO   = id evento
    * bol $CANCELADOS =  se inclui cancelados ou nao!
    */    
    public function Ingressos($IDUSUARIO, $IDEVENTO, $CANCELADOS = false) {
    	
		$nTotalIngressos = 0;

        $Evento = $this->Selecionar($IDEVENTO);
        

        //IDs Matriculas do evento
        $cMatriculasEvt = $Evento['evt_matricula'];

        //min por aluno/resp
        $nQtdIngressMin = $Evento['evt_qtdingralunomin'];
        
        //max por aluno/resp
        $nQtdIngressMax = $Evento['evt_qtdingralunomax'];

        //pega todos os alunos do responsavel
        $AlunosModel  = new AlunosModel();
        $RESPON       = $AlunosModel->SelecionaResponFinan($IDUSUARIO);
        $TIPO_USUARIO = $RESPON->usu_tipo;

        $nMatriculasAtivas =  0;
        if($TIPO_USUARIO != 'ADMIN') {
            $__nIDS  = $AlunosModel->ListaIDS($IDUSUARIO,$CANCELADOS);

            $nIDS   	 = "";
            foreach ($__nIDS as $ids) {
            	$nIDS .= $ids['usu_id'].",";
            }
            $nIDS = substr($nIDS,0,-1);
            $aIDS = explode(",",$nIDS);



            //verifica quais dos IDS estao com matricula ativa nos cursos do evento!
            $MatriculasModel   = new MatriculasModel();
            $MatriculasAtivas  = $MatriculasModel->SelecionaMatriculasAtivas($aIDS, $cMatriculasEvt, 'amat_alunoid');
            $nMatriculasAtivas = $MatriculasAtivas->num_rows; 
        }

        //Verifica se ja tem ingresso vendido para o responsavel
        $nIngressosAdquiridos = 0;
        $IngressosAdquiridos = $this->IngressosAdquiridos($IDUSUARIO,$IDEVENTO);

        if($IngressosAdquiridos) {
        	$nIngressosAdquiridos = $IngressosAdquiridos->num_rows;
        }


        $nTotalIngressos = array();
        $nTotalIngressos['MAX']               = 0;
        $nTotalIngressos['MIN']               = 0;
		$nTotalIngressos['DISPONIVEIS_GERAL'] = 0;
		$nTotalIngressos['VENDIDOS']          = 0;
		$nTotalIngressos['ADQUIRIDOS']        = 0;
		
		$nTotalIngressos['ADQUIRIDOS'] = $nIngressosAdquiridos;

        $nTotalIngressos['MAX'] = ($nMatriculasAtivas * $nQtdIngressMax) - $nIngressosAdquiridos ;
        if($nTotalIngressos['MAX'] < 0 ) {
        	$nTotalIngressos['MAX'] = 0; }

        //verifica se ja comprou qtd minima
        $nTotalIngressos['MIN'] = ($nMatriculasAtivas * $nQtdIngressMin) - $nIngressosAdquiridos ;
        if($nTotalIngressos['MIN'] < 0 ) {
        	$nTotalIngressos['MIN'] = 0; }
      

        //Ingressos disponiveis para o evento 
        $IngressosEvento      			= $this->IngressosDoEvento($IDEVENTO,false);
        $IngressosDisponiveis 			=  $Evento['evt_qtdingressos'] - $IngressosEvento['VENDIDOS'];
        $nTotalIngressos['DISPONIVEIS_GERAL'] = $IngressosDisponiveis;

        if($TIPO_USUARIO == "ADMIN") {
            $nTotalIngressos['MAX'] = 999;            
            $nTotalIngressos['MIN'] = 0;
        }


        $nTotalIngressos['VENDIDOS']    = $IngressosEvento['VENDIDOS'];;

        return $nTotalIngressos;

    	
    }

    public function IngressosAdquiridos($IDUSUARIO,$IDEVENTO) {
    	$LOCAL = $_SESSION['APP_LOCALID'];
        $nIngressosAdquiridos = 0;
        $QUERY = " SELECT * FROM evt_ingressos WHERE evti_localid = '$LOCAL' AND can_data IS NULL AND evti_responid = '$IDUSUARIO' AND evti_eventoid = '$IDEVENTO'";    	
        $select = $this->ExecQuery($QUERY);


        if ($select->num_rows > 0) {
            return $select;
            
        }

        return false;
    }
	
    public function IngressosDoEvento($IDEVENTO,$BUSCAEVT = true) {
    	$LOCAL = $_SESSION['APP_LOCALID'];
    	$ingressos = array();
    	$ingressos['VENDIDOS'] = 0;
    	$ingressos['TOTAL']    = 0;

    	//vendidos
        $QUERY = " SELECT * FROM evt_ingressos WHERE evti_localid = '$LOCAL' AND can_data IS NULL AND evti_eventoid = '$IDEVENTO' ";    	
        $select = $this->ExecQuery($QUERY);
        $ingressos['VENDIDOS'] = $select->num_rows;

        //
        if($BUSCAEVT) {
        	$Evento = $this->Selecionar($IDEVENTO);
        	$total = $Evento['evt_qtdingressos'];
        	$ingressos['TOTAL'] = $total;
        }

        return $ingressos;
        
    }

    public function ListaReservas($IDEVENTO) {

        $LOCAL = $_SESSION['APP_LOCALID'];
        
        
        $QUERY = " SELECT *, i.cad_data as icad_data FROM evt_ingressos AS i
                   LEFT OUTER JOIN sis_usuarios ON usu_id = evti_responid
                   WHERE evti_localid = '$LOCAL' AND i.can_data IS NULL AND evti_eventoid = '$IDEVENTO' ";
        $select = $this->ExecQuery($QUERY);
        
        if($select->num_rows > 0) {
            return $select;
        }

        return false;
        

    }

    public function ReservaIngressos($IDRESPON,$IDEVENTO,$QTD, $LANCAFINAN = true) {

    		$LOCAL  = $_SESSION['APP_LOCALID'];
    		$USUA   = $_SESSION['APP_USUID'];
            $EVENTO = $this->Selecionar($IDEVENTO);
            $VLR = $EVENTO['evt_vlrpadrao'];
    		$VLRTOTAL = $VLR * $QTD;
            $DESCRICAO = $EVENTO['evt_titulo'];
            $FINAN_ID_TEMP = time().'-'.fRand(5).'-'.fRand(5).'-'.fRand(8).'-'.fRand(8);
           
            $q = "SELECT count(evti_id) as C FROM evt_ingressos";
            $this->ExecQuery($q);
            $count = $this->result_array();          
            $sequen = $count[0]['C'];


            $ins = '';
            for($i= 1; $i <= $QTD; $i++){
                $KEY = sha1(md5(time()*$i));

                $sep = ", ";
                if($i == $QTD) { $sep = ";"; }
                $ins .= "( '$LOCAL', '$sequen', '$IDEVENTO', '$VLR', '$IDRESPON', '$i', '$KEY', CURRENT_TIMESTAMP(), '$USUA', '$FINAN_ID_TEMP' )$sep";
            }

            
                        
            $this->autocommit(false);
    		$QUERY = "INSERT INTO evt_ingressos (evti_localid, evti_sequen, evti_eventoid, evti_valor, evti_responid, evit_cod, evit_key, cad_data, cad_usua, evti_idtemp) VALUES $ins";
    		$up = $this->ExecNonQuery($QUERY);

            $deondeid = $up->insert_id;

            if(!$up) {
                $up->error;
                $this->roolback();
                return false;
            }

            if($LANCAFINAN) {
                $FinanceiroModel = new FinanceiroModel();
                $FINAN = $FinanceiroModel->CriaMovimento($IDRESPON,$VLRTOTAL,date("d/m/Y"),'E',$DESCRICAO,'EVENTO','evt_ingressos',$sequen,true,'CAIXA',11,"PC");
                
                if(!$FINAN) {
                    $up->error;
                    $this->roolback();
                    return false;
                }
            }
            $up = "UPDATE evt_ingressos SET evti_finanid = '$FINAN' WHERE evti_idtemp = '$FINAN_ID_TEMP' "; 
            $up = $this->ExecNonQuery($up);

            $this->commit();
            $ret['QTD']      = $QTD;
            $ret['VLRTOTAL'] = $VLRTOTAL;
            $ret['FINAN']    = $FINAN;
            return $ret;



    }

    ## Reserva(marca ocupado) assentos pelo usuário
    public function OcupaAssentos($IDEVENTO = '',$IDRESPON = '', $ACAO = "OCUPADO") {
        $ret['ok']      = false;
        $ret['ingressos'] = false;
        $ret['assentos'] = false;

        $assentos =  $this->assentos;
        
        if($IDEVENTO == '') {

            $IDEVENTO = $this->IDEVENTO;
        }

        if($IDRESPON == '') {
            $ret['erro']        = '1';
            $ret['mensagem']    = 'ERRO AO SELECIONAR O RESPONSAVEL OU NÃO INFORMADO' ;
            return $ret;            
        }

        $_assentos = "";
        if(is_array($assentos)) {
            foreach ($assentos as $key => $value) {
                $_assentos .= "'".$value."'" . ",";
            }

            $_assentos = substr($_assentos,0,-1);
            $assentos = $_assentos;
        }

        //Verifica se algum assento esta reservado 
        $ocupados = $this->VerificaAssentosOcupados($assentos);
        if($ocupados) {
            $ret['erro']        = '1';
            $ret['mensagem']    = 'Alguns dos assentos selecionados está ocupado!';
            echo json_encode($ret);
            return false;
        }

        $this->autocommit(false);
          $update = " UPDATE evt_assentos SET evta_status = '$ACAO', evta_respon = '$IDRESPON', evta_datapaga = CURRENT_TIMESTAMP() WHERE  evta_cod in ($assentos) AND evta_eventoid = '$IDEVENTO' ";
          //echo "<br/>";
          $x = $this->ExecNonQuery($update);



            //MARCANDO OS ASSENTOS NOS INGRESSOS
            if($x) {    
                $nAssentos = count($this->assentos);
                
                ##########################################################################################################################################
                //selecionando ingressos

                if($ACAO == 'RESERVA') {
                    $select = "SELECT evti_id  from evt_ingressos WHERE (evit_assento = '' || evit_assento IS NULL) AND evti_eventoid = '$IDEVENTO' ";
                } else {
                    $select = "SELECT evti_id  from evt_ingressos WHERE evti_responid = '$IDRESPON' AND (evit_assento = '' || evit_assento IS NULL) AND evti_eventoid = '$IDEVENTO' ";
                }

                    $res = $this->ExecQuery($select);

                    //Quantidade de ingressos disponiveis é menor que a quantidade de assentos
                    if($res->num_rows < $nAssentos) {
                        $this->roolback();
                        $ret['erro']        = '1';
                        $ret['mensagem']    = 'Quantidade de ingressos disponiveis é menor que a quantidade de assentos x';
                        echo json_encode($ret);
                        return false;
                    }


                $ingressos = $this->result_array();
                ###########################################################################################################################################
                


                ############################################################################################################################################
                #repassa um ingresso para cada assento que foi selecionado
                $arr_assentos = $this->assentos;
                $i = 0;
                foreach ($arr_assentos as $assento) {
                    $__ingresso = $ingressos[$i]['evti_id'];
                    $ret['ingressos'] .= $__ingresso . ",";
                    $ret['assentos']  .= $assento . ",";
                    $up = "UPDATE evt_ingressos SET evit_assento = '$assento', evti_datareserva = current_timestamp() WHERE evti_id = '$__ingresso' ";
                    //echo "<br/>";
                    $up = $this->ExecNonQuery($up);
                    if(!$up) {
                        $this->roolback();
                        $ret['erro']        = '1';
                        $ret['mensagem']    = 'ERRO ao atualizar tabelas [INGRESSOS x ASSENTOS]';
                        echo json_encode($ret);                        
                    }
                    $i++;
                }
                if(substr($ret['ingressos'],-1) == ',')
                    $ret['ingressos'] = substr($ret['ingressos'], 0,-1);

                if(substr($ret['assentos'],-1) == ',')
                    $ret['assentos'] = substr($ret['assentos'], 0,-1);

                

                



                $ret['ok']      = true;
                $ret['erro']    = false;
                $ret['mensagem']  = 'Assentos reservados com sucesso!';

            }


        $this->commit();
        echo json_encode($ret);

    }


        //Verifica se algum assento esta reservado 
    function VerificaAssentosOcupados($assentos = "", $IDEVENTO = '') {
        
        if($IDEVENTO == "") {
            $IDEVENTO = $this->IDEVENTO;
            $query = "SELECT * FROM evt_assentos WHERE evta_cod in ($assentos) AND evta_eventoid = '$IDEVENTO' ";
        } else {
            $query = "SELECT * FROM evt_assentos WHERE evta_eventoid = '$IDEVENTO' ";
        }

        $this->ExecQuery($query);
        $res = $this->result_array();

        $ocupado = array();
        foreach ($res as $value) {
            if($value['evta_status'] != 'LIVRE') {
                array_push($ocupado,$value['evta_cod']);
            }
        }

        if(count($ocupado) >= 1) {
            return $ocupado;
        }
        return false;
    }

    function GetNumIngressos($campo,$valor) {
        $q = "SELECT count(evti_id) as NUM FROM evt_ingressos WHERE $campo = '$valor' ";
        $this->ExecQuery($q);
        $res = $this->result_array();          
        $res = $res[0]['NUM'];

        return $res;
    }

}


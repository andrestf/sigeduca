<?php

class BalcaoProdutosModel extends DB {

    private $table, $campolocal, $campoid, $campoorder;
    public function __construct() {
        $this->cn 		  = $this->CnCliente();
        $this->table      = "prod_itens";
        $this->campoid    = "prod_id";
        $this->campolocal = "prod_localid";
        $this->campoorder = "prod_id";
    }
    
    public function Listar($where = '') {

        
       $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) $where ORDER BY  $this->campoorder ASC";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }
    
    
    public function seleciona($id = "", $ret = false) {
        
        if($id == "") {
            exit();
        }

        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) AND $this->campoid = '$id'";
        $query = $this->ExecQuery($query);
      
        if($ret == false) {
        	$retorno = $this->result_array();
        	return $retorno[0];
    	} else {
            $retorno = $this->result_array();
            return $retorno[0];
        }

        
        
        
        

    	//return $retorno;

    }
    
    public function Update($id,$campos = array()) {
        if(!is_array($campos) || $campos == "" || count($campos) == 0) {
            return false;
        }
        
        $upd = "";
        foreach ($campos as $campo => $valor) {

            if( substr($valor, 0,6) == 'false,') {
                $valor = str_replace("false,", "", $valor);
                $upd .= "`$campo` = $valor,";
            } else {

                $upd .= "`$campo` = '$valor',";
            }
        }
        
        $upd = substr($upd,0,-1);
        
        $update = "UPDATE $this->table SET $upd WHERE $this->campoid = '$id' ";

        $up = $this->ExecNonQuery($update);
        if($up) {
            return $up;
        }
        return false;
    }

    public function Inserir($campos) {
        $campos[$this->campolocal] = $_SESSION['APP_LOCALID'];
        
        $campos["cad_data"] = "current_timestamp()";
        $campos["cad_usua"] = $_SESSION['APP_USUID'];
        
        $ins = $this->insert($this->table,$campos);


        return $ins;
    }     
    
    public function Insertxxxxxx($campos = array()) {
        if(!is_array($campos) || $campos == "" || count($campos) == 0) {
            return false;
        }

        $vals   = "";
        $fields = "";
        foreach ($campos as $campo => $valor) {
            $fields .= "$campo,";
            $vals   .= "'$valor',";
        }
        
        $vals   = substr($vals  ,0,-1);
        $fields = substr($fields,0,-1);
        
        $ins= "INSERT INTO $this->table ($fields) VALUES ($vals)";
        if($this->ExecQuery($ins)) {
            return true;
        }
        return false;
    }    


    public function UpdateEstoque($idprod,$tipo,$qtd,$motivo) {
        
        $acao = "NULL";

        if($tipo == "e") {
            $acao = "ADD";
            $qtdx = $qtd;
        }

        if($tipo == "s") {
            $acao = "REM";
            $qtdx = ($qtd)*-1;   
        }

        $campos = array(
            "prod_qtd" => "false,prod_qtd+" . $qtdx
        );

        $this->autocommit(false);
            $x = $this->Update($idprod,$campos);
            if($x) {
                $this->table      = "prod_itenshist";
                $this->campoid    = "prodhist_id";
                $this->campolocal = "prodhist_localid";
                $this->campoorder = "prodhist_id";                

                $campos = array(
                    "prodhist_acao"   => $acao,
                    "prodhist_acaoqtd"   => $qtdx,
                    "prodhist_prodid" => $idprod,
                    "prodhist_motivo" => $motivo
                );
                $xx = $this->Inserir($campos);

                if($xx) {
                    $this->commit();
                    $this->autocommit(true);
                    return true;
                }


            }
        $this->autocommit(true);
        return false;

    }

}

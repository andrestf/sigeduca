<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MatriculasModel
 *
 * @author andre
 */
class MatriculasModel extends DB{
    public function __construct() {
        $this->cn = $this->CnCliente();

        $LOCAL = $_SESSION['APP_LOCALID'];
        $this->CONDICAO_PADRAO = " (tpmat_localid = '$LOCAL' or tpmat_localid = '0') ";
        $this->AND_CONDICAO_PADRAO = ' AND '.$this->CONDICAO_PADRAO . '';

    }

    
    public function SelecionaMatricula($campos = "", $condicao = "") {
        if($campos != "") {
            $campos = ", ".$campos;
        }

            if($condicao != '') {
                $condicao = " AND $condicao";
            }

         $LOCAL = $_SESSION['APP_LOCALID'];
         $selec = "SELECT * $campos FROM sis_alunosmat WHERE amat_localid = '$LOCAL' $condicao";
         $selec = $this->ExecQuery($selec);

         if($selec->num_rows >= 1) {
            return $selec;
         } 
         
         return false;
         
    }


    public function Seleciona($campos = "", $condicao = "") {
        if($campos != "") {
            $campos = ", ".$campos;
        }

         $selec = "SELECT * $campos FROM sis_tpmatriculas WHERE $this->CONDICAO_PADRAO $condicao";
         $selec = $this->ExecQuery($selec);

         return $selec;
    }

    /**
    ID    = ID (INT OU ARRAY DE INTEIROS)  do ALUNO/RESP
    TPMAT = TIPO DA MATRICULA PODE SER VAZIO OU NULO
    */
    public function SelecionaMatriculasAtivas($ID, $TPMAT = NULL, $groupby = "") {
            
        $cWhereTPMAT = '';
        if($TPMAT != '' && $TPMAT != NULL) {
            $cWhereTPMAT = " AND amat_tpmat IN ($TPMAT) " ; 
        }
       
        if(is_array($ID)) {
            $ccWHERE = " IN (".implode(",", $ID).") ";
        } else {
             $ccWHERE = " = '$ID' ";
        }
        $cGroupBy  = '';
        if($groupby != '') {
            $cGroupBy = " group by $groupby ";
        }

        $QUERY = " SELECT * FROM sis_alunosmat WHERE amat_alunoid $ccWHERE AND can_data IS NULL AND amat_situacao = 'INICIADA' $cWhereTPMAT  $cGroupBy";
        $SELEC = $this->ExecQuery($QUERY);

        if($SELEC->num_rows > 0) {
            return $SELEC;
        }

        return false;


    }
}

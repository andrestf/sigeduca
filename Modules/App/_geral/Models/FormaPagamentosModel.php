<?php

class FormaPagamentosModel extends DB {

    private $table, $campolocal, $campoid, $campoorder;
    public function __construct() {
        $this->cn = $this->CnCliente();
        $this->table 	  = "sis_formapgto";
        $this->campoid    = "fpg_id";
        $this->campolocal = "fpg_localid";
        $this->campoorder = "fpg_ordem";
    }

    public function Listar() {
        $query = "SELECT * FROM $this->table WHERE ($this->campolocal = 0 OR $this->campolocal = ".$_SESSION['APP_LOCALID']."  ) ORDER BY  $this->campoorder ASC";
        $query = $this->ExecQuery($query);
        $retorno = $this->result_array();

        return $retorno;
    }


}

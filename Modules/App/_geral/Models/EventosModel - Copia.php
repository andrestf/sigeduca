<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class EventosModel extends DB {


    public function __construct() {
        $this->cn = $this->CnCliente();
    }

    public function Lista() {
    	$query = "SELECT * FROM evt_eventos";
        $select = $this->ExecQuery($query);


        if ($select->num_rows > 0) {
            return $this->result_array();
        }

        return false;    	
    }

    public function Adicionar($object) {

    	$lOK = true;


    	#################################################
    	## INSERT DO EVENTO
    	$campos  = "";
    	$valores = "";

    	foreach ($object as $key => $value) {

    		if($key != "evt_vlrpadrao") {
    			$campos  .= "`$key`, ";
    			$valores .= "'$value', ";
    		}
    	}

		$LOCALID  = $_SESSION['APP_LOCALID'];
		$CAD_USUA = $_SESSION['APP_USUID'];    	
		$CAD_DATA = "CURRENT_TIMESTAMP()";
		$VLRUNIT  = $object['evt_vlrpadrao'];

		//valor vem X.XXX,XX
        $VLRUNIT = str_replace(".", "", $VLRUNIT);
        $VLRUNIT = str_replace(",", ".", $VLRUNIT);
	    //$campos['evt_vlrpadrao'] = $VLRUNIT;

		$this->autocommit(false);
    	$ins = "INSERT INTO evt_eventos ($campos evt_localid, evt_vlrpadrao, cad_data, cad_usua) values ($valores $LOCALID, $VLRUNIT, $CAD_DATA, $CAD_USUA)";
    	$ins = $this->ExecNonQuery($ins,false);
    	if($ins->error) {
	       	$this->roolback();
	       	$x = "Falha ao inserir evento...";
        	$lOK = false;
        	return $x;
    	} 

        

		#################################################
    	## MONTANDO OS LUGARES DOS EVENTOS
        if($lOK) {
        	$lOK = true;
        	#############################################################################
        	$EVTID = $ins->insert_id;

	        if($VLRUNIT == "") {
	        	$VLRUNIT = 0;
	        }
	        
	        //valor vem X.XXX,XX
	        #$VLRUNIT = str_replace(".", "", $VLRUNIT);
	        #$VLRUNIT = str_replace(",", ".", $VLRUNIT);

        	$valores = "";
        	$layout = new EventosLayoutController();
        	$layout = $layout->PreparaLugaresInsert($object['evt_layout']);
        	#############################################################################


        	$ins = array();
        	$valores = array();

        	$nMaxInsert = 215;
        	$nRegAtual  = 0;
        	$nInsAtual  = 0;
        	if($layout != "") {



        		$layout = explode(",", $layout);
#        		echo count($layout);
#	        	exit();
	            ###############################
            	## PARA CADA ASSENTO NO LAYOUT
	        	$SEQUEN = 1;
	        	$ultimoAssento = end($layout);
	        	$_valores = "";
	        	foreach ($layout as $assento) {
	        		
	        		$assento = trim($assento);
	        		
	        		$ins[$nInsAtual] = "INSERT INTO evt_assentos (evta_localid, evta_eventoid , evta_cod, evta_hash, evta_sequen, evta_valor, evta_status) VALUES ";
	        		$SEP = ",";
	        			
						if($assento == $ultimoAssento) {
		        			$SEP = ";";
		        		}

		        		if( ($nRegAtual % $nMaxInsert) == 0) {
		        			$SEP = ";";
		        		}
		        		
		        		//echo $SEP;
		        	//echo "->" . $assento . " $SEP <br/>";
	        		/**/
		        		$HASH = md5(sha1(sha1(md5($assento.$EVTID)) . sha1(fRand(10)) . md5(time())));
		        		$HASH = substr($HASH, 0,8) . "-" . substr($HASH, 8,4) ."-". substr($HASH, 12,4) ."-". substr($HASH, 16,6) ."-".  substr($HASH, 22,10);
						$_valores = $_valores . "( '$LOCALID', '$EVTID', '$assento', '$HASH', '$SEQUEN',$VLRUNIT,'LIVRE')$SEP" ;	
						$valores[$nInsAtual] = $_valores; 
		        		$SEQUEN++;
					/**/

					
	        		//echo $nRegAtual . "  ";
	        		if( ($nRegAtual % $nMaxInsert) == 0)  {
	        			$_valores = "";
		        		$nInsAtual++;
		        		//$nInsAtual <br/>";
		        	}

		        	//$nRegAtual++;

	        	}
	        	$nTotIngressos = $SEQUEN;
	        	## FIM => CADA ASSENTO LAYOUT
        	}

        	$n = 0;

        	foreach ($ins as $insert) {
	        	$inss = $insert . $valores[$n];
	        	$inss = $this->ExecNonQuery($inss);
	           	if($inss->error) {
	           		echo $inss->error;
		    	   	$this->roolback();
		    	    $x = "Falha ao gerar ingressos...";
	            	$lOK = false;
	            	return $x;
	            }

	            $n++;
        	}
        	

        	


            ###################################################################################################
            ## ATUALIZANDO INFORMAÇÕES DO EVENTO DE ACORDO COM OS INGRESSOS GERADOS!!!
            if($lOK) {
            	$lOK = true;
            	$up = "UPDATE evt_eventos SET evt_layoutexporta = '*', evt_qtdingressos = $nTotIngressos, evt_statusinterno = 'PRONTO' where evt_id = $EVTID";
	        	$up = $this->ExecNonQuery($up);
	        	if($up->error) {
	        		echo $up->error;
		    	   	$this->roolback();
		    	   	$x = "Falha ao gerar evento...";
	            	$lOK = false;
	            	return $x;
	            }


	            $this->commit();

	            return 1;


            }
            ## FIM => ATUALIZANDO INFORMAÇÕES EVENTO PARA CADA INGRESSO GERADO
    	} 
    	## FIM => MONTANDO LUGARES EVENTO



        
    }
	

}


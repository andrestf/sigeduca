<?php

class ProdutosController extends Controllers {

    public function index() {
        $this->grid();
    }
    
    public function listar() {
        $this->ValidaNivel(50);
        $PRODUTOS = new ProdutosModel();
        
        $TODOS_PRODUTOS = $PRODUTOS->Listar();
        
        $dados = array(
            "lista_dos_produtos" => $TODOS_PRODUTOS,
        );
        
        $this->RenderView('produtos/listar',$dados);
        
        
    }
    
    public function grid() {
            require_once "Core/GridPHP.php";

            $grid["rowNum"] = 10; // by default 20 
            $grid["sortname"] = 'serv_id'; // by default sort grid by this field 
            $grid["autowidth"] = true; // expand grid to screen width 
            $grid["multiselect"] = true; // allow you to multi-select through checkboxes 
            $grid["form"]["position"] = "center"; 
            $grid["caption"] = "Produtos"; 
            $g->set_options($grid); 

            // you can provide custom SQL query to display data 
            #$g->select_command = "SELECT i.id, invdate , c.name, 
            #                        i.note, i.total, i.closed FROM invheader i 
            #                        INNER JOIN clients c ON c.client_id = i.client_id"; 

            #// you can provide custom SQL count query to display data 
            #$g->select_count = "SELECT count(*) as c FROM invheader i 
            #                        INNER JOIN clients c ON c.client_id = i.client_id"; 



            $g->table = "sis_servicos"; 

            // Coluna Descrição
            $col = array(); 
            $col["title"] = "Servico"; // caption of column 
            $col["name"] = "serv_descricao"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["editable"] = true; 
            $col["hidden"] = false;
           // $col["editoptions"] = array("defaultValue"=>"99","style"=>"border:0");            
           // $col["width"] = "0"; 
            #$col["formatter"] = "date";
            #$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y');
            $cols[] = $col; 

            // Coluna Valor
            $col = array(); 
            $col["title"] = "Valor"; // caption of column 
            $col["name"] = "serv_valor"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)  
            $col["editable"] = true; 
            $col["hidden"] = false;
           // $col["editoptions"] = array("defaultValue"=>"99","style"=>"border:0");            
            $col["width"] = "10"; 
            #$col["formatter"] = "date";
            #$col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'d/m/Y');
            $cols[] = $col; 

            $g->set_columns($cols); 

            $out = $g->render("grid1"); 
            $dados['out'] = $out;

            $this->RenderView('grid_generica2', $dados);
        
        
    }
}

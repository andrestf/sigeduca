<?php
//$UHX23 = '2017-11-15';if(date("Y-m-d") >= $UHX23) {exit("ERRO: entre em contato com o desenvoldedor...");}

class EventosController extends Controllers {

    private $evtid, $step, $act, $idusuario, $idrespon, $respondados, $NUM, $ACAO;
    function __construct() {
        parent::__construct();
        $this->NUM = 999999;
        $this->ACAO = 'OCUPADO';

    }


    public function Aluno() {

        if(!$this->TipoAcesso("OPERADOR")) {
           # $this->RenderView("erros/404");
           # exit();
        }  

        if(!isset($_GET['codigo'])) {
            return;
        }


        $dados = array();
        $dados['OPERADOR'] = $this->TipoAcesso("OPERADOR");
        $dados['NUM'] = $this->NUM;
        $dados['ERRO'] = false;
        $dados['ERROM'] = false;
        

        $ID_ALUNO   = $_GET['codigo'];
        if(isset($_GET['tipo'])) {
            $TIPO_ALUNO = $_GET['tipo']; //a ou r
        } else {
            $TIPO_ALUNO = "a";
        }

        $this->idusuario =  $ID_ALUNO;



        $matriculas  = false;
        $idsusuarios = false;
        $eventos     = false;
        $respon      = false;
        $aluno       = false;

        //SELECIONANDO O USUARIO
        $AlunosModel = new AlunosModel();
        $ALUNO = $AlunosModel->Seleciona($ID_ALUNO);
        $respon = $ALUNO;
        $this->respondados = $respon;
        $lOK = true;
        @$ID_RESPON = $ALUNO->usu_responfinan;
        
        
        if($ID_RESPON != "" and $ID_RESPON != "0" ) {

            
            if($ID_ALUNO != $ID_RESPON) {
                //NAO É RESPONSAVEL
    
                $lOK = false;
                $dados['ERRO'] = 1;
                $dados['ERROM'] = "Cadastro não é um responsável financeiro!";

                $REDIR = new RedirectHelper();
                $REDIR->redir("Eventos","Aluno","codigo=$ID_RESPON&tipo=r");                
                //echo "nao é responsavel O RESPON É $ID_RESPON";

            }
       }
        ######################################################
        $act = '';
        if(isset($_GET['act'])) {
            $act = $_GET['act'];
            $act = strtolower($act);
            $act = ucfirst($act);
            $this->act = $act;


            if($act == 'Reserva') {
                if(!$this->TipoAcesso("OPERADOR")) {
                    exit("ERRO -1");
                }
            }
        }

        if(isset($_GET['step'])) {
            if(!isset($_GET['evt'])) {
                return;
            }
            $this->idrespon = $_GET['codigo'];
            $this->evtid = $_GET['evt'] / $dados['NUM'];
            $step = $this->act."_Step".$this->step  = $_GET['step'];
            $func = "$step";

            #if (class_exists($func)) {
                $this->$func();    
            #} else {
                //$redir = new RedirectHelper();
                //$redir->Redir("Erros","Redirect","s=Evento/Aluno/&codigo=".$ID_ALUNO);
            #}
            
            
            return;
        }


        if($lOK) {
            //SELECIONANDO USUARIOS DO ID INFORMADO PARA VER SE TEM MATRICULAS ATIVAS
            
            $IDS_ALUNOS   = $AlunosModel->ListaIDS($ID_ALUNO);
            
            if($IDS_ALUNOS) {

                foreach ($IDS_ALUNOS as $aluno) {
                    $idsusuarios .=  $aluno['usu_id'].",";
                }
                $idsusuarios = substr($idsusuarios, 0,-1);

                $MATS = new MatriculasModel();
                $MAT = $MATS->SelecionaMatricula("", " amat_alunoid IN ($idsusuarios) AND amat_situacao = 'INICIADA' AND can_data IS NULL " );
                

                if($MAT) {
                    foreach ($MAT as $key) {
                        $matriculas .= $key['amat_tpmat'] . ",";
                    }
                
                    $matriculas = substr($matriculas, 0,-1);


                    //SELECIONANDO OS EVENTOS DISPONIVES PARA O ALUNO
                    $EventosModel = new EventosModel();
                    $eventos = $EventosModel->Lista(" (evt_matricula IN ($matriculas) OR evt_matricula = '' OR evt_matricula IS NULL )");


                    
                }//IF MAT
            }//IF IDS_ALUNOS
        }//IF lOK

        
        $dados['CODIGO'] = $ID_ALUNO;
        $dados['TIPO']   = $TIPO_ALUNO;
        $dados['MATRICULAS'] = $matriculas;
        $dados['EVENTOS']    = $eventos;
        $dados['IDS']        = $idsusuarios;
        $dados['RESPON']     = $aluno;


        
        $this->RenderView('eventos/alunos/index',$dados);  
    }

    function ListaReservas() {
        $dados = array();

        if(!isset($_GET['evt'])) {
            exit();
        }

        $IDEVT = $_GET['evt'];

        $EventosModel = new EventosModel();
        $dados['INGRESSOS'] = $EventosModel->ListaReservas($IDEVT);
        $dados['OPERADOR'] = $this->TipoAcesso("OPERADOR");

        $this->RenderView('eventos/listareservas',$dados);
    }

    function Index() {
    	if(!$this->ValidaNivel(50)) {
    		return;
    	}
    	$dados = array();

    	$Eventos = new EventosModel();
    	$Eventos = $Eventos->Lista();

    	$dados['eventos'] = $Eventos;
        $dados['OPERADOR'] = $this->TipoAcesso("OPERADOR");

    	$this->RenderView('eventos/index',$dados);
    }




    function Editar() {
        if(!$this->ValidaNivel(50)) {
            return;
        }

       
        $dados = array();

        $dados['ret'] = "";
        //salvar dados do editar
        if(isset($_POST['idEdit']) && $_POST['idEdit'] != '') {

            $evt = $_POST['idEdit'];
            $campos['evt_titulo']           = $_POST['titulo'];

            ##      
            $ini0                           = $_POST['dataini'];
            $campos['evt_dataini']          = DataDB($ini0) ." ". substr($ini0, 11,6) . ":00";

            ##
            $fim0                           = $_POST['datafim'];
            $campos['evt_datafim']          = DataDB($fim0) ." ". substr($fim0, 11,6) . ":00";



            $iniLimite0                   = $_POST['datalimiteini'];
            $campos['evt_datalimiteini']  = DataDB($iniLimite0) ." ". substr($iniLimite0, 11,6) . ":00";
            
            $fimLimite0                   = $_POST['datalimitefim'];
            $campos['evt_datalimitefim']  = DataDB($fimLimite0) ." ". substr($fimLimite0, 11,6) . ":00";



            $campos['evt_descricao']        = $_POST['descricao'];
            $campos['evt_qtdingralunomin']  = $_POST['ingressosalunomin'];
            $campos['evt_qtdingralunomax']  = $_POST['ingressosalunomax'];
            $campos['evt_statuspublico']    = $_POST['statuspublico'];

            
            $vlr = $_POST['valorunit'];
            $vlr = str_replace(".", "", $vlr); 
            $vlr = str_replace(",", ".", $vlr);
            $campos['evt_vlrpadrao']        = $vlr;
            
            $campos['evt_matricula']        = "";
            if(isset($_POST['cursos'])) {
                $campos['evt_matricula']    = implode(",",$_POST['cursos']);    
            }

            $campos['evt_matricula'] = str_replace(" ", "", $campos['evt_matricula']);

            $Eventos = new EventosModel();
            $lOK = $Eventos->Editar($evt,$campos);

            if($lOK) {
                $dados['ret'] = "<div class='alert alert-success'>Evento editado com sucesso!</div>";
            } else {
                $dados['ret'] = "<div class='alert alert-error'>Erro ao editar evento!</div>";
            }

            
        }

        $id = $_GET['evt'];
        ##evento
        $Eventos = new EventosModel();
        $dados['evento'] = $Eventos->Selecionar($id);
        
        ## Layouts
        $Layouts = new EventosLayoutModel();
        $dados['layouts'] = $Layouts->Lista();


        ## Cursos
        $Cursos = new MatriculasModel();
        $dados['cursos'] = $Cursos->Seleciona();


        $dados['acao'] = "EDITAR";
        $dados['title'] = "Editar Evento";

        #######################################################################################
        //EVENTO + LAYOUT
        $IDEVT = $_GET['evt'];
        $EventosLayoutController = new EventosLayoutController();             
        $Layout = $EventosLayoutController->MontaLayoutCompleto($IDEVT);
        $dados['layout'] = $Layout;



        $dados['RESERVA_LIBERADA'] = true;
        $IDRESPON = 0;
        //INGRESSOS DISPONIVEIS PARA O USUÁRIO
        if($this->TipoAcesso("OPERADOR")) {
            $IDRESPON = $_SESSION['APP_USUID'];
        }           
        $ingressos = $Eventos->IngressosUsuario($IDRESPON, $IDEVT);
        $dados['ingressos'] = $ingressos;

        //Ingressos disponiveis para o evento 
        $Evento                = $Eventos->IngressosDoEvento($IDEVT,false);
        $IngressosDisponiveis           =  $dados['evento']['evt_qtdingressos'] - $Evento['VENDIDOS'];
        $nTotalIngressos['DISPONIVEIS_GERAL'] = $IngressosDisponiveis;

        $dados['ACAO'] = 'RESERVA';
        if($IDRESPON == 2 || $IDRESPON == 1) {
            $dados['ACAO'] = 'RESERVA';
            $this->ACAO = 'RESERVA';
            //$dados['ingressos']['ADQUIRIDOS']
            //$dados['ingressos']['UTILIZADOS']
            $dados['ingressos']['DISPONIVEIS'] = $IngressosDisponiveis;
            $_GET['codigo'] = $IDRESPON;
            $_GET['evt']    = $IDEVT*$this->NUM;
            $dados['evtid'] = $IDEVT;
        }
        #######################################################################################
        
        $this->RenderView('eventos/novo',$dados);           

        RETURN;

    }

    function Novo() {
    	if(!$this->ValidaNivel(50)) {
    		return;
    	}

    	$dados = array();
         $dados['ret'] = "";

    	if(isset($_POST['continuar'])) {
			$campos['evt_titulo'] 			= $_POST['titulo'];

			##		
			$ini0				 			= $_POST['dataini'];
			$campos['evt_dataini'] 			= DataDB($ini0) ." ". substr($ini0, 11,6) . ":00";

			##
			$fim0 							= $_POST['datafim'];
			$campos['evt_datafim'] 			= DataDB($fim0) ." ". substr($fim0, 11,6) . ":00";

			$campos['evt_descricao'] 		= $_POST['descricao'];
			$campos['evt_qtdingralunomin'] 	= $_POST['ingressosalunomin'];
			$campos['evt_qtdingralunomax'] 	= $_POST['ingressosalunomax'];
			$campos['evt_vlrpadrao'] 		= $_POST['valorunit'];
			
            $iniLimite0                   = $_POST['datalimiteini'];
            $campos['evt_datalimiteini']  = DataDB($iniLimite0) ." ". substr($iniLimite0, 11,6) . ":00";
            $fimLimite0                   = $_POST['datalimitefim'];
            $campos['evt_datalimitefim']  = DataDB($fimLimite0) ." ". substr($fimLimite0, 11,6) . ":00";

			$campos['evt_matricula'] 		= "";
			if(isset($_POST['cursos'])) {
				$campos['evt_matricula'] 	= implode(",",$_POST['cursos']);	
			}

            $campos['evt_matricula'] = str_replace(" ", "", $campos['evt_matricula']);

			$campos['evt_layout'] 			= $_POST['layout'];
			$campos['evt_qtdingressos']		= 0;

            $campos['evt_statuspublico']    = $_POST['statuspublico'];


			$Eventos = new EventosModel();
			$lOK = $Eventos->Adicionar($campos);

			if($lOK == 1) {
				header('location: /index.php?route=eventos');
                return;
			} else {
				//
				ECHO $dados['erro'] = $lOK;
                EXIT();
			}


    		
    	}

        $dados['evento']['evt_matricula'] = '';
        $dados['evento']['evt_titulo'] = '';
        $dados['evento']['evt_descricao'] = '';
        $dados['evento']['evt_qtdingralunomin'] = '';
        $dados['evento']['evt_qtdingralunomax'] = '';
        $dados['evento']['evt_vlrpadrao'] = '';
        $dados['evento']['evt_matricula'] = '';
        $dados['evento']['evt_dataini'] = '';
        $dados['evento']['evt_datafim'] = '';
        $dados['evento']['evt_datalimiteini'] = '';
        $dados['evento']['evt_datalimitefim'] = '';
        $dados['evento']['evt_statuspublico'] = '';

    	## Layouts
    	$Layouts = new EventosLayoutModel();
    	$dados['layouts'] = $Layouts->Lista();


    	## Cursos
    	$Cursos = new MatriculasModel();
    	$dados['cursos'] = $Cursos->Seleciona();

        $dados['title'] = "Cadastrar Novo Evento ";
        $dados['acao'] = "CADASTRAR";

    	//$dados['x'] = $x;
    	$this->RenderView('eventos/novo',$dados);

        RETURN;	

    }


    ##############################################################
    ## RESERVA DE INGRESSOS (RESTRITO)
    function Reserva_Step2() {
        $KEY = sha1(md5(time()*2.32));
        $_SESSION['APPEVT_EVENTO']  = $this->evtid;
        $_SESSION['APPEVT_RESPON']  = $this->idrespon;
        $_SESSION['APPEVT_KEY']     = $KEY;

        
        
        $EventosModel = new EventosModel();
        $Ingressos    = $EventosModel->Ingressos($this->idrespon,$this->evtid,false);

        $dados['INGRESSOS'] = $Ingressos;
        $dados['RESPONID']  = $this->idrespon;
        $dados['RESPON'] = $this->respondados;
        $dados['EVENTO'] = $EventosModel->Selecionar($this->evtid);
        $dados['EVTID']  = $this->evtid;


        $this->RenderView('eventos/alunos/reserva',$dados);
        

    }

    ## RESERVA DE INGRESSOS (RESTRITO)
    function Reserva_Step3() {

        $CODIGO = $_POST['codigo'];
        $EVENTO = $_POST['evt'];
        $QTD    = $_POST['q'];

        $EventosModel = new EventosModel();
        $INGRESSOS    = $EventosModel->Ingressos($CODIGO,$EVENTO,false);
        //print_r($INGRESSOS);

        $lOK = true;
        $RET['erro'] = "0";

        
        if($QTD < $INGRESSOS['MIN']) {
            $RET['erro'] = "1";
            $RET['mensagem'] = "Quantidade mínima é inválida!";
            $lOK = false;
        }


        if($QTD > $INGRESSOS['MAX']) {
            $RET['erro'] = "1";
            $RET['mensagem'] = "Quantidade máxima é inválida!";
            $lOK = false;
        }


        if($QTD > $INGRESSOS['DISPONIVEIS_GERAL']) {
            $RET['erro'] = "1";
            $RET['mensagem'] = "Quantidade de ingressos não disponível";
            $lOK = false;
        }


        if($lOK) {
            $LANCAFINAN = true;
            $reserva = $EventosModel->ReservaIngressos($CODIGO,$EVENTO,$QTD,$LANCAFINAN);
            if($reserva) {
                $RET['QTD'] = $reserva['QTD'];
                $RET['VLRTOTAL'] = $reserva['VLRTOTAL'];
                $RET['FINAN'] = $reserva['FINAN'];
                $RET['mensagem'] = "Ingressos reservados com sucesso!";
                $linkRecibo = permalink('alunos/GeraRecibo/&movimento='.$RET['FINAN'].'&codigo='.$_GET['codigo']);
                $RET['mensagemRet'] = '
                        <div class="alert alert-success">Ingressos reservados com sucesso! </div>
                        <a href="'.$linkRecibo.'" target="_rec" class="btn btn-primary"> <i class="fa fa-print"></i> Recibo</a> &nbsp;
                        <a href="javascript: history.back(-1)" class="btn btn-default"> <i class="fa fa-history"></i> Voltar</a> &nbsp;';
            } else {
                $RET['erro'] = "1";
                $RET['mensagem'] = "FALHA";
            }
        }
        
        echo json_encode($RET);
        
        
    }


    ########################################################################
    ## RESERVA DE ASSENTOS PELO USUÁRIO
    public function Getassentos_Step1() {
        $dados = array();
        

        //EVENTO + LAYOUT
        $IDEVT = $this->evtid;
        $EventosLayoutController = new EventosLayoutController();             
        $Layout = $EventosLayoutController->MontaLayoutCompleto($IDEVT);
        $dados['layout'] = $Layout;




        //INGRESSOS DISPONIVEIS PARA O USUÁRIO
       $IDRESPON = $this->idrespon;
       $EventosModel = new EventosModel();
       $EVENTO = $EventosModel->Selecionar($IDEVT);

       $dados['RESERVA_LIBERADA'] = true;
       $dados['ACAO'] = 'OCUPADO';

       if(!$this->TipoAcesso("OPERADOR")) {
            if( date("Y-m-d G:i:s") <=  date("Y-m-d G:i:s", strtotime($EVENTO['evt_datalimiteini']))) {
                $dados['RESERVA_LIBERADA'] = false;
            }
       }

       $ingressos = $EventosModel->IngressosUsuario($IDRESPON, $IDEVT);
       $dados['ingressos'] = $ingressos;

            $this->RenderView("eventos/alunos/reserva_assentos",$dados);

        
    }

    ########################################################################
    ## CONFIRMA RESERVA DE ASSENTOS PELO USUÁRIO
    public function Getassentos_Step2() {
        $dados = array();
        $this->ACAO = "OCUPADO";

        if(isset($_GET['m'])) {
            if($_GET['m'] == 'RESERVA' && $this->TipoAcesso("OPERADOR")) {
                $this->ACAO = 'RESERVA';
            }
        }

        
        $IDEVT    = $this->evtid;
        $IDRESPON = $_GET['codigo'];

        //ASSENTOS 
        $aASSENTOS = $_POST['assentos'];
        $nASSENTOS = $_POST['nassentos'];

        $EventosModel = new EventosModel();
        $EventosModel->assentos = $aASSENTOS;
        $EventosModel->IDEVENTO = $IDEVT;
        $EventosModel->IDRESPON = $_GET['codigo'];



        $EventosModel->OcupaAssentos($IDEVT, $IDRESPON, $this->ACAO);


        
    }



    // impressão do vouchers

    //lista os ingressos com assentos pra impressão
    public function Voucher_Step1($impressao = false) {
        $dados = array();
        $view = "eventos/alunos/voucher_lista";
        

        $IDEVT = $this->evtid;
        $EventosModel = new EventosModel();       
        $EVENTO = $EventosModel->Selecionar($IDEVT);
        $dados['evento'] = $EVENTO;
        
        $VoucherModel = new VoucherModel();
        $Vouchers     = $VoucherModel->ListaComAssentos($_GET['codigo'],$IDEVT);
        $dados['vouchers'] = $Vouchers;
        $dados['num'] = $this->NUM;

        if($impressao) {
            $view = "eventos/alunos/voucher_imprimir";
        }

        if($impressao) {
            $IDINGRESSO = $_GET['voucher']/$this->NUM;
            $dados['voucher'] = $VoucherModel->SelecionaPorIDs($IDINGRESSO,$_GET['codigo'],$IDEVT);
            
            $this->RenderView($view, $dados, "public/header", false, false);
        } else {
            $this->RenderView($view,$dados);
        }
        
        
        
    }

    //impressão do voucher individual
    public function Voucher_Step2() {
        $this->Voucher_Step1(true);
    }

}
<?php

class BalcaoProdutosController extends BalcaoController {

    private $ProdutosModel;
    public function __construct()
    {
        parent::__construct();

        if(!$this->TipoAcesso("operador")) {
            exit();
        }

        $this->ProdutosModel = new BalcaoProdutosModel();


    }

    public function index(){
        $this->RenderView("balcao/produtos/gerenciar");
        
    }

    public function Busca() {
        $ProdutosModel = $this->ProdutosModel;

        $wWhere = "";
        if($_GET['nome'] != '') {
            $nome = $ProdutosModel->prepare($_GET['nome']);
            $wWhere .= " AND prod_nome like '%$nome%' ";
        }
        
        if($_GET['codigo'] != '') {
            $codigo = $ProdutosModel->prepare($_GET['codigo']);
            $wWhere .= " AND prod_id = '$codigo' ";
        }

        if($_GET['situacao'] != '') {
            $situacao = $ProdutosModel->prepare($_GET['situacao']);
            $wWhere .= " AND prod_situacao = '$situacao' ";
        }

        $produtos = $ProdutosModel->Listar($wWhere);

        $html = "";
        if($produtos) {
            foreach ($produtos as $produto) {
                $link = " <a href='".permalink('balcao','produtos','action=editar&produto='.$produto['prod_id'])."'> ";
                $linkE = "</a>";

                $html .= "<tr>";
                    $html .= "<td> $link ".$produto['prod_id']              ." $linkE </td>";
                    $html .= "<td> $link ".$produto['prod_situacao']        ." $linkE </td>";
                    $html .= "<td> $link ".$produto['prod_nome']            ." $linkE </td>";
                    $html .= "<td> $link ".$produto['prod_qtd']             ." $linkE </td>";
                    $html .= "<td> $link R$ ".$produto['prod_vlrvenda']     ." $linkE </td>";
                $html .= "</tr>";
            }       
        } else {
                $html .= "<tr>";
                    $html .= "<td colspan='5'>nenhum resultado.</td>";
                $html .= "</tr>";
        }

        ########################################################################################################
        $useJson = false;
        if(isset($_GET['json'])) {
            $useJson = true;
        }
        $json = '{
                "results": [';
        if($produtos) {
            $x = 0;
            foreach ($produtos as $produto) {
                $x++;
                $json .= '{"id" : '.$produto['prod_id'].', "text" : "'.$produto['prod_nome'] .'", "preco" : "'.$produto['prod_vlrvenda'].'", "qtd" : '.$produto['prod_qtd'].' }';

                if($x < count($produtos)) {
                    $json .= ",";
                }
            }
        }

        $json .= '  ],
                "pagination": {
                    "more": false
                }
            }';
        ########################################################################################################
        if($useJson) {
            echo $json;
        } else {
            echo $html;
        }
        //$this->RenderView("balcao/produtos/ficha");
    }


    public function Save() {
       
        $camposIgnore = array("nextAct","id");

        $campos = array();
        foreach ($_POST as $key => $value) {
            if (!in_array($key, $camposIgnore) ) {
                $campos['prod_'.$key] = $value;
            }
        }
        
        $campos['prod_vlrvenda'] = str_replace(".", "", $campos['prod_vlrvenda']);
        $campos['prod_vlrvenda'] = str_replace(",", ".", $campos['prod_vlrvenda']);

        $ID_PRODUTO = $_POST['id'];
        //
        if( $_POST['nextAct'] == "editar") {
            $row = 0;
            $upd = $this->ProdutosModel->Update($ID_PRODUTO,$campos);
            $row = $upd->affected_rows;
            header('location: index.php?route=balcao/produtos/&action=index&s='.$ID_PRODUTO."&a=edit&r=".$row);
            
        }

        if($_POST['nextAct'] == "adicionar" ) {
            $ins = $this->ProdutosModel->Inserir($campos);
            $lastId = $ins->insert_id;
            header('location: index.php?route=balcao/produtos/&action=index&s='.$lastId."&a=add");
        }
        //print_r($campos);
    }

    public function Novo() {
        $datas =array();
        $datas['TituloFicha'] = "Adicionar Produto"; 

        $newProd = $this->ProdutosModel->seleciona(-1,true);
        $datas['produto'] = $newProd;
        $datas['nextAct'] = "adicionar";
        print_r($datas['produto']); 
        
        $this->RenderView("balcao/produtos/ficha",$datas);
    }


    public function Editar() {

        if (!isset($_GET['produto'])) {
            header("location: /balcao/produtos/&action=index");
            exit();
        };
        $CODIGO_PRODUTO = $_GET['produto'];

        $datas = array();
        $datas['TituloFicha'] = "Editar Produto";
        $datas['produto'] = $this->ProdutosModel->seleciona($CODIGO_PRODUTO);
        $datas['nextAct'] = "editar";
        $datas['produtoID'] = $datas['produto']['prod_id'];

        $this->RenderView("balcao/produtos/ficha",$datas);
    }

    public function AltEstoque() {
        $idprod = $_POST['produto'];
        $tipo = $_POST['tipo'];
        $qtd = $_POST['qtd'];
        $motivo = $_POST['descri'];

        if($this->ProdutosModel->UpdateEstoque($idprod,$tipo,$qtd,$motivo)) {
            echo "1";
            return true;
        }

        echo "0";
        return false;
    }

}
<?php

class BalcaoVendasController extends BalcaoController {

    function __construct()
    {
        parent::__construct();
        if(!$this->TipoAcesso("operador")) {
            exit();
        }

    }

    public function nova(){
		$datas = array();
		$this->RenderView("balcao/vendas/nova",$datas);

    }
} 
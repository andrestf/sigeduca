<?php

class VeiculosController extends Controllers {

    public function index() {
        $this->listar();
    }

    public function listar() {
        $this->ValidaNivel(50);
        $VEICULOS = new VeiculosModel();

        $TODOS_VEICULOS = $VEICULOS->Listar();

        $dados = array(
            "lista_dos_veiculos" => $TODOS_VEICULOS,
        );

        $this->RenderView('veiculos/listar',$dados);


    }

    public function cadastrar() {
        $this->ValidaNivel(50);
        $dados = array();
        $this->RenderView('veiculos/cadastrar',$dados);
    }
}

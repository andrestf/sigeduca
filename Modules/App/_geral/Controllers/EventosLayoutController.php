<?php
//ALICE ATRAS DO ESPELHO - TEATRO - 07/07/2018
//$UHX23 = '2017-11-15';if(date("Y-m-d") >= $UHX23) {exit("ERRO: entre em contato com o desenvoldedor...");}

class EventosLayoutController extends Controllers {

    public $IS_DEMO, $ID_EVENTO;
    function __construct() {
        parent::__construct();

        $this->IS_DEMO = true;

        return $this;
    }
    public function MontaLaySimplesByGet() {
        $this->IS_DEMO = true;
        $id = $_GET['id'];
        $titulo = '';
        $print = $_GET['print'];

        $l = $this->MontaLaySimples($id);

        if($print == 't') {
            echo $l;
        } else {
            return $l;
        }
    }

    public function PreparaLugaresInsert($id_layout) {
        $lay = new EventosLayoutModel();
        $lay->pk = "evtlay_cod";
        $lay = $lay->Seleciona($id_layout);
        

        $lays = array();
        $retorno = "";
        foreach ($lay as $__lay) {
            $lay = $__lay['evtlay_layout'];    

        
            $assentos = explode(";",$lay);
            

            foreach ($assentos as $assento) {
                if(substr($assento,0,1) !== '!') {
                    $retorno .= $assento.",";
                }
            }
            $retorno = str_replace(",,", "", $retorno);

            if(substr($retorno, -1) == ",") {
                $retorno = substr($retorno, 0,-1);
            }

        }


        return $retorno;
    }

    public function getTipoAssento($assento) {

        $ini = substr($assento, 0,1);
        switch ($ini) {
            case '!': return ""; break;
            case '*': return "deficiente"; break;
            case '@': return "idoso"; break;
            case '+': return "acompanhate"; break;
            case '=': return "obeso"; break;
            case '-': return "reservado"; break;
            default:
                return "Assento Normal";
                break;
        }
    }


    public function MontaLayoutCompleto($IDEVENTO) { 

        $this->IS_DEMO   = false;
        $this->ID_EVENTO = $IDEVENTO;





        $EventosModel  = new EventosModel();
        $evento        = $EventosModel->Selecionar($IDEVENTO);
        
        $COD_LAYOUT    = $evento['evt_layout'];

        $l = $this->MontaLaySimples($COD_LAYOUT);

        return $l;


    }

    public function MontaLaySimples($id,$titulo = "") {
#       !n  Espaço
#       !#  Linha
#       *   Assentos deficiente fisico
#       @   Assentos Idosos
#       +   Assentos Acompanhate
#       =   Assento Obeso
#       -   Assento Reservado
#       !p  Palco

        $lay = new EventosLayoutModel();
        $lay->pk = "evtlay_cod";
        $lay = $lay->Seleciona($id);


        $content = array();
        $tabs = "<div class='tab-asec'>";
        $i = 0;
        $tabs .= "<div class='tab-titles'>";
        foreach ($lay as $layout) {
                        $i++;
            if($layout['evtlay_descri'] == '') {
                $layout['evtlay_descri'] = "PADRÃO";
            }

            $c = '';
            if($i == 1) {
                $c = "tab-title-active";
            }
            $tabs .= "<div class='tab-title $c' data-target='tab-$i'>".$layout['evtlay_descri']."</div>";

            //assentos
            $content[$i] = $layout['evtlay_layout'];

        }
        $tabs .= "</div>";

        $y = 0;
        $tabs .= "<div class='tab-content'>";
        foreach ($content as $contents) {
            $y++;
            $c = '';
            if($y == 1) {
                $c = "tab-content-item-active";
            }
            $tabs .= "<div class='tab-content-item $c' id='tab-$y' >";
            $__assentos = explode(";",$contents);
            $tabs .= $this->MontaLugares($__assentos);
            $tabs .="</div>";
        }
        $tabs .= "</div>";



        $tabs .= "</div>";


        $script = '
        <script>
$(".tab-title").on("click", function() {
    
    tabTarget = $(this).attr("data-target");
    
    $(".tab-title").removeClass("tab-title-active");
    $(".tab-content-item").removeClass("tab-content-item-active");

    $(this).addClass("tab-title-active");
    $("#"+tabTarget).addClass("tab-content-item-active");
})
</script>
        ';

        $tabs = $tabs . $script;

        //echo $tabs;
        

        
        
        




        

        //$out .= $this->MontaLugares();

        return $tabs;

    }

    public function MontaLugares($assentos) {
        $out = "";
        $IDEVENTO = $this->ID_EVENTO;

        $EventosModel = new EventosModel();
        $OCUPADOS = array();

        if(!$this->IS_DEMO) {
            $nOCUPADOS = $EventosModel->VerificaAssentosOcupados('',$IDEVENTO);
            if($nOCUPADOS) {
                $OCUPADOS = $nOCUPADOS;
            }
        }

        $linha = 1;
        foreach ($assentos as $assento) {
            $assento = trim($assento);
            //espaço multiplicados
            if(substr($assento,0,1) == '!') {
                
                $code = substr($assento, 1,1);


                if($code == "n" OR $code == "%") {
                    $repetir = 1;
                    $len = strlen($assento);    
                    if($len != 2 AND $code == 'n' ) {
                        $repetir = substr($assento, 2,$len);
                    }
                    $demo = '';
                    if($this->IS_DEMO) {
                        $demo = "-demo";
                    }

                    $espaco = '';
                    if($code == "%" ){
                        $espaco = '-meio';
                    }

                    $x = "<span class='lugar$demo espaco$demo$espaco' ></span>";
                    $out .= str_repeat($x, $repetir);           
                }

                if($code == "#") {
                    $out .= "<div style='clear:left;'></div>";
                    $linha++;
                }
                
                if($code == "p") {
                    $out .= "<div class='palco'> PALCO </div>";
                }

                $assento = '';
            }

            if($assento != '') {
                $class = $this->getTipoAssento($assento);
                
                if ($this->IS_DEMO) {
                    $out .= "<span class=''><div class='lugar-demo assento-demo $assento $class' data-title='".strtoupper($this->LimpaAssentos($assento,1))."' title='".ucfirst($class)." ".strtoupper($this->LimpaAssentos($assento))."'></div></span>";    
                } else {

                    //$OCUPADOS = array("ta17","ta15","ta13","ta11");
                    
                    if(in_array($assento, $OCUPADOS)) {
                        $class .= " ocupada ";
                        $out .= "<span class=''><div class='lugar assento $class' title='Assento Ocupado'></div></span>";                            
                    } else {
                        //escolha de lugares
                        $img = 'chair01.png';
                        $out .= "<span class=''><div class='lugar assento $class' id='".$assento."' data-title='".strtoupper($this->LimpaAssentos($assento,1))."' title='".ucfirst($class)." ".strtoupper($this->LimpaAssentos($assento))."'></div></span>";    
                    }
                }
                
            }
            

        }      

        return $out;        
    }


    public function LimpaAssentos($assento, $n = 0){

        $arr = array("*","@","+","=","-");
        $assento = str_replace($arr,"",$assento);
        
        if($n > 0)
            return substr($assento,$n,strlen($assento));
        
        return $assento;
    }

}
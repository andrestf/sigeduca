<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


class BalcaoController extends Controllers {

    function __construct()
    {
        parent::__construct();

        if(!$this->TipoAcesso("operador")) {
            exit();
        }
    }

    public function index(){
         $this->RenderView("balcao/dashboard");
    }


    public function vendas() {
        require_once 'balcao/BalcaoVendasController.php';
        //echo "e";
        if(!isset($_GET['action'])) {
            $action = "index";
        } else {
            $action = $_GET['action'];
        }
        $x = new BalcaoVendasController();
        $x->$action();
    }


    public function produtos() {
        
        /* */
        require_once 'balcao/BalcaoProdutosController.php';
        //echo "e";
        if(!isset($_GET['action'])) {
            $action = "index";
        } else {
            $action = $_GET['action'];
        }
        $x = new BalcaoProdutosController();
        $x->$action();
        /**/
    }    

}

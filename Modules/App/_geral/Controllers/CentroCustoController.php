<?php 
class CentroCustoController extends Controllers {

    function __construct() {
        parent::__construct();

    }

    public function Add() {
    	echo '
				<div class="modal-header">Adicionar Centro de Custo</div>    	
				<div class="modal-body"></div>    	
				    <div class = "row">
				        <div class="col-sm-12 col-lg-12 col-md-12">
							<form action="//categorias_movimentacoes_financeiras/adicionar/?modal=true&amp;tipo=D" role="form" id="CategoriasMovimentacoesFinanceiraAdicionarForm" method="post" accept-charset="utf-8">
							<div style="display:none;">
								<input type="hidden" name="_method" value="POST"/></div>
								<input type="hidden" name="data[CategoriasMovimentacoesFinanceira][classificacao_mae]" autocomplete="off" id="CategoriasMovimentacoesFinanceiraClassificacaoMae"/>
								<input type="hidden" name="data[CategoriasMovimentacoesFinanceira][tipo]" autocomplete="off" id="CategoriasMovimentacoesFinanceiraTipo"/>

								<div class="form-group col-sm-6 col-md-6 col-lg-6 required">

				                <label for="CategoriasMovimentacoesFinanceiraCategoriaId">Conta mãe</label>
				                <select name="data[CategoriasMovimentacoesFinanceira][categoria_id]" class="required form-control" required="required" autocomplete="off" id="CategoriasMovimentacoesFinanceiraCategoriaId">
				                    <option value="">Selecione</option>
				                                                        <option classificacao = "1.1" value="2100098" tipo = "D">&nbsp;&nbsp;&nbsp;&nbsp;1.1 - Despesas administrativas e comerciais</option>
				                                                        <option classificacao = "1.2" value="2100135" tipo = "D">&nbsp;&nbsp;&nbsp;&nbsp;1.2 - Despesas de produtos vendidos</option>
				                                                        <option classificacao = "1.3" value="2100144" tipo = "D">&nbsp;&nbsp;&nbsp;&nbsp;1.3 - Despesas financeiras</option>
				                                                        <option classificacao = "1.4" value="2100146" tipo = "D">&nbsp;&nbsp;&nbsp;&nbsp;1.4 - Investimentos</option>
				                                                        <option classificacao = "1.5" value="2100148" tipo = "D">&nbsp;&nbsp;&nbsp;&nbsp;1.5 - Outras despesas</option>
				                                        </select>
				            </div>
				            <div class="form-group col-sm-6 col-md-6 col-lg-6 required">
				            	<label for="CategoriasMovimentacoesFinanceiraNome">Nome</label>
				            		<input name="data[CategoriasMovimentacoesFinanceira][nome]" class="required form-control" maxlength="100" autocomplete="off" type="text" id="CategoriasMovimentacoesFinanceiraNome" required="required"/>
				            </div>
				            <div class = "both col-sm-12 col-lg-12 col-md-12 margin-top-10px">
				            	<button class="btn btn-success btn-responsive" type="submit">
				            		<span class="glyphicon glyphicon-ok margin-right-10px"></span> 
				            		&nbsp; Cadastrar
				            	</button>
				            	<a href="javascript: CloseModal();" class="btn btn-danger btn-responsive">
				            		<span class="glyphicon glyphicon-remove margin-right-10px"></span>
				            		Cancelar
				            	</a>                
							</div>
				            
				            </form>
				        </div>
				    </div>
				</div>    	
    	';
    }


    public function Add2() {
    	sleep(2);
    	$modal = false;
    	if(isset($_GET['modal']) && $_GET['modal'] == 'true') {
    		$modal = true;
    	}

    	if($_POST) {
    		$cc = new CentrodecustoModel();
    		$campos['cec_codigo'] = $_POST['codigo'];
    		$campos['cec_descri'] = $_POST['descricao'];
    		$campos['cec_localid'] = $_SESSION['APP_LOCALID'];
    		$campos['cad_data'] = date("Y-m-d G:i:s");
    		$campos['cad_usua'] = $_SESSION['APP_USUID'];

    		$cc = $cc->inserir($campos);
    		if(!$cc) {
    			echo -1;
    			return;
    		}

    		echo "";
    		return;
    	}


#    	if($modal == true) {
#    		echo "aqui";
#    		return;
#    	}
    	echo '	
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content">
		    		<form action="index.php?route=CentroCusto/add2/?modal=true&tipo=D" method="post" class="form-new-reg">
		    		<input type="hidden" name="_method" value="POST"/>            
		    
						<div class="modal-header">Adicionar Centro de Custo</div>    	
						<div class="modal-body modal-large row">
							<div class="form-group col-sm-6 col-md-6 col-lg-6 required">
								<label>Código</label>
								<input type="text" class="form-control" name="codigo"/>
							</div>
							<div class="form-group col-sm-6 col-md-6 col-lg-6 required">
								<label>Descrição</label>
								<input type="text" class="form-control" name="descricao"/>
							</div>

						</div>
						<div class="modal-footer">
							<button class="btn btn-success btn-responsive"> <span class="glyphicon glyphicon-ok margin-right-10px"></span> Cadastrar </button>
			            	<a href="javascript: CloseModal();" data-toogle="modal-desmiss" class="btn btn-danger btn-responsive"> <span class="glyphicon glyphicon-remove margin-right-10px"></span> Cancelar </a>
						</div>
					</form>
		    	</div>
		    </div>
    	';
    }    

}

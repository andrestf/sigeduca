<?php

class AlunosFinanceiroController extends Controllers {

    /**
     * LIsta os processos iniciados do aluno que nao foram exportados para o financeiro
     * @return type
     */

    public function __construct()
    {
        parent::__construct();
        if (!isset($_GET['codigo'])) {
            exit("ERRO");
        }

    }
    public function index() {
        if (!isset($_GET['codigo'])) {
            exit("ERRO");
        }

        $tipo = "a";
        $fichaTipo = "a";
        if(isset($_GET['tipo'])) {

            $tipo = $_GET['tipo'];

            switch ($tipo) {
                case 'a':
                    $tipo = "a";
                    $fichaTipo = "a";
                    break;

                case 'r':
                    $tipo = "r";
                    $fichaTipo = "r";
                    break;
                
                default:
                    exit("ERRO: TIPO INVÁLIDO LINHA: " . __LINE__);
                    break;
            }
        } else {
            exit("TIPO NAO INFORMADO");
        }


        $ID_ALUNO = $_GET['codigo'];

        $AlunosFinanceiroModel = new AlunosFinanceiroModel();
        $nParcelasLancadas = $AlunosFinanceiroModel->VerificaLancamento($ID_ALUNO);

        $CreditosModel      = new CreditosModel();
        $ListaCreditos      = $CreditosModel->Lista($ID_ALUNO);
        $CreditosDisponivel = $CreditosModel->SomaCreditoDisponivel($ID_ALUNO);

        $AlunosModel = new AlunosModel();
        $Matriculas = $AlunosModel->SelecionaMatricula($ID_ALUNO);
        $nMatriculas = $Matriculas->num_rows;

        $AlunosMatriculasModel = new AlunosMatriculasModel();
        $MatriculasSemFinanceiro = $AlunosMatriculasModel->SelecionaMatriculas($ID_ALUNO, ' AND amat_exporta IS NULL');

        $Parcelas = $AlunosFinanceiroModel->ListaParcelas($ID_ALUNO); 
        $TipoDocumento = new TipoDocumentosModel();
        $TipoDocumento = $TipoDocumento->Listar();

        $Funcoes = new UsuariosHelper();

        $dados = array(
            "fichaTipo" => $fichaTipo,
            "ID_ALUNO" => $ID_ALUNO,
            "nParcelasLancadas" => $nParcelasLancadas,
            "MatriculasSemFinanceiro" => $MatriculasSemFinanceiro,
            "nMatriculas" => $nMatriculas,
            "Parcelas" => $Parcelas,
            "TpDocumentos" => $TipoDocumento,
            "Funcao" => $Funcoes,
            "Creditos" => $ListaCreditos,
            "CreditosDisponivel" => $CreditosDisponivel
        );

        $this->RenderView('alunos/financeiro/index', $dados);
    }

    /**
     * Iniccia o processo de lancamento de Parcelas de um processo(matricula)
     * @return type
     */
    public function IniciaLancaParcelas() {
        if (!isset($_GET['codigo'])) {
            exit("ERRO");
        }

        $this->ValidaNivel(40);

        $ID_ALUNO = $_GET['codigo'];
        $ID_MATRICULA = $_GET['matricula'];

        //Valida de matricula pertence ao aluno e se nao tem parcelas já lancadas
        $AlunosMatriculasModel = New AlunosMatriculasModel();
        $matricula = $AlunosMatriculasModel->SelecionaMatriculas($ID_ALUNO, "AND amat_id = '$ID_MATRICULA' AND amat_exporta IS NULL LIMIT 1");
        if (!$matricula) {
            exit('Matricula ja processada ou nao pertence a esse aluno!');
        }

        //TP Documentos
        $TpDocumentosModel = new TipoDocumentosModel();
        $TpDocumentos = $TpDocumentosModel->Listar();

        //Forma de Pagamentos
        $FormaPagamentosModel = new FormaPagamentosModel();
        $FormaPagamentos = $FormaPagamentosModel->Listar();

        //Processo Selecionado
        $AlunosMatriculasModel = new AlunosMatriculasModel();
        $Matricula = $AlunosMatriculasModel->SelecionaMatriculas($ID_ALUNO, " AND amat_id = '$ID_MATRICULA' AND amat_exporta IS NULL");

        //Creditos
        $CreditosModel = new CreditosModel();
        $VlrCreditoDisponivel = $CreditosModel->SomaCreditoDisponivel($ID_ALUNO);
        
        $dados = array(
            "TpDocumento" => $TpDocumentos,
            "FormaPagamentos" => $FormaPagamentos,
            "Matricula" => $Matricula[0],
            "VlrCreditoDisponivel" => $VlrCreditoDisponivel
        );


        //print_r($Matricula);
        $this->RenderView('alunos/financeiro/lancarparcelas', $dados);
    }

    /**
     * Salva lançamento das parcelas do aluno x martricula. 
     * Esse processo inicia depois do IniciaLancaParcelas();
     * */
    public function SalvaParcelas() {
        if (!isset($_GET['parcelas']) && !isset($_POST['parcelas'])) {
            exit("ERRO ER-0798 [Alunos Financeiro Ctrl]");
        }

        if (isset($_POST['parcelas'])) {
            $parcelas = $_POST['parcelas'];
            $nParcelas = count($parcelas);
            $aParcelas = array();

            for ($i = 1; $i < $nParcelas - 1; $i++) {
                $aParcelas[] = $parcelas[$i];
            }
        } else {

            $valor = $_POST['valorec'];
            $valor = str_replace(".", "", $valor);
            $valor = str_replace(",", ".", $valor);

            $valor = $valor/$_POST['nParcelas'];
            
            $aParcelas = array();
            $aParcelas[0]['parcela'] = "";
            $aParcelas[0]['valor'] = $valor;
            $aParcelas[0]['valorOriginal'] = $_POST['valorec'];
            $aParcelas[0]['vencimento'] = $_POST['vencimento'];
            $aParcelas[0]['descricao'] = $_POST['descricao'];
            $aParcelas[0]['documento_num'] = $_POST['documento'];
            $aParcelas[0]['documento'] = $_POST['tpdoc'];
            $aParcelas[0]['predata'] = DataDB($_POST['predata']);

            $aParcelas[0]['tipo'] = "Débito/Parcela";

        }
        /** formato das parcelas 
         * 
         * 
          [0] => Array
          (
          [parcela] => N
          [valor] => nn.nn
          [vencimento] => dd/mm/yyyy
          [descricao] => nonon
          [documento] => DINH*
          [tipo] => ADICIONAL
          ); */
        $tpLanca = "";
        if (isset($_GET['act'])) {
            $tipo = strtoupper($_GET['act']);
            switch ($tipo) {
                case 'CREDITO' :
                    $tpLanca = "CREDITO";
                    break;
            }
        }

        $AlunosFinanceiroModel = new AlunosFinanceiroModel();

        /**
        * repetir lancamento de parcelas
        ****************************************************************************************/
        $lote  = "";
        $lotex = sha1(time());

        #$nMesesRepetir = $_POST['nParcelas'];
        
        if(isset($_POST['nParcelas'])) {
            $nMesesRepetir = $_POST['nParcelas'];            
        } else {
            $nMesesRepetir = 1;
        }

        if($nMesesRepetir == 0 || $nMesesRepetir == '') {
            $nMesesRepetir = 1;
        }

        
        $vencimento =  DataDB($aParcelas[0]['vencimento']);
        $data =  DataDB($aParcelas[0]['vencimento']);
        for($m=0;$m <= $nMesesRepetir-1;$m++) {
            $aParcelas[0]['vencimento'] = DataBR($vencimento);
            $data = $vencimento;
            if($m > 0) {          
                $lote = $lotex;
                $vencimento = date('Y-m-d', strtotime("+1 month",strtotime($data)));
            }
            
            $aParcelas[0]['vencimento'] = DataBR($vencimento);
            
            //valores das parcelas

            
            $AlunosFinanceiroModel->LancaParcelas($aParcelas, $tpLanca);
        } 
        /******************************************************************************************/
        //$AlunosFinanceiroModel->LancaParcelas($aParcelas, $tpLanca);
    }

    #######################################################################################################

    public function LancaValores() {
        if(!$this->TipoAcesso("OPERADOR")) {
            $this->RenderView("erros/404");
            exit();
        }

        if (!isset($_GET['codigo'])) {
            exit("ERRO CODIGO NAO DEFINIDO");
        }

        if (!isset($_GET['act'])) {
            exit("ERRO ACAO INDEFINIDA");
        }


        $Alunos = new AlunosModel();
        if(!$Alunos->VerificaAtivo($_GET['codigo'])) {
            $dados = array();
            $this->RenderView('erros/ContratoCancelado', $dados);
            return;
        }

        //TP Documentos
        $TpDocumentosModel = new TipoDocumentosModel();
        $TpDocumentos = $TpDocumentosModel->Listar();

        //Forma de Pagamentos
        $FormaPagamentosModel = new FormaPagamentosModel();
        $FormaPagamentos = $FormaPagamentosModel->Listar();
        $tipo = strtoupper($_GET['act']);
        $aluno = $_GET['codigo'];

        //RESPONSAVEL FINANCEIRO
        $_ResponsavelFinan = new ResponsavelFinanceiroModel();
        $ResponsavelFinan = $_ResponsavelFinan->SelecionaRespAluno($aluno);

        switch ($tipo) {
            case 'DEBITO':
                $tpLanca = "DEBITO";
                break;

            case 'CREDITO':
                $tpLanca = "CREDITO";
                #$TpDocumentos = array();
                #$TpDocumentos[0]['tpd_cd'] = 'DINH';
                #$TpDocumentos[0]['tpd_descricao'] = 'Dinheiro';

                
                
                unset($TpDocumentos[5]);
                
                
                break;
            default :
                $tpLanca = "INVALIDO";
                break;
        }




        $dados = array(
            "TpDocumento" => $TpDocumentos,
            "FormaPagamentos" => $FormaPagamentos
        );



        $this->RenderView('alunos/financeiro/lancavalores', $dados);
    }

}

<?php

$report = new AlunosRelatoriosModel();
$report->CondicaoExtra = " and NOT amf_iniciado IS NULL and amf_concluido IS NULL and serv_descapelido = 'SEMESTRAL' ";
$report->OrderBy = "usu_nomecompleto";
$report->GroupBy = "usu_id";
$dados  = $report->Gerar();
?>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>Aluno</th>
			<th>Matricula</th>
			<th>Processo</th>
			<th>Vencimento</th>
		</tr>
	</thead>
	<tbody>
		<?php if($dados->num_rows >= 1) { ?>
                    <?php while($report = $dados->fetch_assoc() ) {?>
                    <tr>
                            <td width="*"><?php echo $report['usu_nomecompleto']?> <br/></td>
                            <td><?php echo $report['tpmat_descricao']?> <br/></td>
                            <td><?php echo $report['serv_descricao']?> <br/></td>
                            <td><?php echo $report['serv_descapelido']?> <br/></td> 
                    </tr>
                    <?php }//while ?>
                <?php } else {//if ?>
                    <tr>
                        <td  colspan="5" style="font-size: 28px">
                            <center>
                            Sem resultados
                            </center>
                        </td>
                    </tr>
                    
                <?php } ?>
	</tbody>
</table>


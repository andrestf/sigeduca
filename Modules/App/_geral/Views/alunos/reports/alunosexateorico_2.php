<?php
$xxx = new Report();
//$xxx->CONDICAO_EXTRA = " AND PROD_LETRA != '' AND PROD_CATID = '2'";
$xxx->CONDICAO_EXTRA = " AND PROD_CATID = '2'";
$xxx->GROUP_BY = " PE_NOME ";
$xxx->ORDER_BY = " PROD_LETRA DESC, PE_NOME DESC   ";
$report = $xxx->AlunosReport();
$i = 1;

    
?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-titlez" style="width: 100%">
                <center>
                    <u><b><?=  strtoupper($LOCAL->LOC_NOME);?> <br/></u></b>
                    
                    <br/>
                    <small>
                        Relação de alunos que prestarão exame <b> TEÓRICO </b> para motoristas e motociclistas dia <b> <?php echo $xxx->EXAME_DATAINI; ?> </b>
                    </small>
                </center>
            </h3>
        </div>
        <div class="box-body">
            <table id="report" class="table table-bordered table-condensed table-responsive table-striped">
                <thead>
                    <tr>
                        <th width="50">Nº</th>
                        <th width="50">CAT.</th>
                        <th>NOME DO EXAMINADO</th>
                        <th width="150">INSTRUTOR</th>
                        <th width="150">CPF</th>
                        <th width="150">RENACH</th>
                        <th width="150">RESULTADO</th>
                    </tr>
                </thead>
                <?php
                
                while($aluno = $report->fetch_object()) { 
                    $CATS = "";
                         $Qx = "SELECT PROD_LETRA AS CATEGORIAS, CATCNH_COD "
                            . " FROM pessoasxprocessos PRX "
                            . " LEFT OUTER JOIN produtos ON PROD_ID = PRXAL_PROCID "
                            . " LEFT OUTER JOIN categoriascnh ON CATCNH_ID = PROD_LETRA "
                            . " WHERE PRXAL_ALUNOID = '$aluno->PE_ID' AND PRX.can_data IS NULL ORDER BY PROD_LETRA ASC" ;
                         $Q = $conexao->query($Qx);
                         while ($Qs = $Q->fetch_object()) {
                                $CATS .= trim($Qs->CATCNH_COD) . "/";
                         }
                    
                    ?>
                <tr>
                    <td><?=$i;?></td>
                    <td><?php echo substr($CATS,0,-1) ;?></td>
                    <td><?=strtoupper($aluno->PE_NOME);?></td>
                    <td></td>
                    <td><?=$aluno->PE_CPF;?></td>
                    <td><?=$aluno->PE_RENACH;?></td>
                    <td></td>
                <?php $i++; } ?>
            </table>
        </div>
    </div>
</section>

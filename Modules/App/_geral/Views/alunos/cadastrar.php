﻿<!--
era assim.... ...
form class="" method="post" name="frm_aluno" id="frm_aluno" action="" onsubmit="salvar(); return false;"

mudou pra linha de baixo!!
-->
<?php $funcs = new FuncoesHelper(); ?>

<form class="" method="post" name="frm_aluno" id="frm_aluno" action="" onsubmit="salvar(); return false;" style="margin-top: -20px; padding-bottom: 20px;">
    <input type="hidden" name="tipo" id="tipoFicha" value="<?php echo $fichaTipo?>">
    <?php if(isset($_GET['alunoid_r'])){ ?>
        <!-- input type="hidden" name="alunoid_r" id="alunoid_r" value="<?php echo $_GET['alunoid_r'];?>" -->
    <?php } ?>
    <div class="content asec-box" id="ficha_novo_aluno">
        <h3 class="page-title">
            <?php echo $view_PageTitle ?>
        </h3>

        <BR/>&nbsp;

        <div class="pull-right grupo-btn btns1">
            <button type="reset" onclick="cancelar();" class="btn btn-link"> <i class="fa fa-arrow-left"></i>  Cancelar </button> &nbsp;
            
            <?php if($this->TipoAcesso("OPERADOR")) { ?>
                <span class="grupo-btn-01">
                    <button type="submit" class="btn btn-success btnProgressSave"> <i class="fa fa-save"></i> Salvar</button>
                </span>
            <?php } ?>

            <span class="grupo-btn-02" style="display: none"> 
                <?php if($this->TipoAcesso("OPERADOR")) { ?>
                    <span class="btn btn-info btnEditar btnProgressSave" > <i class="fa fa-save "></i> Salvar</span>
                <?php } ?>
                <span class="btn btn-dropbox btnFicha hide" > <i class="fa fa-user"></i> Ficha</span>
                <span class="btn btn-primary btnContrato" > <i class="fa fa-newspaper-o"></i> Contrato</span>
                <span class="btn btn-linkedin btnNP hide" > <i class="fa fa-money"></i> Promissória</span>
                
                <?php if($fichaTipo == 'a') { ?>
                    <span class="btn btn-warning btnProcessos"> <i class="fa fa-cogs "></i> Matrículas</span>
                <?php } ?>
                
                <?php #if($this->TipoAcesso("OPERADOR")) { ?>
                    <a data-toggle="tooltip" title="Eventos"    href="<?php echo Permalink('Eventos/Aluno',        '','codigo='.$view_Aluno->usu_id."&tipo=".$_GET['tipo'])?>" class="btn btn-primary btnFinanceiro"> <i class="fa fa-wpforms"></i> Ingressos </a>
                <?php #} ?>
                <a data-toggle="tooltip" title="Financeiro" href="<?php echo Permalink('AlunosFinanceiro/index','','codigo='.$view_Aluno->usu_id."&tipo=".$_GET['tipo'])?>" class="btn btn-success btnFinanceiro"> <i class="fa fa-money"></i> Financeiro</a>

            </span>
        </div>
        
        <?php if(isset($matriculas) && $matriculas) { ?>

        <?php if(@$view_Aluno->can_data != '') { ?>
            <BR/><BR/>
            <div class="alert alert-danger text-center">CONTRATO CANCELADO</div>
            <SCRIPT>
                $("#frm_aluno").css("background","#ffcaca");
            </SCRIPT>
        <?php }?>


        <?php if(!$matriculas && $fichaTipo == 'a') { ?>
            <BR/><BR/>
            <div class="alert alert-danger text-center">Aluno sem matricula</div>
            
        <?php } else { ?>

        
            <section id="infos" style="margin-top: 45px;">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="box no-border">
                            <div class="box-header bg-aqua text-center">
                                
                                Matrículas 
                        
                            </div>
                            <div class="box-body text-center" style="    min-height: 60px;">
                                <?php if($matriculas) {
                                        echo count($matriculas); } else { echo "Aluno sem matricula."; } ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                        
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="box no-border">
                            <div class="box-header bg-fuchsia text-center">
                                Fase Atual
                            </div>
                            <div class="box-body text-center" style="min-height: 60px;">
                                <?php if($fases) { ?>
                                    <?php foreach ($fases as $fasex) { ?>
                                        <?php echo $fasex['serviten_descricao']; ?> <br/>
                                    <?php } ?>
                                <?php } else {  ?>

                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="box no-border">
                            <div class="box-header bg-orange  text-center">
                                Financeiro
                            </div>
                            <div class="box-body text-center">
                                <div class="row">
                                    <div class="col-md-4 text-red">Em aberto<br/> R$ <?php echo number_format($valorDevido,2,",","."); ?></div>
                                    <div class="col-md-4 text-blue">Pago<br/> R$ <?php echo number_format($valorPago,2,",","."); ?></div>
                                    <div class="col-md-4 text-green">Crédito <br/> R$ <?php echo number_format($creditosDisponivel,2,",","."); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
        <?php } //if !$matriculas ?>
        <?php } //isset matricula ?>

        <section id="observacao">
            <h4><b>Informações</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>Período</label><br/>
                        <select name="periodo" id="periodo" class="form-control ">
                            <option></option>
                            <?php foreach($tabPeriodo as $per) {?>
                            <option value="<?php echo $per['per_cod']; ?>" <?php echo (@$view_Aluno->usu_periodo == $per['per_cod']) ? "selected='selected'" : '';  ?> > <?php echo $per['per_descri']?> </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
             </div>
        </section>        
        <section id="dados_pessoais">
            <h4 class="ficha_title">Dados Pessoais</h4>
            <div class="form">
                <input type="hidden" id="aluno_id" name="id" value="<?php echo @$view_Aluno->usu_id?>" />
                <input type="hidden" id="aluno_acao" name="acao" value="incluir" />
                <input type="hidden" id="aluno_acesso" name="acesso" value="" />
                <input type="hidden" id="aluno_chave" name="chave" value="" />
                
                <div class="form-group">
                    <?php $class = 'col-sm-12'; if($fichaTipo  == 'ALUNO') { $class="col-sm-10"; ?>
                        <div class="col-md-2">
                            <label>Foto</label>
                            <div class="div-foto">
                                <img src="http://econdutorcfc.com/alunos/semfoto.jpg" class="foto-aluno hide" />
                                
                                <img src="/alunos/GetFoto/?codigo=<?php echo $view_Aluno->usu_id?>" class="foto-aluno"  />
                                <div class="foto-edit">
                                    <i class="fa fa-pencil cursor" onclick="FotoUpload();"></i>
                                    &nbsp;&nbsp;&nbsp;
                                    <i class="fa fa-trash cursor" onclick="FotoRemove();"></i>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <div class="<?php echo $class; ?>">
                        <div class="form-group">

                        <?php if($fichaTipo == "ALUNO") { ?>
                        <div class="col-sm-4 bottom10">
                            <label>Matricula</label>
                            <input type="text" class="form-control matricula" name="matricula" value="<?php echo @$view_Aluno->usu_matricula; ?>" readonly="readonly"/>
                        </div>
                        <?php } ?>

                            <div class="col-sm-9 bottom10">
                                <label for="nomecompleto">Nome completo <span class="obg">*</span></label>
                                <input id="nomecompleto" name="nomecompleto" placeholder="Nome completo" maxlength="100" class="form-control" type="text" onkeyup="filtraCampo(this)" value="<?php echo @$view_Aluno->usu_nomecompleto?>" requiredx="">
                            </div>
                            
                            <?php if($this->TipoAcesso("OPERADOR")) { ?>
                                <div class="col-sm-3 bottom10">
                                    <label for="situacao">Situação</label>
                                    <input id="situacao" name="situacao" placeholder="Situacao" maxlength="30" class="form-control" type="text" onkeyup="filtraCampo(this)" value="<?php echo @$view_Aluno->usu_situacao?>" requiredx="">
                                </div>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 bottom10">
                                <label for="nomepai">Nome do pai <span class="obg">*</span></label>
                                <input id="nomepai" name="nomepai" placeholder="Nome do pai" maxlength="100" class="form-control" type="text" onkeyup="filtraCampo(this)"  value="<?php echo @$view_Aluno->usu_nomepai; ?>"  requiredx="">
                            </div>
                            <div class="col-sm-6 bottom10">
                                <label for="nomemae">Nome da mãe <span class="obg">*</span></label>
                                <input id="nomemae" name="nomemae" placeholder="Nome da mãe" maxlength="100" class="form-control" type="text" onkeyup="filtraCampo(this)" value="<?php echo @$view_Aluno->usu_nomemae; ?>" requiredx="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 bottom10">							
                                <label for="nascimento">Data nascimento <span class="obg">*</span></label>

                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar"></span></span>
                                    <input id="nascimento" name="nascimento" maxlength="10" placeholder="dd/mm/aaaa" type="text" class="form-control datepicker" value="<?php echo DataBR(@$view_Aluno->usu_nascimento); ?>" requiredx="" style=" z-index: 1 !important;">
                                </div>
                            </div>

                            <div class="col-sm-6 bottom10">
                                <input type="hidden"  name="sexo"/>
                                <label for="sexo">Sexo <span class="obg">*</span></label><br>
                                <input id="sf" type="radio"  name="sexo" value="F" <?php echo (@$view_Aluno->usu_sexo == "F") ? "checked='checked'" : '';  ?> > Feminino &nbsp;&nbsp;
                                <input id="sm" type="radio" name="sexo" value="M"  <?php echo (@$view_Aluno->usu_sexo == "M") ? "checked='checked'" : '';  ?>> Masculino
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-12 col-md-10 col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="nacionalidade">Nacionalidade</label><br>
                                <select name="nacionalidade" id="nacionalidade" class="form-control ">
                                    <option value="1" <?php echo (@$view_Aluno->usu_nacionalidade == "1") ? "selected='selected'" : '';  ?> >Brasileiro</option>
                                    <option value="2" <?php echo (@$view_Aluno->usu_nacionalidade == "2") ? "selected='selected'" : '';  ?>>Brasileiro Naturalizado</option>
                                    <option value="3" <?php echo (@$view_Aluno->usu_nacionalidade == "3") ? "selected='selected'" : '';  ?>>Estrangeiro</option>
                                    <option value="4" <?php echo (@$view_Aluno->usu_nacionalidade == "4") ? "selected='selected'" : '';  ?>>Brasileiro Nascido no Exterior</option>
                                </select>
                            </div>
                            <div class="col-sm-3" id="ufpessoadiv">
                                <label for="uf">UF </label><br>
                                <select name="uf" class="form-control select2" id="ufpessoa">
                                    <option value=""> Selecione </option>
                                        <?php while($uf = $ListaUF->fetch_object()) { ?>
                                            <option value="<?php echo $uf->uf; ?>" data-id="<?php echo $uf->codigo; ?>" <?php if($uf->uf == @$view_Aluno->usu_lograuf) { echo 'selected="selected"';}?> > <?php echo $uf->uf; ?>  </option>
                                        <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-5">
                                <label for="naturalidade"> Naturalidade </label><br>
                                <select name="naturalidade" class="form-control select2x" id="naturalidade">  </select>
                            </div>
                        </div>                          
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <section id="documentos">
            <h4 class="ficha_title">Documentos</h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label for="cpf">CPF <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="cpf" name="cpf" placeholder="000.000.000-00" onkeyup="formataCPF(this)" maxlength="14" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_cpf; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rg">RG <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="rg" name="rg" placeholder="000000000" maxlength="20" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_rg; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rgemissor">Orgão Emissor </label>
                        <input id="rgemissor" name="rgemissor" placeholder="Ex.: SSP" maxlength="25" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_rgemissor; ?>" requiredx="">
                    </div>

                    <div class="col-sm-3">
                        <label for="rguf">UF </label>
                        <select id="rguf" name="rguf" class="form-control select2 " requiredx="">
                            <option value=""> Selecione </option>
                                <?php while($uf2 = $ListaUF2->fetch_object()) { ?>
                                    <option value="<?php echo $uf2->uf; ?>"  <?php if($uf2->uf == @$view_Aluno->usu_rguf) { echo 'selected="selected"'; }?> > <?php echo $uf2->uf; ?> </option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>

        <section id="endereco">
            <h4 class="ficha_title">Endereço</h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3 bottom10">
                        <label for="cep">CEP </label>
                        <input id="cep" name="cep" placeholder="CEP" maxlength="9" class="form-control cep" type="text" value="<?php echo @$view_Aluno->usu_cep; ?>" requiredx="" onblur="buscacep();">
                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank" class="fa fa-search btn btn-default btnCep" style="  float: right; margin-top: -34px;  height: 34px;"></a>
                    </div>
                    <div class="col-sm-6 bottom10">
                        <label for="logradouro">Logradouro <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="logradouro" name="logradouro" onkeyup="filtraCampo(this)" placeholder="Logradouro" maxlength="40" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_logradouro; ?>" requiredx="">
                    </div>                
                    <div class="col-sm-3 bottom10">
                        <label for="logranumero">Numero <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="logranumero" name="logranumero" onkeyup="formataNumerico(this)" placeholder="Número" maxlength="9" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_logranumero; ?>" requiredx="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 bottom10">
                        <label for="lograbairro">Bairro <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="lograbairro" name="lograbairro" onkeyup="filtraCampo(this)" placeholder="Bairro" maxlength="40" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_lograbairro; ?>" requiredx="">
                    </div>
                    <div class="col-sm-3 bottom10">
                        <label for="logramunicipio">Município <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="logramunicipio" name="logramunicipio" onkeyup="filtraCampo(this)" placeholder="Município" maxlength="40" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_logramunicipio; ?>" requiredx="">
                    </div>              
                    <div class="col-sm-3 bottom10">
                        <label for="lograuf">Complemento </label>
                        <?php if(@$view_Aluno->usu_logracomplemento == '') {
                            $complemento = "CASA";
                        } else {
                            $complemento = @$view_Aluno->usu_logracomplemento;
                        } ?>
                        <input id="logracomplemento" name="logracomplemento" placeholder="Complemento" maxlength="30" class="form-control" type="text" value="<?php echo $complemento; ?>" requiredx="">
                    </div>                    
                    <div class="col-sm-3 bottom10">
                        <label for="lograuf">UF <?php echo ($fichaTipo  != 'ALUNO') ? '<span class="obg">*</span>' : ''; ?> </label>
                        <input id="lograuf" name="lograuf" placeholder="UF" maxlength="2" class="form-control" type="text" value="<?php echo @$view_Aluno->usu_lograuf; ?>" requiredx="">
                    </div>
                    

                </div>
                <div class="clearfix"></div>

            </div>
        </section>
        
        
        <section id="contatos" style="display: block;">
            <h4><b>Telefones</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label>Celular</label>
                        <input type="text" class="form-control telefone" name="telcelular" id="telcelular" value="<?php echo @$view_Aluno->usu_telcelular; ?>"/>
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Residencial</label>
                        <input type="text" class="form-control telefone" name="telresid" id="telresid" value="<?php echo @$view_Aluno->usu_telresid; ?>"/>
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Comercial</label>
                        <input type="text" class="form-control telefone" name="telcomercial" value="<?php echo @$view_Aluno->usu_telcomercial; ?>" />
                    </div>
                    
                    <div class="col-sm-3">
                        <label>Contato</label>
                        <input type="text" class="form-control telefone" name="telrecado" value="<?php echo @$view_Aluno->usu_telrecado; ?>"/>
                    </div>
                </div>
                
                <div class="clearfix"></div>
             </div>
             <div class="clearfix"></div>
        </section>
        
        <section id="acesso">
            <h4><b>Acesso</b></h4>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" value="<?php echo @$view_Aluno->usu_email; ?>"/>
                    </div>
                </div>
            </div>
            <div class="form">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>Login</label>
                        <input type="text" class="form-control" name="login" id="login" value="<?php echo @$view_Aluno->usu_login; ?>"/>
                    </div>
                    <div class="col-sm-4">
                        <label>Senha</label>
                        <input type="text" class="form-control" name="senha" id="senha" value="<?php echo @$view_Aluno->usu_senha; ?>"/>
                    </div>

                    <?php if($this->TipoAcesso("OPERADOR")) { ?>
                    <div class="col-sm-4">
                        <label>Acesso</label>
                        <select class="form-control" name="sitacesso">
                            <option value="A" <?php echo (@$view_Aluno->usu_sitacesso == "A") ? "selected='selected'" : '';  ?> >Ativo</option>
                            <option value="I" <?php echo (@$view_Aluno->usu_sitacesso == "I") ? "selected='selected'" : '';  ?> >Inativo</option>
                        </select>
                    </div>
                    <?php } ?>
                    


                </div>
                
                <div class="clearfix"></div>
             </div>            
            
        </section>
        <?php if($fichaTipo == "ALUNO" || $fichaTipo == 'a') { ?>

        
            <section id="sectionResponFinan">
                <h4><b>Responsável Financeiro</b></h4>
                <?php if(isset($_GET['codigo'])) { ?>
                <input type="hidden" name="responfinan" id="txtresponfinan" value="<?php echo $view_Aluno->respon_id; ?>" />
                <div class="form">
                    <div class="form-group">
                        <div class="col-sm-12" id="ResponFinan">
                            <?php if($this->TipoAcesso("OPERADOR")) { ?>
                                <div class="DivAddResponFinan">
                                    <p>
                                        Nome ou CPF<small>(somente números)</small>
                                        <a href="<?php echo Permalink('alunos/cadastrar/','','tipo=r&alunoid_r='.@$view_Aluno->usu_id.'')?>" onclick="addResponFinan()" style="margin-bottom: 10px;" class="btn btn-default btn-flat btn-sm BtnAddResponFinan pull-right">Cadastrar</a>
                                    </p>
                                    <input type="text" name="" class="form-control" id="txtBsucaResponF" autocomplete="off" />
                                    <div class="form" id="frmAddRespon" style="height: 130px; overflow: auto; padding: 0px; display: none; margin-bottom: 5px;">
                                        <ul id="ulResponsF" class="listaRespons" ></ul>
                                    </div>
                                    <div class="divResponDadosSemInfo" style="display: block;" >
                                        <br/>
                                            <center>
                                                <b>Nenhum responsável financeiro. 
                                                </b>
                                            </center>
                                    </div>
                                </div>
                            <?php } ?>
                            
                                <div class="row divResponDados" style="display: none;" >
                                    <div class="col-sm-12">
                                    <?php if($this->TipoAcesso("OPERADOR")) { ?>
                                        <span class="btn btn-default btn-flat btn-xs btnRemoveRespon pull-right" style="cursor: pointer; " title="Remover" data-toggle="tooltip"><i class="text-red fa fa-trash"></i></span>
                                    <?php } ?>
                                        <a href="<?php echo Permalink('alunos/editar','','&codigo='.@$view_Aluno->respon_id.'&tipo=r&alunoid_a='.@$view_Aluno->usu_id.'')?>" class="btn btn-default btn-flat btn-xs pull-right" style="cursor: pointer; margin-right: 5px; " title="Ficha" data-toggle="tooltip"><i class="text-black fa fa-user"></i></a>
                                        
                                        <b class='text-blue'>Responsável:</b> <span class='respon_nome'><?php echo @$view_Aluno->respon_nomecompleto?></span>
                                    </div>
                                    <div class="col-sm-6"><b class='text-blue'>CPF:</b> <span class='respon_cpf'><?php echo @$view_Aluno->respon_cpf?></span></div>
                                    <div class="col-sm-6"><b class='text-blue'>E-mail:</b> <span class='respon_email'><?php echo @$view_Aluno->respon_email?></span></div>
                                </div>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                 </div>
                    <!-- Trigger the modal with a button -->

                    <!-- Modal -->

                 <?php } else { ?>
                    <div class="form">
                        Para adicionar um responsável financeiro, primeiro salve o cadastro do aluno.
                    </div>  
                 <?php } ?>
                 <div class="clearfix"></div>
            </section>
        <?php } ?>        

        
        <?php if($this->TipoAcesso("OPERADOR")) { ?>
            <section id="observacao">
                <h4><b>Observações</b></h4>
                <div class="form">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Observacao</label><br/>
                            <input type="text" multiple="multiple" name="observa" class="form-control obs" value="<?php echo @$view_Aluno->usu_observa; ?>" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                 </div>
            </section>

            <?php if( isset($view_Aluno->usu_id) ) { ?>
            <section id="infos2">
                <h4><b>Informações</b></h4>
                <div class="form">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Cadastrado por:</label><br/>
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $funcs->fRetCampo('sis_usuarios','usu_apelido'," usu_id = '".@$view_Aluno->cad_usua."' "); ?>" />
                        </div>

                        <div class="col-sm-6">
                            <label>Data Cadastro:</label><br/>
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo DataBR(@$view_Aluno->cad_data); ?>" />
                        </div>
                    </div>

                    <div class="clearfix"></div>
                 </div>
            </section>
            <?php } ?>
        <?php } ?>



        <hr/>
        <?php if($this->TipoAcesso("OPERADOR")) { ?>
            <div class="grupo-btn btns2">
                    

                    <?php if($fichaTipo == "ALUNO" || $fichaTipo == 'a') { ?>
                        <?php if(@$view_Aluno->can_data != '') { ?>
                            <button type="reset" onclick="exContrato();" class="btn btn-danger pull-left"> <i class="fa fa-trash"></i>  Recuperar </button> &nbsp;
                        <?php } else { ?>
                            <button type="reset" onclick="exContrato();" class="btn btn-danger pull-left"> <i class="fa fa-trash"></i>  Excluir </button> &nbsp;
                        <?php } ?>
                    <?php } ?>

                    <div class="pull-right">
                        <button type="reset" onclick="cancelar();" class="btn btn-link"> <i class="fa fa-arrow-left"></i>  Cancelar Edição</button> &nbsp;
                        
                        <span class="grupo-btn-01">
                            <button type="submit" class="btn btn-success btnProgressSave"> <i class="fa fa-save"></i> Salvar</button>
                        </span>

                        <span class="grupo-btn-02" style="display: none">
                            <span class="btn btn-info btnEditar btnProgressSave" > <i class="fa fa-save "></i> Salvar</span>
                            <span class="btn btn-dropbox btnFicha hide" > <i class="fa fa-user"></i> Ficha</span>
                            <span class="btn btn-primary btnContrato" > <i class="fa fa-newspaper-o"></i> Contrato</span>
                            <span class="btn btn-linkedin btnNP hide" > <i class="fa fa-money"></i> Promissória</span>
                            <span class="btn btn-warning btnProcessos"> <i class="fa fa-cogs "></i> Matrículas</span>
                            
                            <a data-toggle="tooltip" title="Eventos"    href="<?php echo Permalink('Eventos/Aluno',        '','codigo='.$view_Aluno->usu_id."&tipo=".$_GET['tipo'])?>" class="btn btn-primary btnFinanceiro"> <i class="fa fa-wpforms"></i> Ingressos </a>
                            
                            <?php if(isset($matriculas) && $matriculas) { ?>
                                <a data-toggle="tooltip" title="Financeiro" href="<?php echo Permalink('AlunosFinanceiro/index','','codigo='.$view_Aluno->usu_id)?>" class="btn btn-success btnFinanceiro"> <i class="fa fa-money"></i> Financeiro</a>
                            <?php } ?>
                        </span>           
                    </div>
            
                <div class="clearfix"></div>
            </div>
        <?php  }?>

        <div class="pull-right"></div>
        <div class="clearfix"></div>        
        
    </div>
</form>
<style>
    .listaRespons,
    .listaRespons li {
        list-style: none;
        margin: 0px;
        padding: 0px;
    }

    .listaRespons li {
        padding: 5px;
    }

    .listaRespons li:hover {
        cursor: pointer;
        background: #6db974 !important;
    }
    .listaRespons li:nth-child(even) {
        background: #e1e1e1;
    }

    .infox1 {
        color: #cc7676;
        margin-top: -7px;
    }
</style>
<script>

    <?php if(!$this->TipoAcesso("OPERADOR")) { ?>
        $(function() {

            $( "#frm_aluno input[type=text]" ).each(function( index ) {
              val = $(this).val();
              $(this).parent().append("<div class='infox1'>"+val+"</div>");
              
            });

            $( "#frm_aluno select" ).each(function( index ) {
              val = $(this).val();
              $(this).parent().append("<div class='infox1'>"+val+"</div>");
              
            });


            $("#frm_aluno input").attr("disabled","disabled")
            $("#frm_aluno input").hide()
            $("#frm_aluno select").attr("disabled","disabled")
            $(".select2").hide()
            $("#frm_aluno select").hide()
            
            $("#frm_aluno .btnCep").hide();

            $("#frm_aluno .input-group-addon").hide();

        });
    <?php } ?>


    var codigo = "";
    var id_r = '';
    var canSave = true;
    var tipo = "<?php echo $_GET['tipo']; ?>"
    <?php if(isset($view_Aluno->usu_id)) { ?>
        codigo = '<?php echo $view_Aluno->usu_id; ?>';
        Ficha2();
    <?php } ?>

    var tipoEx = "EXC";
    <?php if(@$view_Aluno->can_data != '') { ?>
        var tipoEx = "REC";
    <?php } ?>


    

    $('body').on('keydown', 'input, select, textarea', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }
    })


 <?php if($fichaTipo == "ALUNO" || $fichaTipo == 'a') { ?>
    function exContrato() {
        alertify.confirm(
                "Atenção",
                "Confirmar exclusão/recuperação de contrato?",
                function() { exContrato2() },
                function() { return true; }
            ).set('labels', {ok:'Confirmar', cancel:'Cancelar'});
    }

    function exContrato2() {

        $.ajax({
            url : '/index.php?route=Alunos/CancelarAluno/&codigo='+codigo+"&t="+tipoEx,
            success: function(e) {
                if(e == "") {
                    alertify.success("Processo concluído!!!");

                    if(tipoEx == 'EXC') {
                        $("#frm_aluno").css("background","#ffcaca");    
                    } else {
                        $("#frm_aluno").css("background","#fff");
                    }
                    
                } else {
                    alertify.error("Erro ao processar!!!");
                }
            },
            erro: function(){
                alertify.error("Erro ao processar!!!");
            }
        });                
    }
<?php } ?>

    function addResponFinan() {
        $("#txtresponfinan").val('');
        $("#frmAddRespon").hide();
        $("#ulResponsF").html('');
        $("#ModalResponFinan").modal('show');

    }
    

    $("#txtBsucaResponF").on("keyup",function() {
        txt = $("#txtBsucaResponF").val();
        html = "<center><b>... PROCURANDO...</b></center>";
        $("#ulResponsF").html(html);
        $("#frmAddRespon").show();

        

        html = "";
        if(txt.length >= 4) {
            $.ajax({
                url: "<?php echo Permalink("ResponsavelFinanceiro","BuscaPorNome")?>/&aluno="+codigo+"&nome="+txt,
                success: function(e) {
                    $("#ulResponsF").html(e);                  
                }
            })
        } else {
            $("#frmAddRespon").hide();
        }
    });
    

    $(".btnRemoveRespon").on("click", function() {
        RemoveRespon();
    });

    function RemoveRespon() {
        $("#frmAddRespon").show()
        $(".divResponDadosSemInfo").show();
        $(".divResponDados").hide();
        $(".DivAddResponFinan").show();        
        addResponFinan()
    }

    function selecionaResp(e) {
        $("#frmAddRespon").hide();
        $(".divResponDadosSemInfo").hide();
        $(".divResponDados").show();
        $(".DivAddResponFinan").hide();


        id_r = $(e).data("id");
        nome_r = $(e).data("nome");
        cpf_r = $(e).data("cpf");
        email_r = $(e).data("email");

        $("#txtresponfinan").val(id_r);

        $("#txtBsucaResponF").val(nome_r);
        $(".respon_nome").html(nome_r)
        $(".respon_cpf").html(cpf_r)
        $(".respon_email").html(email_r)
    };

    <?php if(@$view_Aluno->respon_id) { ?>
        $("#frmAddRespon").hide();
        $(".divResponDadosSemInfo").hide();
        $(".divResponDados").show();
        $(".DivAddResponFinan").hide();
    <?php } ?>


    $(function(){
        alertify.set('notifier','position', 'top-right');     
    });

    $(".btnFicha").click(function() {
        window.location = "<?php echo Permalink("alunos","impFicha")?>/&aluno="+codigo+"&tipo="+tipo; 
    });
    
   $(".btnContrato").click(function() {
       window.location = "<?php echo Permalink("alunos","contrato")?>/&aluno="+codigo+"&tipo="+tipo; 
   });
   
   $(".btnNP").click(function() {
       window.location = "<?php echo Permalink("alunos","notapromissoria")?>/&aluno="+codigo+"&tipo="+tipo; 
   });
    
    $("#nacionalidade").change(function() {
       var nac = $("#nacionalidade option:selected").val();
       
       
       if(nac == 3 || nac == 4){
           $("#ufpessoadiv").hide();
           CarregaListaPaises();
       } else {
           $("#ufpessoadiv").show();
       }
    });

    function CarregaListaPaises(callback) {
        $("#naturalidade").html('<option value="0"> Carregando </option>');
        
        $.ajax({
            url : '/index.php?route=Alunos/CarregaListaPaises/',
            success: function(e) {
                $("#naturalidade").html(e);
                //callback();
            },
            erro: function(){
                alertify.error("Erro ao carregar Naturalidade!");
            }
        });        
    }
   
    $(".btnEditar").click( function() {
        
        if(canSave == false) {
            return false;
        }

        canSave = false;
        //$(".btnProgressSave").html('... salvando ....');
        $(".btnProgressSave").attr('disabled','disabled');

        var dados = $("#frm_aluno").serializeArray();
        $.ajax({
           data : dados,
           dataType : "json",
           type : 'post',
           url : '/index.php?route=alunos/SalvaFicha',
           success : function(e) {
               if(e.erro != "") {
                   alertify.error(e.mensagem);
                   canSave = true;
                   $(".btnProgressSave").removeAttr('disabled');
                   return false;
               }
               
               
                setTimeout(function(){
                    canSave = true; 
                    alertify.success(e.mensagem);
                    $(".btnProgressSave").removeAttr('disabled');
                },1000)
               
           },
           complete: function(){
                setTimeout(function(){
                    canSave = true; 
                    $(".btnProgressSave").removeAttr('disabled');
                },1000)
           }
        });
        
    });
   
   
    $(".btnProcessos").click( function() {
        if(codigo == "") {
            alertify.error("Concluir cadastro antes de iniciar processos!");
            return false;
        }
        
        window.location = "<?php echo Permalink("alunos","ListaMatriculas")?>/&aluno="+codigo+"&tipo="+tipo; 
    })
    
    
    function cancelar() {
        <?php if(isset($_GET['alunoid_r'])) { ?>
            window.location = "<?php echo Permalink("alunos/editar/&tipo=a&codigo=".$_GET['alunoid_r'])?>";
        <?php } else { ?>
            window.location = "<?php echo Permalink("alunos/listar/&tipo=".$_GET['tipo'])?>";
        <?php } ?>
    }
    
    function Ficha2(){
        $(".grupo-btn-01").hide();
        $(".grupo-btn-02").show();
    }
    
    function Ficha1(){
        $(".grupo-btn-01").show();
        $(".grupo-btn-02").hide();
    }

    function salvar() {

        if($("#aluno_id").val() !== "" ) {
          
            return false;
        }

        if(canSave == false) {
            return false;
        }

        canSave = false;
        $(".btnProgressSave").attr('disabled','disabled');
        
        var dados = $("#frm_aluno").serializeArray();
        
        $.ajax({
            data : dados,
            dataType: "json",
            type : "post",
            url  : '/index.php?route=alunos/incluir',
            success: function(e) {
                
                if(e.erro != "") {
                    alertify.error(e.mensagem);
                    $(".btnProgressSave").removeAttr('disabled');
                    canSave = true;
                    Ficha1();
                    return false;
                    
                }
                codigo = e.id;
                $("#id").val(e.id);
                $("#acao").val('editar');
                alertify.success('Aluno cadastrado com sucesso!');
                canSave = true;
                Ficha2();
                if(e.redir == '') {

                   location.href="index.php?route=alunos/editar/&codigo="+codigo+"&tipo=<?php echo $_GET['tipo']; ?>";
                } else {
                    location.href = e.redir + "&from=" + codigo;
                }                
            },
            error : function(e,x,s) {
                console.log(e +"\n"+ x +"\n"+ s)
            },
            
            complete: function() {
                canSave = true;
                $(".btnProgressSave").removeAttr('disabled');
            }
        });
    }
    

    
    
    
    $("#ufpessoa").change(function() {
        CarregaNaturalidade(function(){void(0)});
    });
    
    function CarregaNaturalidade(callback) {
        var CODIGO_ESTADO = $("#ufpessoa option:selected").attr("data-id");
        
        $("#naturalidade").html('<option value="0"> Carregando </option>');
        
        $.ajax({
            url : '/index.php?route=Alunos/CarregaNaturalidade/&codigo='+CODIGO_ESTADO,
            success: function(e) {
                $("#naturalidade").html(e);
                callback();
            },
            erro: function(){
                alertify.error("Erro ao carregar Naturalidade!");
            }
        });        
    }
    
    
    $(function() {
        var CODIGO_ESTADO = $("#ufpessoa option:selected").val();
        if(CODIGO_ESTADO !== '' || CODIGO_ESTADO !== '0') {
            
            $("#naturalidade").html('<option value="<?php echo @$view_Aluno->usu_naturalidade?>"><?php echo @$view_Aluno->usu_naturalidade?></option>');
            //CarregaNaturalidade(function(){
            //   $("#naturalidade").val('<?php echo @$view_Aluno->usu_naturalidade?>');
            //});
        }
    });
    
    
    $(".btnExporta").click( function() {
        ExportaECNH();
    });
    var CodigoMunicipio, metodo;
    function ExportaECNH() {
        CodigoMunicipio = "";
        xTemp = $("#logramunicipio").val();
        
        $.ajax({
            url : '/index.php?route=Alunos/CarregaCodMunicipioDeTranSP/&municipio='+xTemp,
            success: function(e) {
                CodigoMunicipio = e;
                //alert(CodigoMunicipio);
                
            },
            erro: function(){
                //alertify.error("Erro ao carregar Naturalidade!");
            },
            complete: function() {
                ExportaECNH2();
            }
        });
    }
    
    
    
    function ExportaECNH2() {

        
        metodo = $("#faseecnh option:selected").val();
        
        if(metodo == "PRIHAB") {
            metodo = 'iniciarPrimeiraHabilitacao';
        }else if(metodo == "INIREA") {
            metodo = 'iniciarReabilitacao';        
        }else if(metodo == "RENOVA") {
            metodo = 'iniciarRenovacao';
        }else if(metodo == "ADIMUD") {
            metodo = 'iniciarMudancaCategoria';
        }else if(metodo == "REACRI") {
            metodo = 'iniciarCassacaoReabilitacao';
        }else {
            alert("Informe a fase do ECNH");
           return false;
        }
        
        if( $("#sf:checked").val() ) {
            sexo = "F";
        }
        
        if( $("#sm:checked").val() ) {
            sexo = "M";
        }
        
        var url;
        rg = $("#rg").val();
        rg = rg.replace(".","");
        rg = rg.replace(".","");
        rg = rg.replace(".","");
        rg = rg.replace(".","");
        rg = rg.replace("-","");
        rg = rg.replace("-","");
        rg = rg.replace("-","");
        rg = rg.replace("-","");
        
        url = ""+
              "&nome="+$("#nomecompleto").val()+
              "&nomePai="+$("#nomepai").val()+
              "&nomeMae="+$("#nomemae").val()+
              "&dataNascimento="+$("#nascimento").val()+
              "&sexo="+sexo+
              "&cpf="+$("#cpf").val()+
              "&numDocumento="+rg+
              "&orgaoDocumento="+$("#rgemissor").val()+
              "&ufDocumento="+$("#rguf").val()+
              "&cep="+$("#cep").val()+
              "&logradouro="+$("#logradouro").val()+
              "&numero="+$("#logranumero").val()+
              "&complemento="+$("#logracomplemento").val()+
              "&bairro="+$("#lograbairro").val()+
              "&municipio="+$("#logramunicipio").val()+
              "&email="+$("#email").val()+
              "&telefone="+$("#telresid").val().replace(" ","").replace("-","")+
              "&celular="+$("#telcelular").val().replace(" ","").replace("-","")+
              "&nacionalidade="+$("#nacionalidade").val()+
              "&confirmEmail="+$("#email").val()+
              "&idMunicipio="+CodigoMunicipio+
              "&numRegPgu="+$("#numdoc").val()+
              "&categoriaAtual="+
              "&captchaResponse="+
              "&autorizaSMS=S"+
              "&autorizaEmail="+
              
              "";

//              "&tipoRegPgu="+$("#rgemissor").val()+
//              "&numRegPgu="+$("#rgemissor").val()+
//              "&categoriaAtual="+$("#rgemissor").val()+
//              "&autorizaSMS="+$("#rgemissor").val()+
//              "&autorizaEmail="+$("#rgemissor").val()+
//              "&captchaResponse="+$("#rgemissor").val()+
//              "&idMunicipio="+$("#rgemissor").val()+

        window.open("https://www.e-cnhsp.sp.gov.br/gefor/GFR/candidato/cadastro/preCadastro.do?method="+metodo+""+url,"_blank");
                          
                          
//                            &nomePai=Cadastro%20Teste
//                            &nomeMae=Cadastro%20Teste
//                            &dataNascimento=01/01/1980
//                            &sexo=F
//                            &cpf=385.738.207-48
//                            &numDocumento=00000000
//                            &orgaoDocumento=ssp
//                            &ufDocumento=SP
//                            &tipoRegPgu=0
//                            &numRegPgu=
//                            &categoriaAtual=
//                            &cep=18600-010
//                            &logradouro=R%20DOUTOR%20COSTA%20LEITE
//                            &numero=150
//                            &complemento=
//                            &bairro=CENTRO
//                            &municipio=BOTUCATU
//                            &telefone=
//                            &email=
//                            &celular=
//                            &confirmEmail=
//                            &autorizaSMS=S
//                            &autorizaEmail=S
//                            &captchaResponse=
//                            &idMunicipio=6249

    
    
    }
    
</script>

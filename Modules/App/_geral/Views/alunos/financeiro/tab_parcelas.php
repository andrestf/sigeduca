    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title text-blue">
                <center> Parcelas </center>
            </h3>
        </div> 
    </div>
    <br/>
    <div class="form" style="font-size: 18px;">
        <span class="text-blue">
            Total Pago: R$ <span class="totPago"></span>
        </span>
        <span class="text-red pull-right">
            Total Devido: R$ <span class="totDevido"></span>
        </span>
    </div>

    <br/><br/>
	
	
	    <div class="form" style="font-size: 18px;">
	    	<?php if($this->TipoAcesso("OPERADOR")) { ?>
	        <a href="javascript: void(0)" onclick="IniciaCancLote()" class="btn btn-default btnIniciaCancLote" title="Cancelamento em Lote"><i class="fa fa-trash"></i></a>

	        <a href="javascript: void(0)" onclick="CancLote()" class="btn btn-default btnCancLote" title="Cancelar Parcelas"><i class="fa fa-check"></i></a>

			<a href="javascript: void(0)" onclick="CancelaCancLote()" class="btn btn-default btnCancelaCancLote" title=" Desmarcar Cancelamento em Lote"><i class="fa fa-history"></i></a>
			<?php } ?>
			
			<span class="pull-right" style="font-size: 12px;">
	        	<input type="checkbox" name="cancelado" id="chkMostraCancelado" class="x-control" /> Mostra Cancelado
	        </span>
	    </div>

	    <br/><br/>
	

	<div class="form">
            <?php if(!$Parcelas) {
                echo '<div class="text-danger"> Aluno não possui parcelas lançadas! </div>';
            } else {?>
		<table class="table table-condensed table-hover table-striped table-grid">
			<thead>
				<tr>
					<th widht="25">#</th>
					<th>Tipo</th>
					
					<?php if(isset($_GET['all'])) { ?>
						<th width="120">Aluno</th>
					<?php } ?>

					<th>Descrição</th>

					<th>Vencimento</th>
					<th>Valor</th>
					<th>Valor Pago</th>
					<th>Dt. Paga</th>
                    <th width="40">Baixado</th>
					<th width="40">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php 
                                
                                $totValorPago = 0;
                                $totValorDevido = 0;
                                
                                foreach ($Parcelas as $parcela) {
                                	$styleCancelado = '';
                                    
                                        if($parcela['finmov_databaixa'] == '') {
                                            $classPaga = ''; 

                                            if($parcela['can_data'] == '')
                                                $totValorDevido = $totValorDevido+$parcela['finmov_valor'];
                                        } else {
                                            $classPaga = ' paga ';
                                            $totValorPago = $totValorPago + $parcela['finmov_valorbaixa'];
                                        }
                                        $classCancelado = "";
                                        if($parcela['can_data'] != '') {
                                            #$styleCancelado = 'background: #ffbbbb';
                                            $classCancelado = " cancelada ";
                                        }
                                        
					#($parcela['finmov_databaixa'] == '')       ? $classPaga = '';  : $classPaga = ' paga ';
					if ($parcela['finmov_dtvenc'] < date('Y-m-d') && $parcela['finmov_databaixa'] == '') {
					 	$classVenc = 'vencida';
					} else {
						$classVenc = ' ';
					}
				 ?>
					<tr class=' <?php echo $classPaga . $classVenc . $classCancelado ?> ' id="parc-<?php echo $parcela['finmov_id']; ?>" style="<?php echo $styleCancelado ?>">
						<td><?php echo $parcela['finmov_id']; ?></td>
						<td><?php echo $parcela['finmov_tipo']; ?></td>

						<?php if(isset($_GET['all'])) {
								if($parcela['finmov_alunoid'] != '' AND $parcela['finmov_alunoid'] != null) { ?>
								<td>
									<?php echo $Funcao->fRetCampo("sis_usuarios", "usu_nomecompleto", "usu_id = " . $parcela['finmov_alunoid']); ?>
								</td>
							<?php } ?>
						<?php } ?>

						<td><?php echo $parcela['finmov_descricao']; ?></td>
						<td><?php echo DataBR($parcela['finmov_dtvenc']); ?></td>
						<td>R$ <?php echo number_format($parcela['finmov_valor'],"2",",","."); ?></td>
						<td><?php echo ($parcela['finmov_valorbaixa'] == '') ? "" : 'R$ '.number_format($parcela['finmov_valorbaixa'],"2",",","."); ?></td>
						<td><?php echo DataBR($parcela['finmov_databaixa']); ?></td>
						<td>
							<?php if($parcela['finmov_databaixa'] != '') {?>
								<?php echo $Funcao->fRetCampo("sis_usuarios","usu_apelido","usu_id = ".$parcela['finmov_usubaixa']); ?>
							<?php } ?>
						</td>
						<td align="right">
						<?php if($this->TipoAcesso("OPERADOR")) { ?>
							<?php if($parcela['can_data'] == '' && $parcela['finmov_deonde'] != 'CREDITO') { ?>
								<?php if($parcela['finmov_databaixa'] == '') {?>
									<input type="checkbox" value="<?php echo $parcela['finmov_id']; ?>" name="cancLote[]" class="chkCancLote" > &nbsp;
									<i class='fa fa-download cursor' title="Baixar Parcela" id="btn_abrebxparcela" onclick="AbreParcela(<?php echo $parcela['finmov_id']; ?>,'<?php echo $parcela['finmov_tpdocum']; ?>','<?php echo $parcela['finmov_valor']; ?>')" data-toggle="tooltip"></i> &nbsp;
								<?php } else { ?>
								<i class='fa fa-undo cursor hide' title="Estornar Parcela" data-toggle="tooltip"></i> &nbsp;
	                            <a href="<?php echo Permalink('alunos','GeraRecibo','movimento='.$parcela["finmov_id"].'&codigo='.$ID_ALUNO)?>">
								 <i class='fa fa-print cursor' title="Imprimir Comprovante" data-toggle="tooltip"></i>
	                            </a>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						</td>
					</tr>
				<?php }?>
                                        
                        <script>
                            $(".totPago").html("<?php echo number_format($totValorPago,"2",",",".")?>");
                            $(".totDevido").html("<?php echo number_format($totValorDevido,"2",",",".")?>");
                        </script>        
			</tbody>
		</table>
		<div class="row col-md-12">
			<hr />
			
		</div>		
            <?php } //if Parcelas?>
            
	</div>

<style>
	.vencida {
		background: #ffd0d0 !important;
	}
	.cancelada {
		background: #ffb900 !important;
	}
</style>


	<script>
		$(".cancelada").hide();
		$("#chkMostraCancelado").change(function() {
			if(this.checked) {
				$(".cancelada").show();
			} else {
				$(".cancelada").hide();
			}
		})

	<?php if($this->TipoAcesso("OPERADOR")) { ?>
		$(".chkCancLote").hide();
			$(".btnIniciaCancLote").show();
			$(".btnCancLote").hide();
			$(".btnCancelaCancLote").hide();
		var bIniciaCancLote = false;
		
		function IniciaCancLote() {

			$(".btnIniciaCancLote").hide();
			$(".btnCancLote").show();
			$(".btnCancelaCancLote").show();


			if(bIniciaCancLote == false) {
				$(".chkCancLote").show();
				bIniciaCancLote = true;	
			} else {
				CancelaCancLote();
			}
					
		}

		function CancelaCancLote() {
			$('.chkCancLote').prop('checked', false);
			$(".chkCancLote").hide();
			bIniciaCancLote = false;		

			$(".btnIniciaCancLote").show();
			$(".btnCancLote").hide();
			$(".btnCancelaCancLote").hide();
		}

		function CancLote() {
			ids = "";
			$( $(".chkCancLote") ).each(function( index ) {
				if( $(this).prop("checked") == 1 ) {
					val = $(this).val();
					ids = ids + val + ",";
				}
			});

			if(ids == "") {
				alertify.error("Selecione ao menos uma parcela");
				return false;
			}

			alertify.confirm("Atenção","Confirmar Cancelamento de Parcelas",
				function(){
					///////////////////////////////////////////////////////////////
					$.ajax({
						type: 'post',
						data : {n : ids},
						url: "/index.php?route=Financeiro/CancelarParcelalote/",
						success: function(res) {
							if(res == 1) {
								alertify.success("Cancelamento Realizado");
							}

							if(res == 0) {
								alertify.error("Falha ao processar!");
							}

							setTimeout(function(){ location.reload(); },300)

							return;
						}
					})
					///////////////////////////////////////////////////////////////
				},
				function(){})




		}

	<?php } ?>

	</script>

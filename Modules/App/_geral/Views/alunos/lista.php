<section class="content asec-box" id="aluno">
    <?php if($this->TipoAcesso("OPERADOR")) {?>
        <div class="box">
            <div class="box-header">
                <a href="<?php echo $this->Link('alunos',"relatorio");?>" ><span class="btn btn-info btnRel"> <i class="fa fa-list-alt"></i> Relatórios</span> </a>
                <a href="<?php echo PermaLink('alunos',"cadastrar","tipo=".$_GET['tipo']);?>" ><span class="btn btn-primary btnAdd"> <i class="fa fa-plus"></i> Adicionar</span> </a>
                <a class='hide' href="<?php echo $this->Link('alunos',"processosgerais");?>" ><span class="btn btn-danger btnProcGerais"> <i class="fa fa-cogs"></i> Processos Gerais</span> </a>
                
            </div>
        </div>
    <?php } ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-graduation-cap"></i>
                <?php echo $view_PageTitle; ?> Cadastrados</h3>
            <div class="box-tools pull-right hide">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
            </div>
        </div>
        <?php if($this->TipoAcesso("OPERADOR")) {?>
        <div class="box-body" id="">
            <form id="frm_alunos_busca">
                <input type="hidden" name="tipo" value="<?php echo isset($_GET['tipo']) ? $_GET['tipo'] : 'a';  ?>" />
                <div class="row">
                    <div class="col-sm-4">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control" id="nome" />
                    </div>

                    <div class="col-sm-4">
                        <label>CPF</label>
                        <input type="text" name="cpf" class="form-control"/>
                    </div>
                    
                    <div class="col-sm-4">
                        <label>Situação</label>
                        <select class="form-control" name="situacao" >
                            <option value="">Todos</option>
                            <option value="1">Ativos</option>
                            <option value="2">Inativos</option>
                        </select>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-sm-4 hide">
                        <label>Fase</label>
                        <?php $faseDB = new DB(); $faseDB->CnCliente();?>
                        <?php $faseDB = $faseDB->ExecQuery("SELECT * FROM sis_servicositens WHERE serviten_ativo = 's' order by serviten_descricao ASC")?>
                        <select class="form-control" name="fase" >
                            <option value=""></option>
                            <?php while($fase = $faseDB->fetch_object()) {?>
                                <option value="<?php echo $fase->serviten_id?>"><?php echo $fase->serviten_descricao?></option>
                            <?php } ?>
                        </select>
                    </div>


                    <div class="col-sm-4">
                        <label>Matrícula</label>
                        <select name="matricula" class="form-control" id="matricula" tabindex="10">
                            <option value="">Selecione</option>
                            <?php while($mat = $matriculas->fetch_object()) {?>
                            <option data-servicos="<?php echo $mat->tpmat_servicos?>" data-itens="<?php echo $mat->tpmat_categoriascnh?>" value="<?php echo $mat->tpmat_id?>"> <?php echo $mat->tpmat_descricao; ?> </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label>Categorias</label>
                        <select name="categoria" id="categorias" class="form-control select2" tabindex="20" >
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label>Ordem</label>
                        <select class="form-control" name="ordem" >
                            <option value="id desc" selected="selected">Data Cadastro</option>
                            <option value="nomecompleto asc ">Nome</option>
                        </select>
                    </div>



                </div>
                <div class="row">

                    
                    <div class="col-sm-4">
                        <label>Limite</label>
                        <select class="form-control" name="limite" id="limite" >
                            <option value="10" selected="selected">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                            <option value="300">300</option>
                            <option value="500">500</option>
                            <option value="*">Sem Limite</option>
                        </select>
                    </div>
                    <div class="col-sm-8">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                        </span>
                    </div>
                </div>
                    
                
            </form>
            <hr/>
            <span class="pull-right" style="font-size: 12px;">
            <input type="checkbox" name="cancelado" id="chkMostraCancelado" class="x-control" /> Mostra Cancelado
        </span>
        <?php } else { ?>
        <div class="box-body">
            <span class="pull-right" style="font-size: 12px;">
            <input type="checkbox" name="cancelado" id="chkMostraCancelado" class="x-control" /> Mostra Cancelado            
        </span>
        </div>
        <?php } ?>
            <div id="lista_alunos"></div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->


<script>

    $(function(){
        ListaAlunos();

    });
    
    $("#frm_alunos_busca").submit( function(e){
        e.preventDefault();
        ListaAlunos();
    });
    
    
    $("#chkMostraCancelado").change(function() {
        if(this.checked) {
            $(".cancelado").show();
        } else {
            $(".cancelado").hide();
        }
    })    
    
    $(function(){

        <?php if(!$this->TipoAcesso("OPERADOR")) {?>
            $("#chkMostraCancelado").click();
             $(".cancelado").show();
        <?php } ?>

    })
    function ListaAlunos() {

        if( $("#limite").val() == "*" ) {

            if( $("#nome").val().length <= 4 ) {
                alertify.error("Nome deve conter no minimo 5 caracteres");
                return false;
            }
        }

        $("#lista_alunos").html( $("#ld").html() );
        $datas = $("#frm_alunos_busca").serialize();
        $.ajax({
            type: 'get',
            url : "/index.php?route=alunos/listagem/?&"+$datas,
            success: function(res) {
                $("#lista_alunos").html(res);
                $(".cancelado").hide();
            }
            
        })
    };
    
    
        
        function EditarAluno(id) {
            tipo = "<?php echo ( isset($_GET['tipo']) ? $_GET['tipo'] : "a");?>";
            linkx = "<?php echo $this->Link("alunos","editar")?>";
            window.location = linkx + "/&codigo="+id+"&tipo="+tipo;
        }


        $("#matricula").change(function(){

            //SELECIONANDO CATEGORIAS DO PROCESSO
            $("#categorias").html('<option>CARREGANDO VALORES</option>');
            proc_itens = $("#matricula option:selected" ).attr("data-itens");
            proc_id = $("#matricula option:selected").val();
            //$("#servicos").select2('val', 1);
            $("#section_servicos").fadeOut();
            $.ajax({
                url : "/index.php?route=processos/CategoriasMatricula/&ids="+proc_itens+"&procid="+proc_id,
                success: function(e) {
                    $("#categorias").html('');
                    $("#categorias").html(e);
                    
                    if(proc_id > 0) {
                        $("#section_servicos").fadeIn();
                    } else {
                        $("#section_servicos").fadeOut();
                    }
                    
                },
                error : function(){
                    alertify.error("Erro ao recuperar itens do processo!");
                }
            });

            //SELECIONANDO SERVIÇOS DO PROCESSO
            proc_id = $("#matricula option:selected").val();
            proc_itens = $("#matricula option:selected" ).attr("data-itens");
            servicos_id = $("#matricula option:selected" ).attr("data-servicos");
            $.ajax({
                url : "/index.php?route=matricula/ServicosMatricula/&ids="+proc_itens+"&servicos="+servicos_id+"&procid="+proc_id,
                success: function(e) {
                    $("#servicos").html('');
                    $("#servicos").html(e);
                    
                },
                error : function(){
                    alertify.error("Erro ao recuperar itens do processo!");
                }
            });

            
        });
    

</script>
    


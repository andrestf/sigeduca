<section class="content asec-box" id="aluno">
    <div class="box">
        <div class="box-header">
            <a href="<?php echo $this->Link('alunos',"relatorio");?>" ><span class="btn btn-default btnRel"> <i class="fa fa-list-alt"></i> Relatórios</span> </a>
            <a href="<?php echo $this->Link('alunos',"cadastrar");?>" ><span class="btn btn-default btnAdd"> <i class="fa fa-plus"></i> Adicionar</span> </a>
        </div>
    </div>
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-list"></i>
                Tipo de Documentos</h3>
            <div class="box-tools pull-right hide">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body" id="">
            <form id="frm_alunos_busca">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Descrição</label>
                        <input type="text" name="nome" class="form-control" />
                    </div>
 
 
                    <div class="col-sm-4">
                        <label>Situação</label>
                        <select class="form-control" name="situacao" >
                            <option value="">Todos</option>
                            <option value="1">Ativos</option>
                            <option value="2">Inativos</option>
                        </select>
                    </div>
   
                
                    <div class="col-sm-4">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            
                            <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Filtrar </button>
                            
                        </span>
                    </div>
                </div>
                    
                
            </form>
            <hr/>
            <div id="lista_tpdocumentos"></div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<script>
    function CarregaLista() {
        alert('Carrega lista');
        
        $.ajax({
            datas : '',
            url : ""
        })
    }
    
    $(function() {
        CarregaLista();
    })
</script>
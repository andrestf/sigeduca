
<?php

$report = new FinanceiroRelatoriosModel();
$report->Campos        = " finmov_descricao, finmov_tipo  ";
$report->CondicaoExtra = " and finmov_deonde = 'evt_ingressos' ";
$report->OrderBy = " order by f.finmov_alunoid, f.finmov_descricao ";
$report->GroupBy = "";

$report->Debug = false;

$dados  = $report->Gerar();

$Funcoes = new UsuariosHelper();

if($dados->num_rows < 1) {
	echo "sem resultado";
	return;
} ?>

<div class='content'>
	<div class='formx'>
	<h3>
		Registro de Contas a Receber  - Analítico por codigo de aluno
	</h3>
        <h6>
	<div class="row">
		<div class="col-sm-12">
			<b>Lçto Inicial: </b><?php echo $_POST['datamov_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Lçto Final: </b><?php echo $_POST['datamov_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<b>Baixa Inicial: </b><?php echo $_POST['databxmov_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Baixa Final: </b><?php echo $_POST['databxmov_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                        
                        <b>Baixa Real Inicial: </b><?php echo $_POST['databxreal_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Baixa Real Final: </b><?php echo $_POST['databxreal_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                        
			<b>Vcto Inicial: </b><?php echo $_POST['venc_ini']?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Vcto Final: </b><?php echo $_POST['venc_fim']?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Usuário:</b> <?php 
                        if(isset( $_POST['usubaixa']) ) {
                         	foreach( $_POST['usubaixa'] as $usuXX) {
                            	echo  $Funcoes->fRetCampo('sis_usuarios','usu_apelido',"usu_id = '$usuXX'") . ", ";
                        	};
                        } ?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Documento:</b> <?php echo $Funcoes->fRetCampo('sis_tpdocum','tpd_descricao',"tpd_cd = '". $_POST['tpdoc']."'")?> 
		</div>
	</div>
	<hr/>

	<table class="table table-condensed">
		<thead>
			<tr>
				<th width='70'>Aluno</th>
				<th width="70">Resp</th>
				<th width="400">Nome</th>
				<th width="190">Descrição</th>
				<th width="50">Doc.</th>
				<th width='110'>Valor</th>
				<th width='110'>Ingressos</th>
			</tr>
		</thead>

		<tbody>
			<?php $valor = ""; ?>
		 	<?php while ($report = $dados->fetch_assoc()) {
		 			$valor = $valor + $report['finmov_valor'];
		 		?>
	 			<tr>
	 				<td><?php echo ($report['finmov_alunoid']);?></td>
	 				<td><?php echo ($report['finmov_alunoidf']);?></td>
                    <td><?php echo $report['nome_aluno'] ?> <br/></td> 
	 				<td><?php echo $report['finmov_tipo'] . "|". $report['finmov_descricao']; ?></td>
	 				<td><?php echo substr($report['tpd_descricao'],0,8);?></td>
	 				<td>R$ <?php echo number_format($report['finmov_valor'],2,",",".");?></td>
	 				<td> <?php echo ($report['finmov_valor']/25);?></td>
	 			</tr>
	 		<?php } ?>
	 	</tbody>

	 	<tfoot>
	 		<tr>
	 			<td colspan="6" align="right">
	 				<hr/>
	 				<b>
	 				VALOR TOTAL A RECEBER: R$ <?php echo number_format($valor,2,",","."); ?>
					</b>
	 			</td>
	 		</tr>
	 	</tfoot>
	</table>
        </h6>
	</div>
</div>	
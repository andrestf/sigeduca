<section class="content asec-box" id="aluno">
    <h3 class="page-title">
        <?php echo $view_PageTitle ?>
    </h3>

    <br/>



    <form action="/index.php?route=Financeiro/TipoDocumentos<?php echo $acao; ?>/&editar=<?php echo $doc['tpd_cd']; ?>" method="post">
    <div class="box asex-box">
        <input type='hidden' class='form-control' name="idEdit"  value="<?php echo $doc['tpd_id']; ?>" />
        <div class="box-body">

            <div class="container">
                <div class="row">
                    <div class="col-md-4" >
                        <div class="panel">
                            <div class="panel-body">
                                <label>Código: </label>     <input type='text' name="codigo" class='form-control' value="<?php echo $doc['tpd_cd']; ?>" />
                                <label>Descrição: </label>  <input type='text' name="descricao" class='form-control'  value="<?php echo $doc['tpd_descricao']; ?>" />
                                <label>Tipo : </label>     
                                <select name="destino" class="form-control">
                                    <option value="V" <?php echo ($doc['tpd_destino'] == 'V') ? 'selected="selected"' : ''?> >A Vista</option>
                                    <option value="P" <?php echo ($doc['tpd_destino'] == 'P') ? 'selected="selected"' : ''?> >A Prazo</option>
                                </select>
                                
                            </div>

                            <div class="panel panel-footer">
                                <input type="submit" value="Confimar" class="btn btn-primary" />
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>              

        </div>
    </div>
    </form>

</section>

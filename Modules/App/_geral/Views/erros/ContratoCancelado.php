<div class="content asec-box">
	<center>
		<h1 class="alert alert-danger">Contrato/Matrícula cancelada</h1>
	</center>
	<h4>
		Operação não permitada para contratos cancelados...
		<br/><br/>

		<a href="javascript: history.back(-1)">Voltar</a>
	</h4>

</div>
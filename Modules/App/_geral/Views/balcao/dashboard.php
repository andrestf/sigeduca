<div class="content">
    <h3 class="page-title">Balcão</h3>
    <br/>
    <div class="box box-body"> 
	    <div class="row">
	    	<div class="col-sm-4">
				<div class="box no-border">      
		            <a href="/index.php?route=balcao/vendas/&action=nova" data-bind="alt+v" id="btnNovaVenda" style="color: #000 !important;" ><span style=" font-size: 20px; line-height: 26px" class="btn btn-info btn-flat btn-block"> <span style="font-size: 18px;"><i class="fa fa-money"></i></span> <br/> <u>V</u>endas</span> </a>
		        </div>
		    </div>

	    	<div class="col-sm-4">
				<div class="box no-border">      
		            <a href="/index.php?route=balcao/vendas/&action=nova" data-bind="alt+n" id="btnNovaVenda" style="color: #000 !important;" ><span style=" font-size: 20px; line-height: 26px" class="btn btn-success btn-flat btn-block"> <span style="font-size: 18px;"><i class="fa fa-shopping-cart"></i></span> <br/> <u>N</u>ova Venda</span> </a>
		        </div>
		    </div>

		    <div class="col-sm-4">
				<div class="box no-border">      
		            <a href="/index.php?route=balcao/produtos/&action=index" data-bind="alt+p" id="btnGerProd"  style="color: #000 !important;" ><span style=" font-size: 20px; line-height: 26px" class="btn btn-primary btn-flat btn-block"> <span style="font-size: 18px;"><i class="fa fa-archive"></i></span> <br/> Gerenciar <u>P</u>rodutos</span> </a>
		        </div>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="box box-primary">
				<div class="box-body text-center">
					<label>VENDAS</label>
					<div class="infos infos-text">4</div>
				</div>
			</div>
		</div>

		<div class="col-sm-8">
			<div class="box box-success text-green">
				<div class="box-body text-center">
					<label>SALDO</label>
					<div class="infos infos-text">R$ 40,00</div>
				</div>
			</div>
		</div>
	</div>
		

</div>


<script>
	shortcuts = ["alt+n","alt+p","alt+v"];
</script>

<style type="text/css">
	.infos {  }
	.infos-text {
		font-size: 3em;
	}
</style>
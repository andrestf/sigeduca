<script type="text/javascript">
	$("body").addClass("sidebar-collapse");


	shortcuts = ["alt+return"];
</script>

<section class="content form">

	<div class="hide btn-group btn-group-lg" role="group" aria-label="Basic example">
	  <button type="button" class="btn btn-primary">Salvar</button>
	  <button type="button" class="btn btn-primary">Cliente</button>
	  <button type="button" class="btn btn-primary">Desc.</button>
	  <button type="button" class="btn btn-primary">Obs</button>
	</div>

	<hr />
	<div class="boxx">
		<div class="row">
			<div class="col-sm-6">
				<label>Produto</label>
				<select class="form-control js-data produtos"></select>
			</div>
			<div class="col-sm-1">
				<label>Qtd:</label>
				<input type="number" name="vendaProdQtd" id="vendaProdQtd" value="1" placeholder="" class="form-control myFrm" />
				</div>

			<div class="col-sm-2">
				<label>Preço:</label>
				<input type="text" name="vendaProdPreco" id="vendaProdPreco" value="R$ " placeholder="" class="form-control myFrm frmreadonly" readonly="readonly"/>
				</div>

			<div class="col-sm-2">
				<label>Total</label>
				<input type="text" name="vendaProdTotal" id="vendaProdTotal" value="R$ " placeholder="" class="form-control myFrm frmreadonly" readonly="readonly"/>
				</div>

			<div class="col-sm-1">
				<label>Lancar</label>
				<button class="btn myFrm btnAddProd"  data-fn="AddProd()" onclick="AddProd();" data-bind="alt+return" style="width: 55px;"><i class="fa fa-check"></i></button>
				</div>
		</div>
		<br>
		<div class="box box-primary box-body">
				<div class="row" style="font-weight: bold; border-bottom: solid 2px #eee; padding-bottom: 5px; font-size: 20px; color: #5f5f5f;">
					<div class="col-sm-5">Item</div>
					<div class="col-sm-2">Qtd</div>
					<div class="col-sm-2">Preço</div>
					<div class="col-sm-2">SubTotal</div>
					<div class="col-sm-1">&nbsp;</div>
				</div>
				<div class="contentItems"></div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-right txtInfos">
					Total: 
					<span class ="vlrFinal" > R$ 00,0</span>
				</div>
			</div>			
			
		</div>
	</div>
</section>


<script type="text/javascript">
	var nTotProds;
		nTotProds = 0;
	var preco;
	var qtd;
	var precoFinal;
	var precoFinalTotal;
	precoFinalTotal = 0;
	var idprod, prodNome;
	var prods;
		prods = [];
	$(function() {

		$('.js-data').select2({
		  ajax: {
		  	delay: 700,
		  	minimumInputLength: 4,
		    url: function (params) {
		    	if( params.term  && params.term.length >= 4) {
		    		busca = params.term;
		    		return  'index.php?route=balcao/produtos/&action=busca&filtro=true&nome='+busca+'&codigo=&situacao=&limite=20&json';
		    	}
		    },
		    dataType: 'json'
		  }
		});	
	
		$('.js-data').on('select2:select', function (e) {
		  $("#vendaProdQtd").focus();
		  	preco = e.params.data.preco;
		  	qtd = e.params.data.qtd;
		  	idprod = e.params.data.id;
		  	prodNome = e.params.data.text;

		  	$("#vendaProdPreco").val("R$ " + preco);
		  	$("#vendaProdTotal").val("R$ " + preco);

		  	preco = parseFloat(preco).toFixed(2);
		});	

		$('.js-data').select2('open');

		$("#vendaProdQtd").on("input", function() {
			precoFinal = parseInt($("#vendaProdQtd").val()) * preco;
			//precoFinal = parseFloat(precoFinal);
			//precoFinal = precoFinal;
			$("#vendaProdTotal").val("R$ " + precoFinal.toFixed(2));
		})
	})

</script>
<style type="text/css">
	.contentItems .row {
		color: black;
	    padding-top: 10px;
	    border-bottom: solid 1px #e1e1e1;
	    padding-bottom: 10px;
	}

	.myFrm {
		height: 45px;
	}

	.frmreadonly {
		background: #eee;
	}

	.select2-container .select2-selection--single {
	    height: 45px; 
	}

	.txtInfos {
    padding-right: 25px;
    font-size: 30px;
    color: #d03030;
    font-weight: bold;		
	}
</style>

<script type="text/javascript">
	function AddProd() {
		
		qtd = $("#vendaProdQtd").val();

		if(idprod <= 0 || idprod == "") {
			alertify.error("Selecione um produto");
			return;
		}

		if(qtd <= 0 || qtd == "") {
			alertify.error("Quantidade inválida!");
			return;
		}

		
		nTotProds = nTotProds+1;
		var ar = { n: nTotProds, prod: idprod, qtd: parseInt(qtd),  }
		prods.push(ar);

		console.log(prods);

		precoFinal = parseInt($("#vendaProdQtd").val()) * preco;
		precoFinalTotal = parseFloat((precoFinalTotal + precoFinal));
		$(".vlrFinal").html("R$ " + precoFinalTotal.toFixed(2));
		precoFinalTotal = precoFinalTotal;

		html = '';
		html = html + '					<div class="row" id="item'+idprod+'" precoFim="'+precoFinal+'" >';
		html = html + '						<div class="col-sm-5 prodNome">'+nTotProds+' - '+prodNome+'</div>';
		html = html + '						<div class="col-sm-2 vendaProdQtdTD">'+qtd+'</div>';
		html = html + '						<div class="col-sm-2 prodPreco">R$ '+preco+'</div>';
		html = html + '						<div class="col-sm-2 prodPrecoFinal">R$ '+precoFinal.toFixed(2)+'</div>';
		html = html + '						<div class="col-sm-1"> <button class="btn btn-xs btn-primary" onclick="RemProd('+idprod+','+precoFinal+','+nTotProds+')"> <i class="fa fa-trash"></i> </button> </div>';
		html = html + '					</div>';
		$(".contentItems").append(html);

		
		idprod = 0;
		qtd = 0;
		$("#vendaProdQtd").val('1');
		$("#vendaProdPreco").val('');
		$("#vendaProdTotal").val('');
		$('.produtos').select2('open');
		$('.produtos').select2("val", "0");

	}

	function RemProd(prod,vlr,itemArray) {
		nTotProds = nTotProds-1;
		//console.log("precoFinalTotal = " + precoFinalTotal)
		precoFinalTotal = parseFloat((precoFinalTotal - vlr));
		$(".vlrFinal").html("R$ " + precoFinalTotal.toFixed(2));

		$("#item"+prod).remove();
	}
</script>
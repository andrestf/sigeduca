<script type="text/javascript">
	
	shortcuts = ["Alt+b","Alt+n","Alt+return"];

</script>

 <div class="content">
        <h3 class="page-title">
            Gerenciar Produtos
        </h3>
        <br/>

	<div class="box box-body">
		<a href="<?php echo permalink("balcao","","")?>"                      data-bind="Alt+b" class="btn btn-small btn-primary btn-flat text-blackx"><i class="fa fa-shopping-cart"></i> <u>B</u>alcao</a>
	    <a href="<?php echo permalink("balcao","produtos","action=novo")?>"   data-bind="Alt+n" class="btn btn-small btn-primary btn-flat text-greenx"><i class="fa fa-plus"></i> <u>N</u>ovo</a>
	</div>

	<div class="box box-body">	
			<form id="frmBuscaProd">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Nome</label>
                        <input type="text" data-fn='$("#frmBuscaProd")[0].reset(); $("#nome").focus();' name="nome" class="form-control" id="nome" data-bind="Alt+return" autocomplete="off" />
                    </div>

                    <div class="col-sm-4">
                        <label>Código</label>
                        <input type="text" name="codigo" id="codigo" class="form-control"/>
                    </div>
                    
                    <div class="col-sm-4">
                        <label>Situação</label>
                        <select class="form-control" name="situacao" >
                            <option value="">Todos</option>
                            <option value="ATIVO">Ativos</option>
                            <option value="INATIVO">Inativos</option>
                        </select>
                    </div>
                </div>        
				
				<div class="row">
 					<div class="col-sm-4">
                        <label>Categorias</label>
                        <select name="categoria" id="categorias" class="form-control select2x" tabindex="20" > </select>
                    </div>

					<div class="col-sm-4">
                        <label>Limite</label>
                        <select class="form-control" name="limite" id="limite" >
                            <option value="10">10</option>
                            <option value="20"  selected="selected">20</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                            <option value="300">300</option>
                            <option value="500">500</option>
                            <option value="*">Sem Limite</option>
                        </select>
                    </div>                    
                

                
                    <div class="col-sm-4">
                        <span class="pull-right">
                            <label>&nbsp;</label><br/>
                            <button data-fn="$('#frmBuscaProd').submit();" id="btnSendForm" class="btn btn-success" data-bind="Alt+f"> <i class="fa fa-search"></i> Filtrar </button>
                        </span>
                    </div>
                </div>
             </form>

             <hr/>

             <table class="table table-hover table-striped table-condensed">
             	<thead>
             		<tr>
             			<th>Cód</th> 
             			<th>Status</th>
             			<th>Nome</th>
             			<th>Estoque</th>
             			<th>Vlr. Venda</th>
             		</tr>
             		<tbody id="tBodyRes">
             			
             		</tbody>
             	</thead>
             </table>
	</div>

</div>

<script>
    $(function() {
        $('#frmBuscaProd').submit();


    
        <?php $a = ''; if ( isset($_GET['a']) && $_GET['a'] !=  '') { $a = $_GET['a']; }?>
        <?php $r = ''; if ( isset($_GET['r']) && $_GET['r'] !=  '') { $r = $_GET['r']; }?>
        <?php $s = ''; if ( isset($_GET['s']) && $_GET['s'] !=  '') { $s = $_GET['s']; }?>

            <?php if($a == 'edit') { ?>
                <?php if($r == '1') { ?>
                    alertify.success("Registro editado com sucesso!");
                <?php } else {?>  
                    alertify.warning("Nenhum regitro alterado!");
                <?php } ?>
            <?php } ?>

            <?php if($a == 'add') { ?>

                <?php if( $s >= 1) { ?>
                    alertify.success("Registro adicionado com sucesso!");
                <?php } else {?>  
                    alertify.error("Falha ao salvar!");
                <?php } ?>
            <?php } ?>
           

        

    });
	$("#nome").focus();
	$("#frmBuscaProd").on("submit", function(e) {
		e.preventDefault();

		$("#tBodyRes").html("carregando");

		var datas = $("#frmBuscaProd").serializeArray();
		$.ajax({
			data : datas,
			url : "<?php echo permalink('balcao','produtos','action=busca&filtro=true'); ?>",
			success: function(res) {
				$("#tBodyRes").html(res);
				click = true;
			}
		});
	});
</script>
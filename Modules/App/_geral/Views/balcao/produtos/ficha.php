<?php

$config = new ConfigHelper();
$nMktUp = $config->Get('nMktUp');
$bValAuto = $config->Get('bValAuto');

?>

<script type="text/javascript">
	shortcuts = ["Alt+s","Alt+c","Alt+e","Alt+a","Alt+c"];
</script>

 <div class="content"> 
        <h3 class="page-title">
            <?php echo $TituloFicha; ?>
        </h3>
        <br/>

	<div class="box box-body">
		<a href="<?php echo permalink("balcao","produtos","")?>" data-bind="Alt+v" class="btn btn-small btn-primary btn-flat text-blackx"><i class="fa fa-arrow-left"></i> <u>V</u>oltar </a>
		
	    <a data-bind="Alt+s" data-fn="saveFrm();" id="" onclick="saveFrm();" class="btn btn-small btn-success btn-flat text-greenx" ><i class="fa fa-save"></i> <u>S</u>alvar</a>
	   	<span class="pull-right">
	    	<?php if($nextAct == 'editar') {?>
	    		<a data-bind="Alt+a" data-fn="ajusteEstoque();" id="" onclick="ajusteEstoque();" class="btn btn-small btn-default btn-flat text-black" ><i class="fa fa-edit"></i> <u>A</u>justar Estoque</a>
	    	<?php } ?>
	    	<a href="<?php echo permalink("balcao","produtos","")?>" data-bind="Alt+c" class="btn btn-small btn-danger btn-flat text-blackx" class="pull-right"><i class="fa fa-close"></i> <u>C</u>ancelar </a>
    	</span>
	</div>

	<form action="<?php echo permalink("balcao","produtos","action=save")?>" method="post" id="frmProd">
		<input type="hidden" name="id" value="<?php echo $produto['prod_id']?>">
		<input type="hidden" name="nextAct" value="<?php echo $nextAct?>">

		<div class="box box-body">
			<div class="form-group">
				<div class="row">
		            <div class="col-sm-6 bottom10">
		                <label for="nome">Produto <span class="obg">*</span></label>
		                <input id="nome" name="nome" placeholder="nome" maxlength="100" class="form-control" type="text" onkeyup=""  value="<?php echo $produto['prod_nome']; ?>"  requiredx="">
		            </div>
	            </div>

				<div class="row">
		            <div class="col-sm-6 bottom10">
		                <label for="produto">Categoria <span class="obg">*</span></label>
		                <select class="form-control"></select>
		            </div>
	            </div>	            

	            <hr>

	            <div class="row bottom10">
		            <div class="col-sm-3 ">
		                <label for="vlrcusto">Preço de Custo <span class="obg"></span></label>
		                <input id="vlrcusto" name="vlrcusto" placeholder="Preço de Custo" maxlength="100" class="form-control" type="text" onkeyup="formataValorNovo(this); calcNewVal(this)" value="<?php echo $produto['prod_vlrcusto']; ?>" requiredx="">
		            </div>

		            <div class="col-sm-3 ">
		                <label for="vlrvenda">Preço de Venda <span class="obg"></span></label>
		                <label class="hide pull-right text-small" style="font-size: 10px;">	<input type="checkbox" id="chkAutomatico" /> <b> Automático </b></label>
		                <input id="vlrvenda" name="vlrvenda" placeholder="Preço de Custo" maxlength="100" class="form-control" type="text" onkeyup="formataValorNovo(this)" value="<?php echo $produto['prod_vlrvenda']; ?>" requiredx="">
		            </div>

		            <div class="col-sm-3 hide">
		                <label for="marketup">Marketup %<span class="obg"></span></label>
		                <input id="marketup" name="marketup" placeholder="" maxlength="100" class="form-control" type="text" onkeyup="formataValorNovo(this); calcNewVal(this);" value="<?php echo $produto['prod_marketup']; ?>" requiredx="">
		            </div>
		        </div>

		        <hr>

		        <div class="row bottom10">
		        	<div class="col-sm-3 hide ">
		                <label for="qtdAtual">Estoque Atual <span class="obg"></span></label>
		                <input id="qtdxxx" name="" placeholder="" maxlength="100" class="form-control" type="number" onkeyup="" value="" requiredx="">
		            </div>
		        </div>

		        <div class="row">
		        	<div class="col-sm-3 ">
		                <label for="qtdMin">Estoque Mínimo<span class="obg"></span></label>
		                <input id="qtdMin" name="qtdMin" placeholder="" maxlength="100" class="form-control" type="number" onkeyup="" value="<?php echo $produto['prod_qtdmin']; ?>" requiredx="">
		            </div>

		            <div class="col-sm-3 ">
		                <label for="qtdMax">Estoque Máximo <span class="obg"></span></label>
		                <input id="qtdMax" name="qtdMax" placeholder="" maxlength="100" class="form-control" type="number" onkeyup="" value="<?php echo $produto['prod_qtdmax']; ?>" requiredx="">
		            </div>
		        </div>

				<div class="row">
		        	<div class="col-sm-3">
		        		<label><b> Permitir venda sem estoque ? </b></label>
		                <select name="vendasemestoq" class="form-control">
		                	<option <?php echo ($produto['prod_vendasemestoq'] == "1") ? "selected='selected'" : ''; ?> value="1">Sim</option>
		                	<option <?php echo ($produto['prod_vendasemestoq'] == "") ? "selected='selected'" : ''; ?> value="">Não</option>
		                </select>
		        	</div>

		        	<div class="col-sm-3">
		        		<label><b>Controla estoque</b></label>
		        		<select name="crtestoque" class="form-control">
		                	<option <?php echo ($produto['prod_crtestoque'] == "1") ? "selected='selected'" : ''; ?> value="1">Sim</option>
		                	<option <?php echo ($produto['prod_crtestoque'] == "") ? "selected='selected'" : ''; ?> value="">Não</option>
		                </select>
		        	</div>
		        </div>

		        <hr>
            </div>			
		</div>
	</form>

</div>	
<?php if($nextAct == 'editar') {?>
	<div class="modal" tabindex="-1" role="dialog" id="modalEdtEstoque">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h3 class="modal-title">Ajustar Estoque</h3>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	
	      	<div class="row">
	      		<div class="col-sm-12">
	        		<label>Tipo</label>
	        	</div>
	      	</div>
	      	
	      	<div class="row">
	      		<div class="col-sm-12">
			        <div class="tipoBox" data-tipo="e" >Entrada</div>
			        <div class="tipoBox" data-tipo="s" >Saída</div>
	      		</div>
	      	</div>
	      	<br />

	      	<div class="row">
	      		<div class="col-sm-4">
		      		<label>Quantidade</label>
		      		<input  type="number" id="frmTxtEstoqueQtd" value="" name="qtd" class="form-control " />
	      		</div>
	      	</div>
	      	<br />

	      	<div class="row">
	      		<div class="col-sm-12">
		      		<label>Motivo</label>
		      		<input  type="text" id="frmTxtEstoqueMotivo" value="" name="motivo" class="form-control" />
	      		</div>
	      	</div>
	      	<br />



	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary btnSalvaEstoque" onclick="SalvaEstoque();">Salvar</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	      </div>
	    </div>
	  </div>
	</div>

	<style type="text/css">
		.tipoBox {
			cursor: pointer;
			width: 80px;
		    border: solid 1px #e1e1e1;
		    margin-right: 9px;
		    text-align: center;
		    vertical-align: middle;
		    line-height: 71px;
		    background: #f1f1f1;
		    color: #000000;
		    border-radius: 4px;
		    float: left;
		}

		.selected {
		    background: #cabee2;
		    border: solid 1px #4b4694;
		    border-bottom: solid 4px #4b4694;
		    color: #39377b;
		    font-weight: bold;
		}
	</style>
	<script type="text/javascript">
		var tipo, qtd, descri;
			tipo = "";
			qtd  = "";
			descri = "";


		$(".tipoBox").on("click", function() {
			tipo = $(this).attr("data-tipo");
			$(".tipoBox").removeClass("selected");

			$(this).addClass("selected");

			//$("#frmTxtEstoqueQtd").val('');
			$("#frmTxtEstoqueQtd").focus();
		});


		$('#modalEdtEstoque').on('hidden.bs.modal', function (e) {

		  	$("#frmTxtEstoqueQtd").val('');
		  	$("#frmTxtEstoqueMotivo").val('');
		  	$(".tipoBox").removeClass("selected");

		  	tipo = "";

		})
		function ajusteEstoque() {
			$("#modalEdtEstoque").modal('show');
		}


		function SalvaEstoque() {
			if(tipo == "") {
				alertify.alert("Atenção","Informe o tipo de movimentação!");
				return;
			}

			qtd = $("#frmTxtEstoqueQtd").val();
			descri = $("#frmTxtEstoqueMotivo").val();

			if(qtd == "") {
				alertify.alert("Atenção","Informe a quantidade");
				return;
			}

			if(descri == "") {
				alertify.alert("Atenção","Informe o motivo");
				return;
			}



			alertify.confirm("Atenção", "Confirmar movimentação de estoque?",
				function() {
					SalvaEstoque2();

					return;
				},

				function() {

				})

		}


		function SalvaEstoque2() {
			$.ajax({
				type: "post",
				data: { produto :<?php echo $produtoID; ?>, tipo : tipo, qtd : qtd , descri: descri },
				url : '/index.php?route=Balcao/produtos/&action=AltEstoque',
				success: function(e) {
					if(e == 1) {
						alertify.success("Estoque ajustado com sucesso!");
						$("#modalEdtEstoque").modal("hide");
						return;
					}
					$("#modalEdtEstoque").modal("hide");
					alertify.error("Falha ao ajustar estoque!");
					return;
				},

				error: function() {
					alert("ERROR");
				}
			})
		}



	</script>
<?php } ?>


<script type="">

	$(function() {
		mkt = $("#mktup").keyup();	
		$("#vlrvenda").keyup();
		$("#vlrcusto").keyup();
	});
	<?php if($bValAuto == '1') { ?>
		$('#chkAutomatico').attr("checked","checked");
		$("#vlrvenda").attr("disabled","disabled");
		console.log("a");
	<?php } ?>

    $('#chkAutomatico').change(function() {
        if($(this).is(":checked")) {
            $("#vlrvenda").attr("disabled","disabled");
            $("#vlrvenda").val("");
            $("#mktup").removeAttr("disabled");

            $("#mktup").focus();
        } else {
        	$("#vlrvenda").removeAttr("disabled");
            $("#mktup").attr("disabled","disabled");
            $("#mktup").val("");
            $("#vlrvenda").focus();
        }
        
    });

    function saveFrm() {
    	$("#frmProd").submit();
    }
	function calcNewVal(e) {
		var mkt = $("#mktup").val();
			mkt = transNum(mkt)/100;

		var vlrCusto = $("#vlrcusto").val();
			vlrCusto = transNum(vlrCusto);
			
		var vlrVenda = $("#vlrvenda").val();
			vlrVenda = transNum(vlrVenda);
			

		if ( $("#chkAutomatico").is(':checked') ) {


			newVlrVenda = mkt*vlrCusto;
			newVlrVenda = newVlrVenda + vlrCusto;
			newVlrVenda = newVlrVenda;

			$("#vlrvenda").val(newVlrVenda*100);
			$("#vlrvenda").keyup();


		}




	}
	

	function transNum(num)  {
		num = num.replace(","," ");
		num = num.replace(".","");
		num = num.replace(" ",".");

		num = parseFloat(num);
		return num;
	}
</script>
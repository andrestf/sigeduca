<?php

$eventoLayout 	 =  "!n ;!n ;a03;a04;a05;!n;!n3;a6;a7;a8;!n2   ;!#;";
$eventoLayout 	.=  "b01;b02;b03;b04;b05;!n;!n3;b6;b7;b8;b9;b10;b11;!#;";
$eventoLayout 	.=  "c01;c02;c03;c04;c05;!n;!n3;c6;c7;c8;c9;c10;!#;";
$eventoLayout 	.=  "w01;w02;w03;w04;w05;!n;!n3;w6;w7;w8;w9;w10;!#;";
$eventoLayout 	.=  "k01;k02;k03;k04;k05;!n;!n3;k6;k7;k8;k9;k10;k11;!#;";
$eventoLayout 	.=  "x01;!n3        ;x05;!n;!n3;x6;!n3     ;x10;!#;";

$cadeirasOcupadas = "a1;a6;a3;c9;b2";
$cadeirasOcupadas = explode(";",$cadeirasOcupadas);
$nMinIngressos = "3";
$nMaxIngressos = "5";
?>



<?php
$assentos = explode(";", $eventoLayout);

$out = "";
foreach ($assentos as $assento) {

	//espaço multiplicados
	if(substr($assento,0,1) == '!') {
		$assento = trim($assento);
		$code = substr($assento, 1,1);


		if($code == "n") {
			$repetir = 1;
			$len = strlen($assento);	
			if($len != 2 ) {
				$repetir = substr($assento, 2,$len);
			}			

			$x = "<span class='lugar espaco' ></span>";
			$out .= str_repeat($x, $repetir);			
		}

		if($code == "#") {
			$out .= "<div style='clear:left;'></div>";
		}

		$assento = '';
	}

	if($assento != '') {

		$classe = '';
		$img 	= 'chair01.png'; 
		$status = "";
		if(in_array($assento, $cadeirasOcupadas)) {
			$classe = ' ocupada ';
			$img 	= 'chair02.png'; 
			$status = 'OC';
		}
		#$out .= '<span class="overlay assento '.$classe.'" id="'.$assento.'" ><span><img src="Public/img/'.$img.'" title="Assento: '.$assento.'" data-cod="'.$assento.'" class=" chair "/> </span></span> &nbsp;';
		$out .= "<span class=''><div class='lugar assento $classe' id='$assento'>".strtoupper($assento)."</div></span>";

	}

}

?>
<div class="content">
	<div class="box">
        <div class="box-header">
            <a href="/index.php?route=alunos/cadastrar/&amp;tipo=a"><span class="btn btn-primary btnAdd btn-flat"> <i class="fa fa-plus"></i> Novo Evento</span> </a>
        </div>
    </div>

    <div class="row col-md-12 msg" style="height: 60px;"></div>
    <div class="row">
	    <div class="col-sm-10">
	    	<span style="    margin: 0 auto; text-align: center; border: solid 1px; float: left; padding: 11px; background: #fff;">
				<div style="    background: #E0E0E0; padding: 20px; margin-bottom: 20px; font-size: 40px; text-align: center;">PALCO 2</div>    		
	    		<?php echo $out; ?>
	    	</span>
	    </div>
	</div>
    <div class="col-sm-2">
        <a href="#" class="btn btn-flat btn-default"> Continuar </a>
    </div>
</div>


<style>

	.overlay, .espaco, .chair {
		border-radius: 10px;
	}
	.lugar {
	    width: 33px;
	    height: 33px;
	    float: left;
	    margin: 3px;
	    border-radius: 50px;
	    text-align: center;
	    /* vertical-align: middle; */
	    line-height: 29px;
	    font-size: 13px;
	    font-weight: bold;
    color: #fff
	}
	.chair {
    padding-bottom: 4px;
    width: 23px;		
	}

	.assento {
		background: #3478c1;
		cursor: pointer;
	}
	.espaco {
		
	}
	.ocupada {
		background: #E1E1E1;
	}

	.selecionada {
		background: #20c320;
	}


</style>	



<script>


	var msg = '';
	var lugares = [];
	var nMaxIngressos = <?php echo $nMaxIngressos; ?>;

	$(".assento").on("click",function() {
		assento = this.id;

		msg = '';
		if( !$(this).hasClass('ocupada') ) {
			
			if ($(this).hasClass('selecionada')) {
				$(this).removeClass('selecionada');
				msg = '';
				removeLugar(assento);
			} else {
				assento = this.id;
				lugares.push(assento);

				x = lugares.length;
				if(x > nMaxIngressos) {
					msg = "<div class='alert alert-error'>Você pode adquirir apenas "+nMaxIngressos+" ingressos. </div>";
					removeLugar(assento);
				} else {
					$(this).addClass('selecionada');
					msg = "<div class='alert alert-info'>Assento: " + assento + " selecionado</div>";								
				}



			}
		} else {
			msg = "<div class='alert alert-warning'>Assento: " + assento + " não esta disponível!</div>";
		}
		


		console.log(lugares);
		setMSG();
	});


	function setMSG() {
		$(".msg").html(msg);

		setTimeout(function(){
			$(".msg").html('');			
		},3000)
	}

	function removeLugar(y) {
		var y = y;
		arr = lugares;
		lugares.splice($.inArray(y, lugares),1);


	}

</script>
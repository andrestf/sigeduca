    <br/>
	<?php echo $ret; ?>
	<?php  if($acao == 'CADASTRAR') { ?>
		<form class="form" id="frmNovoEvt" method="post" action="/index.php?route=eventos/novo/">
	<?php } ?>

	<?php  if($acao == 'EDITAR') { ?>
	<form class="form" id="edtEvt" method="post" action="/index.php?route=eventos/editar/&evt=<?php echo  $evento['evt_id']; ?>">
	<input type="hidden" class="form-control " name="idEdit" value="<?php echo $evento['evt_id'];?>" />		
	<?php } ?>
		<div class="row">
			<h4 class="title">Informações Básicas</h4>
			<div class="col-sm-6" >
				<label> Título</label>
				<input type="text" class="form-control " name="titulo" value="<?php echo $evento['evt_titulo'];?>" />
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3" >
				<label> Início</label>
				<input type="text" class="form-control datetimepicker data1"  name="dataini"  value="<?php echo DataHoraBR($evento['evt_dataini']);?>" />
			</div>

			<div class="col-sm-3" >
				<label> Fim</label>
				<input type="text" class="form-control datetimepicker data2"  name="datafim"  value="<?php echo DataHoraBR($evento['evt_datafim']);?>" />
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12" >
				<label> Descrição</label>
				<input type="text" class="form-control " name="descricao" value="<?php echo $evento['evt_descricao'];?>" />
			</div>
		</div>

		<div class="row">
			<h4 class="title">Ingressos</h4>
			<div class="col-sm-3 hide" >
				<label> Disponíveis</label>
				<input type="number" class="form-control"  name="ingressos" onkeyup="formataNumerico(this);" value="<?php echo $evento['evt_qtdingressos'];?>" />
			</div>

			<div class="col-sm-3" >
				<label> Mínimo por aluno</label>
				<input type="number" class="form-control"  name="ingressosalunomin" onkeyup="formataNumerico(this);" value="<?php echo $evento['evt_qtdingralunomin'];?>" />
			</div>

			<div class="col-sm-3" >
				<label> Maximo por aluno</label>
				<input type="number" class="form-control"  name="ingressosalunomax" onkeyup="formataNumerico(this);" value="<?php echo $evento['evt_qtdingralunomax'];?>" />
			</div>

			<div class="col-sm-3" >
				<label> Valor Unitário</label>
				<input type="text" class="form-control"   name="valorunit" id="valorunit" onkeyup="formataValorNovo(this);" value="<?php echo $evento['evt_vlrpadrao'];?>" />
			</div>
		</div>

		<div class="row hide">
			<h4 class="title">Alunos </h4>
			<div class="col-sm-12" >
				<label> Cursos que pode adiquirir os ingressos.</label>
				<select class="form-control select2" style="width: 50%" multiple="multiple" name="cursos[]" disabled="disabled" ="readonly" >
					<?php

					$matriculas = explode(",", str_replace(" ","",$evento['evt_matricula']));
					foreach ($cursos as $curso) { ?>
						


						<option value="<?php echo $curso['tpmat_id']; ?>" <?php  echo (in_array($curso['tpmat_id'],$matriculas)) ? ' selected="selected" ': ' c ' ; ?>  ><?php echo $curso['tpmat_descricao']?></option>
					<?php } ?>
				</select>
				
				<small clas="text-info hide">Não informar nenhum curso, permitira que qualquer aluno adiquira os ingressos disponíveis de acordo com os limites definidos.</small>
			</div>
		</div>

		<div class="row">
			<h4 class="title">Reserva dos Ingressos</h4>
				<div class="col-sm-3" >
					<label> Início</label>
					<input type="text" class="form-control datetimepicker ldata1"  name="datalimiteini"  value="<?php echo DataHoraBR($evento['evt_datalimiteini']);?>" />
				</div>

				<div class="col-sm-3" >
					<label> Fim</label>
					<input type="text" class="form-control datetimepicker ldata2"  name="datalimitefim"  value="<?php echo DataHoraBR($evento['evt_datalimitefim']);?>" />
				</div>
		</div>

		<div class="row">
			<h4 class="title">Informações ao Público</h4>
				<div class="col-sm-3" >
					<label> Status</label>
						<select class="form-control select2" style="width: 50%" name="statuspublico" >
							<option value="FECHADO" <?php echo ($evento['evt_statuspublico'] == 'FECHADO' ? 'selected="selected"' : '' ); ?>  >FECHADO</option>
							<option value="ABERTO"  <?php echo ($evento['evt_statuspublico'] == 'ABERTO' ? 'selected="selected"' : '' ); ?> >ABERTO</option>
							
					</select>					
				</div>

		</div>		

		<?php if($acao == 'CADASTRAR') { ?>
		<div class="rowx">
			<h4 class="title">Layout do Evento </h4>
			<div class="row">
				<div class="col-sm-12">
					<label> Selecione o layout do evento.</label>
					<select class="form-control" style="width: 50%" name="layout" id="layout">
						<option value="0">Selecione</option>
						<?php foreach ($layouts as $layout) { ?>
							<option value="<?php echo $layout['evtlay_cod']?>" ><?php echo $layout['evtlay_titulo']?></option>
						<?php } ?>
					</select>
					
					<div class="pre" style="margin-top: 20px; min-height: 415px;"></div>

				</div>
			</div>
		</div>
		<?php } ?>

		<div class="row">

			<div class="col-sm-12">
				<a href="<?php echo permalink('eventos/')?>" class="btn btn-flat btn-primary "> Voltar </a>
				<input type="submit" name="continuar" value="Salvar" class="btn pull-right btn-flat btn-default btnSalvarEvt">
			</div>
		</div>

	</form>
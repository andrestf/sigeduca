<div class="content">
    <h3 class="page-title">Lista Reservas - <small>NOME DO EVENTO</small></h3>

	<br/>
	<div class="box">
        <div class="box-header">
            <a href="/index.php?route=eventos/"><span class="btn btn-default btnAdd btn-flat"> Voltar </span> </a>
        </div>
    </div>

	<div class="box">
		
        <div class="box-body">
            <table class="table table-condensed table-striped tablegrid table-grid" id="lista">
				<thead>
					<tr>
						<th width="40">Cód</th>
						<th width="40">Ing</th>
						<th width="150">E-mail</th>
						<th width="*">Responsável</th>
						<th width="50">Assento</th>
					</tr>
				</thead>

				<tbody>
				<?php if($INGRESSOS) { ?>
					<?php $i = 1;foreach ($INGRESSOS as $INGRESSO) { ?>
						<tr class=" <?php echo $INGRESSO['usu_nomecompleto']; ?> ">
							<td><?php echo $INGRESSO['evti_responid']; ?></td>
							<td><?php echo $INGRESSO['evit_cod']; ?></td>
							<td><?php echo $INGRESSO['usu_email']; ?></td>
							<td><?php echo $INGRESSO['usu_nomecompleto']; ?></td>
							<td><?php echo $INGRESSO['evit_assento']; ?></td>
						</tr>
					<?php $i++;} ?>
				<?php } ?>

				</tbody>
            </table>
        </div>
    </div>


</div>


<script>

	$("#busca").on("keyup", function() {
		val = $("#busca").val();

		if(val == '') {
			$("#lista tbody tr").show();
		} else {
			$("#lista tbody tr").hide();

			if( $("#lista tbody tr").hasClass(val) ) {
				$("#lista tbody tr").hasClass(val).show();
			}
		}
	})
</script>

<div class="content">

	<?php if($RESERVA_LIBERADA) { ?>
		<?php if($ACAO != "RESERVA") { ?>
	    <h3 class="page-title">Evento - Reserva de Assentos - Abril 2019</h3>
	    <br/>
		<?PHP } ?>
	    <div class="box box-primary">
	    	<div class="box-header with-border text-center">
				<?php if($ACAO != "RESERVA") { ?>
					Selecione os assentos de sua preferência....
				<hr/>
				<?PHP } ?>
				
				<div class="col-sm-4">
					<div class="box-infos-reservas adq">
						<b>Ingressos Comprados: </b> <br/> <?php echo $ingressos['ADQUIRIDOS']; ?>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="box-infos-reservas min">
						<b>Ingressos Utilizados:</b> <br/> <?php echo $ingressos['UTILIZADOS']; ?>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="box-infos-reservas max">
						<b>Ingressos Disponíveis: </b> <br/> <?php echo $ingressos['DISPONIVEIS']; ?>
					</div>
				</div>
				
	    	</div>
	    	<div class="box-body" style="">
	    		<div style="width: 100%; text-align: center;">
	    			<div class="" style=" display: inline-block; border: solid 1px; padding: 10px;">
						<?php echo $layout; ?>
					</div>
				</div>
			</div>
			<hr/>
			<div class="box-body row">
				
				<div class="col-sm-12">
				<a href="javascript: history.back();" class="btn btn-primary"> <i class="fa fa-history"></i> Cancelar </a>
				</div>

                <div>
                  <a href="javascript: void(0); " onclick="Continuar0()" class="btn btn-default pull-right text-black" > <i class="fa fa-arrow-right"></i> Continuar... </a>
                </div>
            </div>
		
		</div>
	<?php } else { ?>
		<div class="box box-body box-primary">
			<h4>Desculpe!!!</h4>
			A reserva de assentos para o evento selecionado ainda não esta liberado....
			<br/><br/>
            <a href="javascript: history.back();" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> Voltar </a>
		</div>
	<?php } ?>

</div>

<style>
	.box-infos-reservas {
		border: solid 2px;
	    padding: 15px;
	    font-size: 18px;
	    color: #54676b;
	    border-radius: 5px;
	    box-shadow: #000 1px 1px 5px;
	}

	.adq  {color: #457812 !important; }
	.min  {color: #ff2b78 !important; }
	.max  {color: #dcab0d !important; }
	.tab-content {
		    min-height: 460px !important;
	}
</style>


<script>
	var MaxIn = <?php echo $ingressos['DISPONIVEIS']; ?>;
	var nAssentosSelecionados = 0;
	var aAssentosSelecionados = [];
	

	$(".assento").on("click", function() {

		if($(this).hasClass('ocupada')) {
			alertify.error("Assento Ocupado!");
			return;
		}

		title = $(this).attr("data-title");
		CodAssento = $(this).attr("id");

		if($(this).hasClass("selecionada")) {
			///////////////////////////////////////////////////////////
			//remover
			$(this).removeClass("selecionada")
			nAssentosSelecionados = nAssentosSelecionados - 1
			var i = aAssentosSelecionados.indexOf(CodAssento);
			if(i != -1) {
				aAssentosSelecionados.splice(i, 1);
			}

		} else {
			///////////////////////////////////////////////////////////
			//adicionar
			if(nAssentosSelecionados < MaxIn)  {
				$(this).addClass("selecionada")
				nAssentosSelecionados = nAssentosSelecionados + 1
				aAssentosSelecionados.push(CodAssento);
			}
		}

		//console.log(nAssentosSelecionados);
		//console.log(aAssentosSelecionados);

	});


	function Continuar0()  {
		if(nAssentosSelecionados == 0) {
			alertify.alert("Atenção","Informe os assentos para reserva.")
			return;
		}

		alertify.confirm("Atenção","Confirmar escolha dos assentos ?",
			function() {
				Continuar();	
			},

			function() {
				console.log("cancelado");
			})
	}


	function Continuar() {
		$.post({
			type: 'POST',
			dataType: 'json',
			data: {assentos: aAssentosSelecionados, nassentos: nAssentosSelecionados},
			url : "<?php echo permalink("Eventos/Aluno/&codigo=".$_GET['codigo']."&evt=".$_GET['evt']."&m=".$ACAO."&step=2&act=GetAssentos"); ?>",
			success: function(e) {
				if(e.erro) {
					alertify.error(e.mensagem);
					return false;
				} else {
					if(e.ok == true) {
						<?php if($ACAO != "RESERVA") { ?>
							ImprimirVoucher(<?php echo $_GET['evt']?>,<?php echo $_GET['codigo']?>);
						<?php } ?>
						<?php if($ACAO == "RESERVA") { ?>
							window.location = "<?php echo permalink("Eventos/Editar/&evt=".$evtid."&tab=assentos")?>";
						<?php } ?>
						alertify.success(e.mensagem);
					}
					
				}
			}
		})
	}

	function ImprimirVoucher(evt,cd) {
		window.location = "<?php echo permalink("Eventos/Aluno/&")?>codigo="+cd+"&evt="+evt+"&act=voucher&step=1";
	}


</script>
<div class="content">
	<h3 class="page-title">Vouchers para o evento: <?php echo $evento['evt_titulo'];?></h3>

	<?php if(!$vouchers) {?>
		<br/>
		<span class="text-orange">Nenhum voucher disponível para o evento selecionado.</span>
	<?php } else { ?>

	<div class="box">
		
        <div class="box-body">
            <table class="table table-condensed table-striped tablegrid" id="lista">
				<thead>
					<tr>
						<th>Responsável</th>
						<th width="130">Assento</th>
						<th width="130" >Data Compra</th>
						<th width="130">Data Reserva</th>
						<th width="50"></th>
					</tr>
				</thead>

				<tbody>
				
					<?php foreach ($vouchers as $INGRESSO) { ?>
						<tr class=" <?php echo $INGRESSO['evit_assento']; ?> ">
							<td><?php echo strtoupper($INGRESSO['usu_nomecompleto']); ?></td>
							<td><?php echo strtoupper($INGRESSO['evit_assento']); ?></td>
							<td><?php echo DataHoraBR($INGRESSO['iCADDATA']); ?></td>
							<td><?php echo DataHoraBR($INGRESSO['evti_datareserva']); ?></td>
							<td>
								<a href="<?php echo permalink("Eventos/Aluno/&codigo=".$_GET['codigo']."&x=".$INGRESSO['evti_id']."&voucher=".$INGRESSO['evti_id']*$num."&evt=".$_GET['evt'])?>&act=voucher&step=2" >
									<i class="fa fa-print"></i>
								</a>
							</td>
						</tr>
					<?php } ?>

				</tbody>
            </table>
        </div>
    </div>



	<?php } ?>
	<br/><br/>
	<a href="javascript: history.back();" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> Voltar </a>


</div>

<div class="content asec-box">
    <h3 class="page-title">
        Reserva de ingressos
    </h3>

	<br/>
	<div class="res" style="display: none;">
		<span class="cnt"></span>

	</div>
	<div class="box box-primary box-reserva">
		<div class="box-header with-border">
			<span style="font-size: 14px;">
				<b>Reserva Mínima:</b> <?php echo $INGRESSOS['MIN']?>
				<br/>
				<b>Reserva Máxima: </b> <?php echo $INGRESSOS['MAX']?>
				<br/>
				

				<span class="text-blue">
				Ingressos já adquiridos: </b> <?php echo $INGRESSOS['ADQUIRIDOS']?>
				</span>
			</span>
		</div>
		<div class="box-body">
			<div class="error alert alert-danger" style="display: none;"></div>

			<div class="row">
				<div class="col-sm-12">
					<h4>
						Informe a quantidade de ingressos a ser reservada para: 
						<b><?php echo $RESPON->usu_nomecompleto; ?></b>
					</h4>
					<hr/>
				</div>
			</div>			
			<div class="row">
				<div class="col-xs-4">
					<label>Reservar Ingressos:</label>
					<input type="number" value="" min="<?php echo $INGRESSOS['MIN']?>" max="<?php echo $INGRESSOS['MAX']?>" id="qtdReserva" class="form-control">
					<select class="form-control hide" id="qtdReservax">
						<option>[ SELECIONE ]</option>
						<?php for ($i=$INGRESSOS['MIN']; $i <= $INGRESSOS['MAX'] ; $i++) { ?>
							<?php if($i > 0) { ?>
							<option value="<?php echo $i;?>"> <?php echo $i;?> </option>	
							<?php } ?>
						<?php } ?>
					</select>
				</div>

				<div class="col-xs-4">
					<label>Valor Unitário</label>
					<div class="valor">x &nbsp; <?php echo $EVENTO['evt_vlrpadrao']?></div>
				</div>

				<div class="col-xs-4 text-red">
					<label>Valor Total</label>
					<div class="valor-total">R$ <span class="nValorTotal">--</span> </div>
				</div>
	
			</div>

		</div>

		<div class="box-footer">
			<a href="javascript: history.back(-1)" class="btn btn-danger">Cancelar</a> &nbsp;
			<span class="btn btn-success pull-rightx" onclick="ConfirmaReserva()">Confirmar</span>
			<hr/>
			
			<small >		
			Ingressos Disponíveis para o evento <?php echo $INGRESSOS['DISPONIVEIS_GERAL']?>
			</small>


		</div>

	</div>
</div>


<style>
.valor, .valor-total {
    font-size: 22px;	
}
</style>


<script>
	var nCD 		 = "<?php echo $RESPONID; ?>";
	var nEV 		 = "<?php echo $EVTID; ?>";
	var nQtdReserva  = 0 ;
	var nValorPadrao = <?php echo $EVENTO['evt_vlrpadrao']; ?>;
	$("#qtdReserva").on("change", function() {


		nQtdReserva = $("#qtdReserva").val();

		if(nQtdReserva <= <?php echo $INGRESSOS['MIN']?> ) {
			$("#qtdReserva").val(<?php echo $INGRESSOS['MIN']?>);
		}

		if(nQtdReserva >= <?php echo $INGRESSOS['MAX']?> ) {
			$("#qtdReserva").val(<?php echo $INGRESSOS['MAX']?>);
		}		
		
		nValorTotal =  nQtdReserva * nValorPadrao;
		$(".nValorTotal").html(nValorTotal.toFixed(2));

	})
	//$("#qtdReserva").change();

	function ConfirmaReserva() {
		if(nQtdReserva == 0) {
			alertify.alert("Atenção","Informe a quantidade de ingressos para reserva");
			return;
		}

		if(nQtdReserva < <?php echo $INGRESSOS['MIN']?> ) {
			$(".error").html("Atenção<br/> Quantidade não é válida!");
			$(".error").fadeIn();
			$("#qtdReserva").val('');
			$(".nValorTotal").html('--');
			$("#qtdReserva").focus();
			return;
		}

		if(nQtdReserva > <?php echo $INGRESSOS['MAX']?> ) {
			$(".error").html("Atenção<br/> Quantidade não é válida!");
			$(".error").fadeIn();
			$(".nValorTotal").html('--');
			$("#qtdReserva").val('');
			$("#qtdReserva").focus();
			return;
		}			
		ConfirmarReserva2();
		/*
		alertify.confirm("Confirmar?","Confirmar reserva de ingressos. ?",
			function() {//confirm
				ConfirmarReserva2();
			},
			function() {//cancel
				
			}
		).set('labels', {ok:'CONFIRMAR', cancel:'CANCELAR'});
		*/
	}


	function ConfirmarReserva2() {
		LD();

		$(".box-footer").fadeOut();
		$.ajax({
			type: 'post',
			dataType: 'json',
			url : "<?php echo permalink('Eventos/Aluno/')?>&codigo="+nCD+"&evt="+nEV+"&step=3&tipo=r&act=Reserva",
			data: { codigo : nCD, evt : nEV , tipo:'r', step: 3, act: 'reserva', q : nQtdReserva  },
			success: function(res) {
				if(res.erro != 0) {
					alertify.error( res.mensagem )
				} else {
					alertify.success( res.mensagem )
					$(".box-reserva").hide();
					$(".res .cnt").html( res.mensagemRet );
					$(".res").fadeIn();

				}
			},
			error: function(e,x,s) {

			},
			complete: function() {
				LD();
				$(".box-footer").fadeIn();
			}
		})
	}

</script>
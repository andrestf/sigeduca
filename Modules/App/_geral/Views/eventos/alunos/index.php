<div class="content asec-box">
    <h3 class="page-title">
        Eventos
    </h3>
<div class="box box-primary">
	<?php if(!$ERRO) {?>
		<?php if(!$MATRICULAS) { ?>
			<div class="box-body">
				<div class="alert alert-info">
					Aluno sem matrícula
				</div>
			</div>
		<?php } else { ?>
			<br/>

				<div class="box-header with-border">
		            <h3 class="box-title">
		            	<i class="fa fa-list"></i>
		                Selecione o evento
		                </h3>
		        </div>		

				<div class="box-header with-border">
					<span class="text-red">
						<b>Eventos disponíveis para o responsável: </b>
						<?php echo $RESPON['usu_nomecompleto'];?>
					</span>
				</div>
				<div class="box-body">
					<div class="evt-lista">
						<?php if($EVENTOS) { 
							$step = "2";
							$act = "reserva";
							if($this->TipoAcesso("usuario")) { 
								$step = 1;
								$act = "GetAssentos"; } 
							?>

							<?php foreach ($EVENTOS as $evento) {
								if($evento['evt_statuspublico'] == 'ABERTO') { ?>
								<div class="evt-item">
									<?php if(date("Y-m-d G:i:s", strtotime($evento['evt_datalimiteini'])) <= date("Y-m-d G:i:s") || $OPERADOR) { ?>
										<a href="<?php echo permalink("Eventos/Aluno","","codigo=$CODIGO&tipo=$TIPO&evt=".($evento['evt_id']*$NUM)."&step=$step&act=".$act)?>" class="  btnx " data-item="<?php echo $evento['evt_id'];  ?>" >
											<i class="fa fa-circle text-green"></i> <?php echo $evento['evt_titulo'];  ?>
										</a>											
									<?php } else { ?>
										<i class="fa fa-circle text-red"></i> <?php echo $evento['evt_titulo'];  ?>
									<?php } ?>
									<span class="evt-descri" style="display: block">
										<?php echo $evento['evt_descricao']; ?> <br/>
										<b>Data:</b> <?php echo DataHoraBR($evento['evt_dataini']); ?>

									</span>
									<span style="font-size: 14px;">
										<a href="<?php echo permalink("Eventos/Aluno","","codigo=$CODIGO&tipo=$TIPO&evt=".($evento['evt_id']*$NUM)."&step=1&act=voucher")?>" class="btn btn-default text-black btn-sm"> <i class="fa fa-ticket"></i> Meus Ingressos</a>
									</span>
									<span class="evt-descri evt-infos" style="display: block">
										<b>Inicio Reserva Assentos:</b> <?php echo DataHoraBR($evento['evt_datalimiteini']); ?> <br/>
										<b>Fim Reserva Assentos:</b> <?php echo DataHoraBR($evento['evt_datalimitefim']); ?>
									</span>
						
								</div>
							
								<?php }//status publico = aberto ?>
							<?php } ?>
						<?php } else { ?>
							<div class="alert alert-info">
								Nenhum evento disponivel para a matricula informada!
							</div>
						<?php } ?>
					</div>
				</div>
			
		<?php } // IF MATRICULAS ?>
	<?php } else { ?>
		<br/>
		<div class="alert alert-info"><?php echo $ERROM; ?></div>

	<?php }?>
	<div class="box-footer">
			<a href="javascript: history.back(-1)" class="btn btn-primary">Voltar</a>
		</div>
</div>			
</div>


<style>
	.evt-lista {
		margin-bottom: 20px;
	}

	.evt-item {
    /* color: #000; */
    	position: relative;
	    margin-bottom: 15px;
	    border: solid 1px #E1E1E1;
	    padding: 10px;
	    border-radius: 5px;
	    display: block;
	    width: auto;
	    font-size: 28px;
	}
	
	.evt-descri {
		font-size: 12px;
	}
	.evt-infos {
	    display: block;
	    /* float: right; */
	    position: absolute;
	    right: -1px;
	    top: -1px;
	    color: #457845;
	    font-size: 14px;
	    border: solid 1px #E1E1E1;
	    padding: 10px;
	    bottom: -1px;
	    border-radius: 0px 7px 7px 0px;
	    padding-top: 24px;
	    background: #f3f3f3
	}


</style>

<script>
	$(".evt-item").on("click", function() {
		//$("a:first-child").click();
	})

</script>
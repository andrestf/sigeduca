<div class="content">
    <h3 class="page-title">Eventos</h3>
    <br/>

	<div class="box">
        <div class="box-header">
            <a href="/index.php?route=eventos/novo/"><span class="btn btn-default btnAdd btn-flat"> <i class="fa fa-plus"></i> Novo Evento</span> </a>
        </div>
    </div>


	
	<div class="box box-body">
		<?php if($eventos) { ?>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Evento</th>
					<th width="120" class="text-center">Data</th>
					<th width="100" class="text-center">Ingressos Totais</th>
					<th width="100" class="text-center">Ingressos Confirmados</th>
					<th width="100" class="text-center">Ingressos Disponíveis</th>
					<th width="120" class="text-center">&nbsp;</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($eventos as $evento) { 
						$class = "";
						if($evento['evt_cancelado'] != '') {
							$class = 'cancelado';
						}
					?>
				<tr  style="" class=" <?php echo $class; ?> ">
					<td><b class='text-blue'><?php echo $evento['evt_titulo']; ?></b><br/> <small><?php echo $evento['evt_descricao']; ?> </small></td>
					<td class="text-center"><?php echo DataBR($evento['evt_dataini']); ?></td>
					<td class="text-center"><?php echo $evento['evt_qtdingressos']; ?></td>
					<td class="text-center"></td>
					<td class="text-center"></td>
					<td class="text-center"> 
						<i class="fa fa-edit btn btn-default btn-sm" onclick="openEvent(<?php echo $evento['evt_id']; ?>);" title="Editar"></i>
						<i class="fa fa-list btn btn-default btn-sm" onclick="openReservas(<?php echo $evento['evt_id']; ?>);"title="Reservas"></i> </td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php } else { ?>
			<center class="text-blue">
				Nenhum evento encontrado.
			</center>
		<?php } ?>
	</div>

</div>

<style>
	
	.cancelado {
		background: #ffc1c1;
	}
</style>

<script>

	function openEvent(n) {
		window.location = "/index.php?route=eventos/editar/&evt="+n;
	}

	function openReservas(n) {
		window.location = "/index.php?route=eventos/listareservas/&evt="+n;
	}

</script>
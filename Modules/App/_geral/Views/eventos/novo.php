
<link rel="stylesheet" href="/Public/plugins/datetimepicker/bootstrap-datetimepicker.css">
<script src="/Public/bower_components/moment/moment.js"></script>
<script src="/Public/plugins/bootstrap/js/transition.js"></script>
<script src="/Public/plugins/bootstrap/js/collapse.js"></script>
<script src="/Public/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>

<div class="content">
    <h3 class="page-title"><?php echo $title; ?></h3>
<hr/>
<div>
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" id="tb_evento" class="active"><a href="#evento" aria-controls="home" role="tab" data-toggle="tab">Evento</a></li>
    <?php  if($acao == 'EDITAR') { ?>
    	<li role="presentation" id="tb_assentos"><a href="#infos" aria-controls="profile" role="tab" data-toggle="tab">Assentos</a></li>
    <?php } ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content" style="background: #fff; padding: 10px;">
    <div role="tabpanel" class="tab-pane active" id="evento">
    	<?php require_once 'novo_ficha.php'; ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="infos">
    	<?php require_once 'novo_assentos.php'; ?>
    </div>
  </div>

</div>





</div>
		
		<style>
		.form {
			overflow: all;
		}
			.pre {
				margin-top: 20px;
			    border: solid 2px #E1E1E1;
			    display: inline-block;
			    padding: 20px;			
			}
			.title {
				margin-left: 14px;
				font-weight: bold;
				color: #507fd8;
				border-bottom: solid 1px;
				margin-right: 18px;
				padding-bottom: 6px;				
			}
			.row {
				margin-bottom:  10px;
			}
		</style>

        <script type="text/javascript">
            <?php if(isset($_GET['tab'])) { ?>
                tab = "<?php echo $_GET['tab'] ?>";
                $("#tb_"+tab+" a").click()
            <?PHP } ?>

        	$(".pre").hide();
        	<?php if($acao == "CADASTRARAR") { ?>
        		$(".btnSalvarEvt").hide();
        	<?PHP } ?>
            $(function () {

            	$("#valorunit").keyup();

            	$("#layout").on("change", function() {
            		id = $("#layout option:selected").val();

            		if(id != '' && id != '0') {
            			$.ajax({
            				//MontaLaySimplesByGet
            				url: "index.php?route=EventosLayout/MontaLaySimplesByGet/&id="+id+"&print=t",
            				success: function(r) {
            					$(".pre").html(r);
            					$(".pre").show();
            					$(".btnSalvarEvt").show();
            				}
            			})
            		} else {
            					$(".pre").html();
            					$(".pre").hide();
            					$(".btnSalvarEvt").hide();
            		}

            	});

                $('.datetimepicker').datetimepicker({
                	useCurrent : false,
                	sideBySide : true,
                	format : 'DD/MM/YYYY HH:mm'
                });


        
		        $(".data1").on("dp.change", function (e) {
		            $('.data2').data("DateTimePicker").minDate(e.date);
		        });
		        $(".data2").on("dp.change", function (e) {
		            $('.data1').data("DateTimePicker").maxDate(e.date);
		        });


		        $(".ldata1").on("dp.change", function (e) {
		            $('.ldata2').data("DateTimePicker").minDate(e.date);
		        });
		        $(".ldata2").on("dp.change", function (e) {
		            $('.ldata1').data("DateTimePicker").maxDate(e.date);
		        });		        



		        $("#frmNovoEvt").on("submit", function(e) {
		        	//e.preventDefault();
		        })

            });
        </script>
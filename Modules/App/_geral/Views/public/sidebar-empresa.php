  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" id="asec-menu-primary">
        <li class="header"></li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="<?php echo $this->Link("dashboard")?>" ><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

        <li class='treeview'>
          <a href="#"><i class="fa fa-users"></i> <span>Cadastros</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>          
          <?php if($this->ValidaNivel2(40)) { ?>
              <ul class='treeview-menu'>
                <li><a href="<?php echo Permalink("alunos",'listar',"tipo=a")?>" ><i class="fa fa-circle-o"></i> <span>Alunos</span></a></li>
                <li class='menu-sep'></li>
                <li><a href="<?php echo Permalink("alunos",'listar',"tipo=r")?>" ><i class="fa fa-circle-o"></i> <span>Responsáveis</span></a></li>

              </ul>
          <?php } ?>
        </li> 







        <?php if ($this->ValidaNivel2(50)) { ?>
        <li class='treeview'>
          <a href="#"><i class="fa fa-money"></i> <span>Financeiro</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>          
          <?php if($this->ValidaNivel2(50)) { ?>
              <ul class='treeview-menu'>
                <li><a href="<?php echo $this->Link("financeiro")?>"><i class="fa fa-circle-o"></i> <span>Financeiro</span></a></li>
                <li class='menu-sep'></li>
                <li class=''><a href="<?php echo $this->Link("financeiro/TipoDocumentos")?>"><i class="fa fa-circle-o"></i> <span>Tipos de Documentos</span></a></li>
                <li class='hide'><a href="<?php echo $this->Link("financeiro/FomasPagamentos")?>"><i class="fa fa-circle-o"></i> <span>Formas de Pagamento</span></a></li>
                <li class='menu-sep'></li>
                <li class=''><a href="<?php echo $this->Link("financeiro/Reports")?>"><i class="fa fa-circle-o"></i> <span>Relatórios</span></a></li>
                <li class='menu-sep'></li>
              </ul>
          <?php } ?>
        </li>

        <li><a href="<?php echo $this->Link("eventos")?>" ><i class="fa fa-home"></i> <span>Eventos</span></a></li>
        <?php } ?>

      
        <?php if ($this->ValidaNivel2(50)) { ?>
        <li class="treeview">
          <a href="#"><i class="fa fa-building"></i> <span>Colaboradores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo Permalink("colaboradores","lista","tipo=atendentes")?>" ><i class="fa fa-circle-o"></i> <span>Atendentes</span></a></li>
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=professores")?>" ><i class="fa fa-circle-o"></i> <span>Professores</span></a></li>
              <li><a href="<?php echo Permalink("colaboradores","lista","tipo=gerentes")?>"><i class="fa fa-circle-o"></i> <span>Gerentes</span></a></li>
          </ul>
        </li>
        <?php } ?>

       
        <li><a href="<?php echo $this->Link("login","end")?>" data-async="false"><i class="fa fa-sign-out"></i> <span>Sair</span></a></li>
      </ul>
      
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" id="asec-menu-primary">
        <li class="header"></li>
        <!-- Optionally, you can add icons to the links -->
        <li class="hide"><a href="<?php echo $this->Link("dashboard")?>" ><i class="fa fa-home"></i> <span>Dashboard</span></a></li>


        
          <li><a href="<?php echo Permalink("alunos",'listar',"tipo=a")?>" ><i class="fa fa-users"></i> <span>Alunos</span></a></li>
          <li><a href="<?php echo Permalink("AlunosFinanceiro",'index',"tipo=r&all&codigo=".$_SESSION['APP_USUID'])?>" ><i class="fa fa-money"></i> <span>Financeiro</span></a></li>
          <li><a href="<?php echo $this->Link("Eventos/Aluno/&codigo=".$_SESSION['APP_USUID']."&tipo=r")?>" ><i class="fa fa-home"></i> <span>Eventos</span></a></li>
    
      
      
       
        <li><a href="<?php echo $this->Link("login","end")?>" data-async="false"><i class="fa fa-sign-out"></i> <span>Sair</span></a></li>
      </ul>
      
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
  